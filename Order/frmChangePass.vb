﻿Imports System.IO
Public Class frmChangePass

#Region "Controlbox"

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

    Private Sub Btncancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btncancel.Click
        Me.Close()
    End Sub

    Private Sub frmChangePass_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim reader As New StreamReader(Application.StartupPath & "\pass.dat")
        txtPass.Text = reader.Readline
        reader.Close()
    End Sub

    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Dim writer As New StreamWriter(Application.StartupPath & "\pass.dat")
        writer.writeline(txtPass.Text)
        writer.Close()
        Me.Close()
    End Sub
End Class