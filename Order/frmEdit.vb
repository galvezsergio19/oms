﻿Imports System.IO
Public Class frmEdit

    Dim globalpath As String

#Region "Controlbox"

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region " Procedures "

    Public Sub Edit_Product(ByRef Pname As String, ByRef Pprice As String, ByRef Ppath As String, ByRef Pimg As String)
        Try
            Dim path As String = Application.StartupPath & "\Burger\" & Ppath & ".txt"
            globalpath = path
            Dim reader As New StreamReader(path)
            Pname = reader.ReadLine
            lblEditname.Text = "Edit: " & Pname
            Pprice = reader.ReadLine
            Pimg = reader.ReadLine
            reader.Close()
        Catch ex As Exception
        End Try
    End Sub

    Public Sub Edit_Product1(ByRef Pname As String, ByRef Pprice As String, ByRef Ppath As String, ByRef Pimg As String)
        Try
            Dim path As String = Application.StartupPath & "\Drink\" & Ppath & ".txt"
            globalpath = path
            Dim reader As New StreamReader(path)
            Pname = reader.ReadLine
            lblEditname.Text = "Edit: " & Pname
            Pprice = reader.ReadLine
            Pimg = reader.ReadLine
            reader.Close()
        Catch ex As Exception
        End Try
    End Sub

    Public Sub Edit_Product2(ByRef Pname As String, ByRef Pprice As String, ByRef Ppath As String, ByRef Pimg As String)
        Try
            Dim path As String = Application.StartupPath & "\Special\" & Ppath & ".txt"
            globalpath = path
            Dim reader As New StreamReader(path)
            Pname = reader.ReadLine
            lblEditname.Text = "Edit: " & Pname
            Pprice = reader.ReadLine
            Pimg = reader.ReadLine
            reader.Close()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Save_Product()
        If txtPname.Text.Trim = String.Empty Then
            MessageBox.Show("Product Name cannot be empty.", "Invalid Product Name", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            txtPname.Focus()
            txtPname.SelectAll()
        ElseIf txtPprice.Text.Trim = String.Empty Then
            MessageBox.Show("Product Price cannot be empty.", "Invalid Product Price", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            txtPprice.Focus()
            txtPprice.SelectAll()
        Else
            Dim writer As New StreamWriter(globalpath)
            writer.WriteLine(txtPname.Text)
            writer.WriteLine(txtPprice.Text)
            writer.WriteLine(txtImg.Text)
            writer.Close()
            Me.Close()
            frmMenu.auto()
        End If
    End Sub

#End Region

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Save_Product()
    End Sub

    Private Sub txtImg_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtImg.TextChanged
        PicImg.ImageKey = txtImg.Text
    End Sub

    Private Sub frmEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class