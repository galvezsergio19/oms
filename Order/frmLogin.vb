﻿Imports System.IO
Public Class frmLogin

#Region "Controlbox"

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        End
    End Sub

    Private Sub btnMin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMin.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub

#End Region

    Private Sub Btncancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btncancel.Click
        End
    End Sub

    Private Sub BtnLogin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnLogin.Click
        Try
            Dim reader As New StreamReader(Application.StartupPath & "\pass.dat")
            Dim pass As String = reader.ReadLine
            reader.Close()
            If txtPass.Text.ToLower = pass Then
                Me.Close()
                frmMenu.MdiParent = Main
                frmMenu.Show()
            Else
                MessageBox.Show("You have enetered an invalid password. Please try again.", "Invalid Password", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                txtPass.Focus()
                txtPass.SelectAll()
            End If
        Catch ex As Exception
        End Try
    End Sub

End Class