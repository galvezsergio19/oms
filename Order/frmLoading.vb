﻿Public Class frmLoading

    'Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
    '    Me.ProgressBar1.Value = Me.ProgressBar1.Value + 2

    '    If Me.ProgressBar1.Value = 100 Then
    '        Me.Timer1.Enabled = False
    '        Me.Close()
    '        frmLogin.MdiParent = Main
    '        frmLogin.StartPosition = FormStartPosition.CenterScreen
    '        frmLogin.Show()
    '    End If
    'End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Timer2.Enabled = True
        Timer1.Enabled = False
    End Sub

    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        Me.Opacity = Me.Opacity - 0.05
    End Sub

    Private Sub Timer3_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer3.Tick
        If Me.Opacity = 0 Then
            Me.Size = New Size(0, 0)
            Me.Hide()
            Main.Show()
        End If
    End Sub

End Class