﻿Imports System.IO
Imports Microsoft.Office.Interop
Public Class frmMenu

#Region " Controlbox "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        End
    End Sub

    Private Sub btnMin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMin.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub

    Private Sub TimerMenu_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerMenu.Tick
        lblDate.Text = Date.Now.ToString("MMMMMMMMMM dd, yyyy")
        lblTime.Text = TimeOfDay
    End Sub

    Private Sub btnLogout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogout.Click
        Me.Close()
        frmLogin.MdiParent = Main
        frmLogin.Show()
    End Sub

#End Region

#Region " Shortcuts "
    Private Sub LogoutToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LogoutToolStripMenuItem.Click
        btnLogout_Click(sender, e)
    End Sub
#End Region

    Private Sub frmMenu_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        auto()
        Filter_Record()
    End Sub

#Region " Procedures "
    Private Sub Load_Product(ByVal Pname As String, ByVal Pprice As Integer)
        LstOrder.Items.Add(Pname.Trim + " - " + Pprice.ToString)
        txtAmount.Text = Format(Val(txtAmount.Text) + Pprice, "###,##0.00")
    End Sub
    Private Sub Remove_Product(ByVal Pname As String, ByVal Pprice As Integer)
        If LstOrder.SelectedItem = Pname.Trim + " - " + Pprice.ToString Then
            LstOrder.Items.Remove(LstOrder.SelectedItem)
            txtAmount.Text = Format(Val(txtAmount.Text) - Pprice, "###,##0.00")
        End If
    End Sub
    Private Sub Start_product(ByRef Pnamestart As String, ByRef Pnamepath As String, ByRef Ppricestart As String, ByRef Pimg As String)
        Try
            Dim path As String = Application.StartupPath & "\Burger\" & Pnamepath & ".txt"
            Dim reader As New StreamReader(path)
            Pnamestart = reader.ReadLine & vbCrLf & vbCrLf
            Ppricestart = reader.ReadLine
            Pimg = reader.ReadLine
            reader.Close()
        Catch ex As Exception
        End Try
    End Sub
    Private Sub Start_product1(ByRef Pnamestart As String, ByRef Pnamepath As String, ByRef Ppricestart As String, ByRef Pimg As String)
        Try
            Dim path As String = Application.StartupPath & "\Drink\" & Pnamepath & ".txt"
            Dim reader As New StreamReader(path)
            Pnamestart = reader.ReadLine & vbCrLf & vbCrLf
            Ppricestart = reader.ReadLine
            Pimg = reader.ReadLine
            reader.Close()
        Catch ex As Exception
        End Try
    End Sub
    Private Sub Start_product2(ByRef Pnamestart As String, ByRef Pnamepath As String, ByRef Ppricestart As String, ByRef Pimg As String)
        Try
            Dim path As String = Application.StartupPath & "\Special\" & Pnamepath & ".txt"
            Dim reader As New StreamReader(path)
            Pnamestart = reader.ReadLine & vbCrLf & vbCrLf
            Ppricestart = reader.ReadLine
            Pimg = reader.ReadLine
            reader.Close()
        Catch ex As Exception
        End Try
    End Sub
    Public Sub auto()

        Start_product(btnBurger1.Text, btnBurger1.Name, lblBurger1.Text, btnBurger1.Tag)
        Start_product(btnBurger2.Text, btnBurger2.Name, lblBurger2.Text, btnBurger2.Tag)
        Start_product(btnBurger3.Text, btnBurger3.Name, lblBurger3.Text, btnBurger3.Tag)
        Start_product(btnBurger4.Text, btnBurger4.Name, lblBurger4.Text, btnBurger4.Tag)
        Start_product(btnBurger5.Text, btnBurger5.Name, lblBurger5.Text, btnBurger5.Tag)
        Start_product(btnBurger6.Text, btnBurger6.Name, lblBurger6.Text, btnBurger6.Tag)
        Start_product(btnBurger7.Text, btnBurger7.Name, lblBurger7.Text, btnBurger7.Tag)
        Start_product(btnBurger8.Text, btnBurger8.Name, lblBurger8.Text, btnBurger8.Tag)
        Start_product(btnBurger9.Text, btnBurger9.Name, lblBurger9.Text, btnBurger9.Tag)
        Start_product(btnBurger10.Text, btnBurger10.Name, lblBurger10.Text, btnBurger10.Tag)
        Start_product(btnBurger11.Text, btnBurger11.Name, lblBurger11.Text, btnBurger11.Tag)
        Start_product(btnBurger12.Text, btnBurger12.Name, lblBurger12.Text, btnBurger12.Tag)
        Start_product(btnBurger13.Text, btnBurger13.Name, lblBurger13.Text, btnBurger13.Tag)
        Start_product(btnBurger14.Text, btnBurger14.Name, lblBurger14.Text, btnBurger14.Tag)
        Start_product(btnBurger15.Text, btnBurger15.Name, lblBurger15.Text, btnBurger15.Tag)
        Start_product(btnBurger16.Text, btnBurger16.Name, lblBurger16.Text, btnBurger16.Tag)

        Start_product1(btnDrink1.Text, btnDrink1.Name, lblDrink1.Text, btnDrink1.Tag)
        Start_product1(btnDrink2.Text, btnDrink2.Name, lblDrink2.Text, btnDrink2.Tag)
        Start_product1(btnDrink3.Text, btnDrink3.Name, lblDrink3.Text, btnDrink3.Tag)
        Start_product1(btnDrink4.Text, btnDrink4.Name, lblDrink4.Text, btnDrink4.Tag)
        Start_product1(btnDrink5.Text, btnDrink5.Name, lblDrink5.Text, btnDrink5.Tag)
        Start_product1(btnDrink6.Text, btnDrink6.Name, lblDrink6.Text, btnDrink6.Tag)
        Start_product1(btnDrink7.Text, btnDrink7.Name, lblDrink7.Text, btnDrink7.Tag)
        Start_product1(btnDrink8.Text, btnDrink8.Name, lblDrink8.Text, btnDrink8.Tag)
        Start_product1(btnDrink9.Text, btnDrink9.Name, lblDrink9.Text, btnDrink9.Tag)
        Start_product1(btnDrink10.Text, btnDrink10.Name, lblDrink10.Text, btnDrink10.Tag)
        Start_product1(btnDrink11.Text, btnDrink11.Name, lblDrink11.Text, btnDrink11.Tag)
        Start_product1(btnDrink12.Text, btnDrink12.Name, lblDrink12.Text, btnDrink12.Tag)
        Start_product1(btnDrink13.Text, btnDrink13.Name, lblDrink13.Text, btnDrink13.Tag)
        Start_product1(btnDrink14.Text, btnDrink14.Name, lblDrink14.Text, btnDrink14.Tag)
        Start_product1(btnDrink15.Text, btnDrink15.Name, lblDrink15.Text, btnDrink15.Tag)
        Start_product1(btnDrink16.Text, btnDrink16.Name, lblDrink16.Text, btnDrink16.Tag)

        Start_product2(btnSpecial1.Text, btnSpecial1.Name, lblSpecial1.Text, btnSpecial1.Tag)
        Start_product2(btnSpecial2.Text, btnSpecial2.Name, lblSpecial2.Text, btnSpecial2.Tag)
        Start_product2(btnSpecial3.Text, btnSpecial3.Name, lblSpecial3.Text, btnSpecial3.Tag)
        Start_product2(btnSpecial4.Text, btnSpecial4.Name, lblSpecial4.Text, btnSpecial4.Tag)
        Start_product2(btnSpecial5.Text, btnSpecial5.Name, lblSpecial5.Text, btnSpecial5.Tag)
        Start_product2(btnSpecial6.Text, btnSpecial6.Name, lblSpecial6.Text, btnSpecial6.Tag)
        Start_product2(btnSpecial7.Text, btnSpecial7.Name, lblSpecial7.Text, btnSpecial7.Tag)
        Start_product2(btnSpecial8.Text, btnSpecial8.Name, lblSpecial8.Text, btnSpecial8.Tag)
        Start_product2(btnSpecial9.Text, btnSpecial9.Name, lblSpecial9.Text, btnSpecial9.Tag)
        Start_product2(btnSpecial10.Text, btnSpecial10.Name, lblSpecial10.Text, btnSpecial10.Tag)
        Start_product2(btnSpecial11.Text, btnSpecial11.Name, lblSpecial11.Text, btnSpecial11.Tag)
        Start_product2(btnSpecial12.Text, btnSpecial12.Name, lblSpecial12.Text, btnSpecial12.Tag)
        Start_product2(btnSpecial13.Text, btnSpecial13.Name, lblSpecial13.Text, btnSpecial13.Tag)
        Start_product2(btnSpecial14.Text, btnSpecial14.Name, lblSpecial14.Text, btnSpecial14.Tag)
        Start_product2(btnSpecial15.Text, btnSpecial15.Name, lblSpecial15.Text, btnSpecial15.Tag)
        Start_product2(btnSpecial16.Text, btnSpecial16.Name, lblSpecial16.Text, btnSpecial16.Tag)


        Dim str As String = "00.00"
        Dim i As String = "0"
        Dim j As String = "1"
        'Dim reader As New StreamReader(Application.StartupPath & "\Burger\")
        If lblBurger1.Text = str Then
            btnBurger1.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnBurger1.ImageList = ImageList1
            btnBurger1.ImageIndex = i
            btnBurger1.Enabled = False
        Else
            btnBurger1.ImageList = ImageListBurger
            btnBurger1.ImageKey = btnBurger1.Tag
            btnBurger1.Enabled = True
        End If
        If lblBurger2.Text = str Then
            btnBurger2.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnBurger2.ImageIndex = i
            btnBurger2.Enabled = False
        Else
            btnBurger2.ImageList = ImageListBurger
            btnBurger2.ImageKey = btnBurger2.Tag
            btnBurger2.Enabled = True
        End If
        If lblBurger3.Text = str Then
            btnBurger3.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnBurger3.ImageIndex = i
            btnBurger3.Enabled = False
        Else
            btnBurger3.ImageList = ImageListBurger
            btnBurger3.ImageKey = btnBurger3.Tag
            btnBurger3.Enabled = True
        End If
        If lblBurger4.Text = str Then
            btnBurger4.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnBurger4.ImageIndex = i
            btnBurger4.Enabled = False
        Else
            btnBurger4.ImageList = ImageListBurger
            btnBurger4.ImageKey = btnBurger4.Tag
            btnBurger4.Enabled = True
        End If
        If lblBurger5.Text = str Then
            btnBurger5.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnBurger5.ImageIndex = i
            btnBurger5.Enabled = False
        Else
            btnBurger5.ImageList = ImageListBurger
            btnBurger5.ImageKey = btnBurger5.Tag
            btnBurger5.Enabled = True
        End If
        If lblBurger6.Text = str Then
            btnBurger6.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnBurger6.ImageIndex = i
            btnBurger6.Enabled = False
        Else
            btnBurger6.ImageList = ImageListBurger
            btnBurger6.ImageKey = btnBurger6.Tag
            btnBurger6.Enabled = True
        End If
        If lblBurger7.Text = str Then
            btnBurger7.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnBurger7.ImageIndex = i
            btnBurger7.Enabled = False
        Else
            btnBurger7.ImageList = ImageListBurger
            btnBurger7.ImageKey = btnBurger7.Tag
            btnBurger7.Enabled = True
        End If
        If lblBurger8.Text = str Then
            btnBurger8.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnBurger8.ImageIndex = i
            btnBurger8.Enabled = False
        Else
            btnBurger8.ImageList = ImageListBurger
            btnBurger8.ImageKey = btnBurger8.Tag
            btnBurger8.Enabled = True
        End If
        If lblBurger9.Text = str Then
            btnBurger9.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnBurger9.ImageIndex = i
            btnBurger9.Enabled = False
        Else
            btnBurger9.ImageList = ImageListBurger
            btnBurger9.ImageKey = btnBurger9.Tag
            btnBurger9.Enabled = True
        End If
        If lblBurger10.Text = str Then
            btnBurger10.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnBurger10.ImageIndex = i
            btnBurger10.Enabled = False
        Else
            btnBurger10.ImageList = ImageListBurger
            btnBurger10.ImageKey = btnBurger10.Tag
            btnBurger10.Enabled = True
        End If
        If lblBurger11.Text = str Then
            btnBurger11.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnBurger11.ImageIndex = i
            btnBurger11.Enabled = False
        Else
            btnBurger11.ImageList = ImageListBurger
            btnBurger11.ImageKey = btnBurger11.Tag
            btnBurger11.Enabled = True
        End If
        If lblBurger12.Text = str Then
            btnBurger12.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnBurger12.ImageIndex = i
            btnBurger12.Enabled = False
        Else
            btnBurger12.ImageList = ImageListBurger
            btnBurger12.ImageKey = btnBurger12.Tag
            btnBurger12.Enabled = True
        End If
        If lblBurger13.Text = str Then
            btnBurger13.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnBurger13.ImageIndex = i
            btnBurger13.Enabled = False
        Else
            btnBurger13.ImageList = ImageListBurger
            btnBurger13.ImageKey = btnBurger13.Tag
            btnBurger13.Enabled = True
        End If
        If lblBurger14.Text = str Then
            btnBurger14.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnBurger14.ImageIndex = i
            btnBurger14.Enabled = False
        Else
            btnBurger14.ImageList = ImageListBurger
            btnBurger14.ImageKey = btnBurger14.Tag
            btnBurger14.Enabled = True
        End If
        If lblBurger15.Text = str Then
            btnBurger15.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnBurger15.ImageIndex = i
            btnBurger15.Enabled = False
        Else
            btnBurger15.ImageList = ImageListBurger
            btnBurger15.ImageKey = btnBurger15.Tag
            btnBurger15.Enabled = True
        End If
        If lblBurger16.Text = str Then
            btnBurger16.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnBurger16.ImageIndex = i
            btnBurger16.Enabled = False
        Else
            btnBurger16.ImageList = ImageListBurger
            btnBurger16.ImageKey = btnBurger16.Tag
            btnBurger16.Enabled = True
        End If

        If lblDrink1.Text = str Then
            btnDrink1.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnDrink1.ImageList = ImageList1
            btnDrink1.ImageIndex = i
            btnDrink1.Enabled = False
        Else
            btnDrink1.ImageList = ImageListDrink
            btnDrink1.ImageKey = btnDrink1.Tag
            btnDrink1.Enabled = True
        End If
        If lblDrink2.Text = str Then
            btnDrink2.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnDrink2.ImageIndex = i
            btnDrink2.Enabled = False
        Else
            btnDrink2.ImageList = ImageListDrink
            btnDrink2.ImageKey = btnDrink2.Tag
            btnDrink2.Enabled = True
        End If
        If lblDrink3.Text = str Then
            btnDrink3.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnDrink3.ImageIndex = i
            btnDrink3.Enabled = False
        Else
            btnDrink3.ImageList = ImageListDrink
            btnDrink3.ImageKey = btnDrink3.Tag
            btnDrink3.Enabled = True
        End If
        If lblDrink4.Text = str Then
            btnDrink4.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnDrink4.ImageIndex = i
            btnDrink4.Enabled = False
        Else
            btnDrink4.ImageList = ImageListDrink
            btnDrink4.ImageKey = btnDrink4.Tag
            btnDrink4.Enabled = True
        End If
        If lblDrink5.Text = str Then
            btnDrink5.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnDrink5.ImageIndex = i
            btnDrink5.Enabled = False
        Else
            btnDrink5.ImageList = ImageListDrink
            btnDrink5.ImageKey = btnDrink5.Tag
            btnDrink5.Enabled = True
        End If
        If lblDrink6.Text = str Then
            btnDrink6.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnDrink6.ImageIndex = i
            btnDrink6.Enabled = False
        Else
            btnDrink6.ImageList = ImageListDrink
            btnDrink6.ImageKey = btnDrink6.Tag
            btnDrink6.Enabled = True
        End If
        If lblDrink7.Text = str Then
            btnDrink7.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnDrink7.ImageIndex = i
            btnDrink7.Enabled = False
        Else
            btnDrink7.ImageList = ImageListDrink
            btnDrink7.ImageKey = btnDrink7.Tag
            btnDrink7.Enabled = True
        End If
        If lblDrink8.Text = str Then
            btnDrink8.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnDrink8.ImageIndex = i
            btnDrink8.Enabled = False
        Else
            btnDrink8.ImageList = ImageListDrink
            btnDrink8.ImageKey = btnDrink8.Tag
            btnDrink8.Enabled = True
        End If
        If lblDrink9.Text = str Then
            btnDrink9.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnDrink9.ImageIndex = i
            btnDrink9.Enabled = False
        Else
            btnDrink9.ImageList = ImageListDrink
            btnDrink9.ImageKey = btnDrink9.Tag
            btnDrink9.Enabled = True
        End If
        If lblDrink10.Text = str Then
            btnDrink10.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnDrink10.ImageIndex = i
            btnDrink10.Enabled = False
        Else
            btnDrink10.ImageList = ImageListDrink
            btnDrink10.ImageKey = btnDrink10.Tag
            btnDrink10.Enabled = True
        End If
        If lblDrink11.Text = str Then
            btnDrink11.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnDrink11.ImageIndex = i
            btnDrink11.Enabled = False
        Else
            btnDrink11.ImageList = ImageListDrink
            btnDrink11.ImageKey = btnDrink11.Tag
            btnDrink11.Enabled = True
        End If
        If lblDrink12.Text = str Then
            btnDrink12.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnDrink12.ImageIndex = i
            btnDrink12.Enabled = False
        Else
            btnDrink12.ImageList = ImageListDrink
            btnDrink12.ImageKey = btnDrink12.Tag
            btnDrink12.Enabled = True
        End If
        If lblDrink13.Text = str Then
            btnDrink13.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnDrink13.ImageIndex = i
            btnDrink13.Enabled = False
        Else
            btnDrink13.ImageList = ImageListDrink
            btnDrink13.ImageKey = btnDrink13.Tag
            btnDrink13.Enabled = True
        End If
        If lblDrink14.Text = str Then
            btnDrink14.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnDrink14.ImageIndex = i
            btnDrink14.Enabled = False
        Else
            btnDrink14.ImageList = ImageListDrink
            btnDrink14.ImageKey = btnDrink14.Tag
            btnDrink14.Enabled = True
        End If
        If lblDrink15.Text = str Then
            btnDrink15.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnDrink15.ImageIndex = i
            btnDrink15.Enabled = False
        Else
            btnDrink15.ImageList = ImageListDrink
            btnDrink15.ImageKey = btnDrink15.Tag
            btnDrink15.Enabled = True
        End If
        If lblDrink16.Text = str Then
            btnDrink16.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnDrink16.ImageIndex = i
            btnDrink16.Enabled = False
        Else
            btnDrink16.ImageList = ImageListDrink
            btnDrink16.ImageKey = btnDrink16.Tag
            btnDrink16.Enabled = True
        End If

        If lblSpecial1.Text = str Then
            btnSpecial1.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnSpecial1.ImageList = ImageList1
            btnSpecial1.ImageIndex = i
            btnSpecial1.Enabled = False
        Else
            btnSpecial1.ImageList = ImageListSpecial
            btnSpecial1.ImageKey = btnSpecial1.Tag
            btnSpecial1.Enabled = True
        End If
        If lblSpecial2.Text = str Then
            btnSpecial2.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnSpecial2.ImageIndex = i
            btnSpecial2.Enabled = False
        Else
            btnSpecial2.ImageList = ImageListSpecial
            btnSpecial2.ImageKey = btnSpecial2.Tag
            btnSpecial2.Enabled = True
        End If
        If lblSpecial3.Text = str Then
            btnSpecial3.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnSpecial3.ImageIndex = i
            btnSpecial3.Enabled = False
        Else
            btnSpecial3.ImageList = ImageListSpecial
            btnSpecial3.ImageKey = btnSpecial3.Tag
            btnSpecial3.Enabled = True
        End If
        If lblSpecial4.Text = str Then
            btnSpecial4.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnSpecial4.ImageIndex = i
            btnSpecial4.Enabled = False
        Else
            btnSpecial4.ImageList = ImageListSpecial
            btnSpecial4.ImageKey = btnSpecial4.Tag
            btnSpecial4.Enabled = True
        End If
        If lblSpecial5.Text = str Then
            btnSpecial5.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnSpecial5.ImageIndex = i
            btnSpecial5.Enabled = False
        Else
            btnSpecial5.ImageList = ImageListSpecial
            btnSpecial5.ImageKey = btnSpecial5.Tag
            btnSpecial5.Enabled = True
        End If
        If lblSpecial6.Text = str Then
            btnSpecial6.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnSpecial6.ImageIndex = i
            btnSpecial6.Enabled = False
        Else
            btnSpecial6.ImageList = ImageListSpecial
            btnSpecial6.ImageKey = btnSpecial6.Tag
            btnSpecial6.Enabled = True
        End If
        If lblSpecial7.Text = str Then
            btnSpecial7.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnSpecial7.ImageIndex = i
            btnSpecial7.Enabled = False
        Else
            btnSpecial7.ImageList = ImageListSpecial
            btnSpecial7.ImageKey = btnSpecial7.Tag
            btnSpecial7.Enabled = True
        End If
        If lblSpecial8.Text = str Then
            btnSpecial8.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnSpecial8.ImageIndex = i
            btnSpecial8.Enabled = False
        Else
            btnSpecial8.ImageList = ImageListSpecial
            btnSpecial8.ImageKey = btnSpecial8.Tag
            btnSpecial8.Enabled = True
        End If
        If lblSpecial9.Text = str Then
            btnSpecial9.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnSpecial9.ImageIndex = i
            btnSpecial9.Enabled = False
        Else
            btnSpecial9.ImageList = ImageListSpecial
            btnSpecial9.ImageKey = btnSpecial9.Tag
            btnSpecial9.Enabled = True
        End If
        If lblSpecial10.Text = str Then
            btnSpecial10.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnSpecial10.ImageIndex = i
            btnSpecial10.Enabled = False
        Else
            btnSpecial10.ImageList = ImageListSpecial
            btnSpecial10.ImageKey = btnSpecial10.Tag
            btnSpecial10.Enabled = True
        End If
        If lblSpecial11.Text = str Then
            btnSpecial11.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnSpecial11.ImageIndex = i
            btnSpecial11.Enabled = False
        Else
            btnSpecial11.ImageList = ImageListSpecial
            btnSpecial11.ImageKey = btnSpecial11.Tag
            btnSpecial11.Enabled = True
        End If
        If lblSpecial12.Text = str Then
            btnSpecial12.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnSpecial12.ImageIndex = i
            btnSpecial12.Enabled = False
        Else
            btnSpecial12.ImageList = ImageListSpecial
            btnSpecial12.ImageKey = btnSpecial12.Tag
            btnSpecial12.Enabled = True
        End If
        If lblSpecial13.Text = str Then
            btnSpecial13.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnSpecial13.ImageIndex = i
            btnSpecial13.Enabled = False
        Else
            btnSpecial13.ImageList = ImageListSpecial
            btnSpecial13.ImageKey = btnSpecial13.Tag
            btnSpecial13.Enabled = True
        End If
        If lblSpecial14.Text = str Then
            btnSpecial14.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnSpecial14.ImageIndex = i
            btnSpecial14.Enabled = False
        Else
            btnSpecial14.ImageList = ImageListSpecial
            btnSpecial14.ImageKey = btnSpecial14.Tag
            btnSpecial14.Enabled = True
        End If
        If lblSpecial15.Text = str Then
            btnSpecial15.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnSpecial15.ImageIndex = i
            btnSpecial15.Enabled = False
        Else
            btnSpecial15.ImageList = ImageListSpecial
            btnSpecial15.ImageKey = btnSpecial15.Tag
            btnSpecial15.Enabled = True
        End If
        If lblSpecial16.Text = str Then
            btnSpecial16.Text = "NOT AVAILABLE" & vbCrLf & vbCrLf
            btnSpecial16.ImageIndex = i
            btnSpecial16.Enabled = False
        Else
            btnSpecial16.ImageList = ImageListSpecial
            btnSpecial16.ImageKey = btnSpecial16.Tag
            btnSpecial16.Enabled = True
        End If

    End Sub
    Private Sub Clear_Product(ByRef ClrPath As String)
        Dim Clr As String = Application.StartupPath & "\Burger\" & ClrPath & ".txt"
        Dim reader As New StreamReader(Clr)
        Dim pname As String = reader.ReadLine
        reader.Close()
        Dim writer As New StreamWriter(Clr)
        writer.WriteLine(pname)
        writer.WriteLine("00.00")
        writer.Close()
    End Sub
    Private Sub Clear_Product1(ByRef ClrPath As String)
        Dim Clr As String = Application.StartupPath & "\Drink\" & ClrPath & ".txt"
        Dim reader As New StreamReader(Clr)
        Dim pname As String = reader.ReadLine
        reader.Close()
        Dim writer As New StreamWriter(Clr)
        writer.WriteLine(pname)
        writer.WriteLine("00.00")
        writer.Close()
    End Sub
    Private Sub Clear_Product2(ByRef ClrPath As String)
        Dim Clr As String = Application.StartupPath & "\Special\" & ClrPath & ".txt"
        Dim reader As New StreamReader(Clr)
        Dim pname As String = reader.ReadLine
        reader.Close()
        Dim writer As New StreamWriter(Clr)
        writer.WriteLine(pname)
        writer.WriteLine("00.00")
        writer.Close()
    End Sub
    Private Sub Load_Datagrid()
        Try
            With dtgRecord
                Dim txt As New StreamReader(Application.StartupPath & "\Record\record.txt")
                Dim txt1 As New TextBox
                txt1.Text = txt.ReadToEnd
                For r As Integer = 0 To txt1.Lines.Count - 2
                    .Rows.Add(txt1.Lines(r).Split("|"))
                Next
                txt.Close()
                lblRecords.Text = "Records: " & .RowCount
            End With
        Catch ex As Exception
        End Try
    End Sub
    Private Sub Search_Datagrid_RNo()
        Try
            With dtgRecord
                For Each dgrow As DataGridViewRow In .Rows
                    dgrow.Visible = True
                    If dgrow.Cells(0).Value.ToString.Contains(txtSearch.Text) Then
                        dgrow.Visible = True
                    Else
                        dgrow.Visible = False
                    End If
                Next
                lblRecords.Text = "Records: " & .RowCount
            End With
        Catch ex As Exception
        End Try
    End Sub
    Private Sub Search_Datagrid_RDate()
        Try
            With dtgRecord
                For Each dgrow As DataGridViewRow In .Rows
                    dgrow.Visible = True
                    If dgrow.Cells(1).Value.ToString.ToLower.Contains(txtSearch.Text) Then
                        dgrow.Visible = True
                    Else
                        dgrow.Visible = False
                    End If
                Next
                lblRecords.Text = "Records: " & .RowCount
            End With
        Catch ex As Exception
        End Try
    End Sub
    Public Sub Filter_Record()
        If txtSearchby.SelectedItem = "Receipt No" Then
            Search_Datagrid_RNo()
        ElseIf txtSearchby.SelectedItem = "Date" Then
            Search_Datagrid_RDate()
        Else
            Load_Datagrid()
        End If
    End Sub
#End Region

#Region " Removing Data "
    Private Sub BtnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnRemove.Click
        Remove_Product(btnBurger1.Text, Decimal.Parse(lblBurger1.Text))
        Remove_Product(btnBurger2.Text, Decimal.Parse(lblBurger2.Text))
        Remove_Product(btnBurger3.Text, Decimal.Parse(lblBurger3.Text))
        Remove_Product(btnBurger4.Text, Decimal.Parse(lblBurger4.Text))
        Remove_Product(btnBurger5.Text, Decimal.Parse(lblBurger5.Text))
        Remove_Product(btnBurger6.Text, Decimal.Parse(lblBurger6.Text))
        Remove_Product(btnBurger7.Text, Decimal.Parse(lblBurger7.Text))
        Remove_Product(btnBurger8.Text, Decimal.Parse(lblBurger8.Text))
        Remove_Product(btnBurger9.Text, Decimal.Parse(lblBurger9.Text))
        Remove_Product(btnBurger10.Text, Decimal.Parse(lblBurger10.Text))
        Remove_Product(btnBurger11.Text, Decimal.Parse(lblBurger11.Text))
        Remove_Product(btnBurger12.Text, Decimal.Parse(lblBurger12.Text))
        Remove_Product(btnBurger13.Text, Decimal.Parse(lblBurger13.Text))
        Remove_Product(btnBurger14.Text, Decimal.Parse(lblBurger14.Text))
        Remove_Product(btnBurger15.Text, Decimal.Parse(lblBurger15.Text))
        Remove_Product(btnBurger16.Text, Decimal.Parse(lblBurger16.Text))

        Remove_Product(btnDrink1.Text, Decimal.Parse(lblDrink1.Text))
        Remove_Product(btnDrink2.Text, Decimal.Parse(lblDrink2.Text))
        Remove_Product(btnDrink3.Text, Decimal.Parse(lblDrink3.Text))
        Remove_Product(btnDrink4.Text, Decimal.Parse(lblDrink4.Text))
        Remove_Product(btnDrink5.Text, Decimal.Parse(lblDrink5.Text))
        Remove_Product(btnDrink6.Text, Decimal.Parse(lblDrink6.Text))
        Remove_Product(btnDrink7.Text, Decimal.Parse(lblDrink7.Text))
        Remove_Product(btnDrink8.Text, Decimal.Parse(lblDrink8.Text))
        Remove_Product(btnDrink9.Text, Decimal.Parse(lblDrink9.Text))
        Remove_Product(btnDrink10.Text, Decimal.Parse(lblDrink10.Text))
        Remove_Product(btnDrink11.Text, Decimal.Parse(lblDrink11.Text))
        Remove_Product(btnDrink12.Text, Decimal.Parse(lblDrink12.Text))
        Remove_Product(btnDrink13.Text, Decimal.Parse(lblDrink13.Text))
        Remove_Product(btnDrink14.Text, Decimal.Parse(lblDrink14.Text))
        Remove_Product(btnDrink15.Text, Decimal.Parse(lblDrink15.Text))
        Remove_Product(btnDrink16.Text, Decimal.Parse(lblDrink16.Text))

        Remove_Product(btnSpecial1.Text, Decimal.Parse(lblSpecial1.Text))
        Remove_Product(btnSpecial2.Text, Decimal.Parse(lblSpecial2.Text))
        Remove_Product(btnSpecial3.Text, Decimal.Parse(lblSpecial3.Text))
        Remove_Product(btnSpecial4.Text, Decimal.Parse(lblSpecial4.Text))
        Remove_Product(btnSpecial5.Text, Decimal.Parse(lblSpecial5.Text))
        Remove_Product(btnSpecial6.Text, Decimal.Parse(lblSpecial6.Text))
        Remove_Product(btnSpecial7.Text, Decimal.Parse(lblSpecial7.Text))
        Remove_Product(btnSpecial8.Text, Decimal.Parse(lblSpecial8.Text))
        Remove_Product(btnSpecial9.Text, Decimal.Parse(lblSpecial9.Text))
        Remove_Product(btnSpecial10.Text, Decimal.Parse(lblSpecial10.Text))
        Remove_Product(btnSpecial11.Text, Decimal.Parse(lblSpecial11.Text))
        Remove_Product(btnSpecial12.Text, Decimal.Parse(lblSpecial12.Text))
        Remove_Product(btnSpecial13.Text, Decimal.Parse(lblSpecial13.Text))
        Remove_Product(btnSpecial14.Text, Decimal.Parse(lblSpecial14.Text))
        Remove_Product(btnSpecial15.Text, Decimal.Parse(lblSpecial15.Text))
        Remove_Product(btnSpecial16.Text, Decimal.Parse(lblSpecial16.Text))
    End Sub
    Public Sub BtnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnClear.Click
        LstOrder.Items.Clear()
        txtAmount.Text = "0.00"
    End Sub
#End Region

#Region " Adding Data "

#Region " Burgers Data "
    Private Sub btnBurger1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBurger1.Click
        Load_Product(btnBurger1.Text, Decimal.Parse(lblBurger1.Text))
    End Sub

    Private Sub btnBurger2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBurger2.Click
        Load_Product(btnBurger2.Text, Decimal.Parse(lblBurger2.Text))
    End Sub

    Private Sub btnBurger3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBurger3.Click
        Load_Product(btnBurger3.Text, Decimal.Parse(lblBurger3.Text))
    End Sub

    Private Sub btnBurger4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBurger4.Click
        Load_Product(btnBurger4.Text, Decimal.Parse(lblBurger4.Text))
    End Sub

    Private Sub btnBurger5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBurger5.Click
        Load_Product(btnBurger5.Text, Decimal.Parse(lblBurger5.Text))
    End Sub

    Private Sub btnBurger6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBurger6.Click
        Load_Product(btnBurger6.Text, Decimal.Parse(lblBurger6.Text))
    End Sub

    Private Sub btnBurger7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBurger7.Click
        Load_Product(btnBurger7.Text, Decimal.Parse(lblBurger7.Text))
    End Sub

    Private Sub btnBurger8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBurger8.Click
        Load_Product(btnBurger8.Text, Decimal.Parse(lblBurger8.Text))
    End Sub

    Private Sub btnBurger9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBurger9.Click
        Load_Product(btnBurger9.Text, Decimal.Parse(lblBurger9.Text))
    End Sub

    Private Sub btnBurger10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBurger10.Click
        Load_Product(btnBurger10.Text, Decimal.Parse(lblBurger10.Text))
    End Sub

    Private Sub btnBurger11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBurger11.Click
        Load_Product(btnBurger11.Text, Decimal.Parse(lblBurger11.Text))
    End Sub

    Private Sub btnBurger12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBurger12.Click
        Load_Product(btnBurger12.Text, Decimal.Parse(lblBurger12.Text))
    End Sub

    Private Sub btnBurger13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBurger13.Click
        Load_Product(btnBurger13.Text, Decimal.Parse(lblBurger13.Text))
    End Sub

    Private Sub btnBurger14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBurger14.Click
        Load_Product(btnBurger14.Text, Decimal.Parse(lblBurger14.Text))
    End Sub

    Private Sub btnBurger15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBurger15.Click
        Load_Product(btnBurger15.Text, Decimal.Parse(lblBurger15.Text))
    End Sub

    Private Sub btnBurger16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBurger16.Click
        Load_Product(btnBurger16.Text, Decimal.Parse(lblBurger16.Text))
    End Sub
#End Region

#Region " Drinks Data "
    Private Sub btnDrink1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDrink1.Click
        Load_Product(btnDrink1.Text, Decimal.Parse(lblDrink1.Text))
    End Sub

    Private Sub btnDrink2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDrink2.Click
        Load_Product(btnDrink2.Text, Decimal.Parse(lblDrink2.Text))
    End Sub

    Private Sub btnDrink3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDrink3.Click
        Load_Product(btnDrink3.Text, Decimal.Parse(lblDrink3.Text))
    End Sub

    Private Sub btnDrink4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDrink4.Click
        Load_Product(btnDrink4.Text, Decimal.Parse(lblDrink4.Text))
    End Sub

    Private Sub btnDrink5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDrink5.Click
        Load_Product(btnDrink5.Text, Decimal.Parse(lblDrink5.Text))
    End Sub

    Private Sub btnDrink6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDrink6.Click
        Load_Product(btnDrink6.Text, Decimal.Parse(lblDrink6.Text))
    End Sub

    Private Sub btnDrink7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDrink7.Click
        Load_Product(btnDrink7.Text, Decimal.Parse(lblDrink7.Text))
    End Sub

    Private Sub btnDrink8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDrink8.Click
        Load_Product(btnDrink8.Text, Decimal.Parse(lblDrink8.Text))
    End Sub

    Private Sub btnDrink9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDrink9.Click
        Load_Product(btnDrink9.Text, Decimal.Parse(lblDrink9.Text))
    End Sub

    Private Sub btnDrink10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDrink10.Click
        Load_Product(btnDrink10.Text, Decimal.Parse(lblDrink10.Text))
    End Sub

    Private Sub btnDrink11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDrink11.Click
        Load_Product(btnDrink11.Text, Decimal.Parse(lblDrink11.Text))
    End Sub

    Private Sub btnDrink12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDrink12.Click
        Load_Product(btnDrink12.Text, Decimal.Parse(lblDrink12.Text))
    End Sub

    Private Sub btnDrink13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDrink13.Click
        Load_Product(btnDrink13.Text, Decimal.Parse(lblDrink13.Text))
    End Sub

    Private Sub btnDrink14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDrink14.Click
        Load_Product(btnDrink14.Text, Decimal.Parse(lblDrink14.Text))
    End Sub

    Private Sub btnDrink15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDrink15.Click
        Load_Product(btnDrink15.Text, Decimal.Parse(lblDrink15.Text))
    End Sub

    Private Sub btnDrink16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDrink16.Click
        Load_Product(btnDrink16.Text, Decimal.Parse(lblDrink16.Text))
    End Sub
#End Region

#Region " Special Data "
    Private Sub btnSpecial1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSpecial1.Click
        Load_Product(btnSpecial1.Text, Decimal.Parse(lblSpecial1.Text))
    End Sub

    Private Sub btnSpecial2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSpecial2.Click
        Load_Product(btnSpecial2.Text, Decimal.Parse(lblSpecial2.Text))
    End Sub

    Private Sub btnSpecial3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSpecial3.Click
        Load_Product(btnSpecial3.Text, Decimal.Parse(lblSpecial3.Text))
    End Sub

    Private Sub btnSpecial4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSpecial4.Click
        Load_Product(btnSpecial4.Text, Decimal.Parse(lblSpecial4.Text))
    End Sub

    Private Sub btnSpecial5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSpecial5.Click
        Load_Product(btnSpecial5.Text, Decimal.Parse(lblSpecial5.Text))
    End Sub

    Private Sub btnSpecial6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSpecial6.Click
        Load_Product(btnSpecial6.Text, Decimal.Parse(lblSpecial6.Text))
    End Sub

    Private Sub btnSpecial7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSpecial7.Click
        Load_Product(btnSpecial7.Text, Decimal.Parse(lblSpecial7.Text))
    End Sub

    Private Sub btnSpecial8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSpecial8.Click
        Load_Product(btnSpecial8.Text, Decimal.Parse(lblSpecial8.Text))
    End Sub

    Private Sub btnSpecial9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSpecial9.Click
        Load_Product(btnSpecial9.Text, Decimal.Parse(lblSpecial9.Text))
    End Sub

    Private Sub btnSpecial10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSpecial10.Click
        Load_Product(btnSpecial10.Text, Decimal.Parse(lblSpecial10.Text))
    End Sub

    Private Sub btnSpecial11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSpecial11.Click
        Load_Product(btnSpecial11.Text, Decimal.Parse(lblSpecial11.Text))
    End Sub

    Private Sub btnSpecial12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSpecial12.Click
        Load_Product(btnSpecial12.Text, Decimal.Parse(lblSpecial12.Text))
    End Sub

    Private Sub btnSpecial13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSpecial13.Click
        Load_Product(btnSpecial13.Text, Decimal.Parse(lblSpecial13.Text))
    End Sub

    Private Sub btnSpecial14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSpecial14.Click
        Load_Product(btnSpecial14.Text, Decimal.Parse(lblSpecial14.Text))
    End Sub

    Private Sub btnSpecial15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSpecial15.Click
        Load_Product(btnSpecial15.Text, Decimal.Parse(lblSpecial15.Text))
    End Sub

    Private Sub btnSpecial16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSpecial16.Click
        Load_Product(btnSpecial16.Text, Decimal.Parse(lblSpecial16.Text))
    End Sub
#End Region

#End Region

#Region " Editing Data "

    Private Sub btnEditBurger1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditBurger1.Click
        With frmEdit
            .Edit_Product(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnBurger1.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListBurger
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditBurger2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditBurger2.Click
        With frmEdit
            .Edit_Product(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnBurger2.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListBurger
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditBurger3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditBurger3.Click
        With frmEdit
            .Edit_Product(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnBurger3.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListBurger
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditBurger4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditBurger4.Click
        With frmEdit
            .Edit_Product(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnBurger4.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListBurger
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditBurger5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditBurger5.Click
        With frmEdit
            .Edit_Product(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnBurger5.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListBurger
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditBurger6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditBurger6.Click
        With frmEdit
            .Edit_Product(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnBurger6.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListBurger
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditBurger7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditBurger7.Click
        With frmEdit
            .Edit_Product(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnBurger7.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListBurger
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditBurger8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditBurger8.Click
        With frmEdit
            .Edit_Product(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnBurger8.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListBurger
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditBurger9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditBurger9.Click
        With frmEdit
            .Edit_Product(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnBurger9.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListBurger
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditBurger10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditBurger10.Click
        With frmEdit
            .Edit_Product(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnBurger10.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListBurger
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditBurger11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditBurger11.Click
        With frmEdit
            .Edit_Product(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnBurger11.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListBurger
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditBurger12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditBurger12.Click
        With frmEdit
            .Edit_Product(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnBurger12.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListBurger
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditBurger13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditBurger13.Click
        With frmEdit
            .Edit_Product(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnBurger13.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListBurger
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditBurger14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditBurger14.Click
        With frmEdit
            .Edit_Product(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnBurger14.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListBurger
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditBurger15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditBurger15.Click
        With frmEdit
            .Edit_Product(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnBurger15.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListBurger
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditBurger16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditBurger16.Click
        With frmEdit
            .Edit_Product(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnBurger16.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListBurger
            .ShowDialog()
        End With
    End Sub

    Private Sub btnEditDrink1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditDrink1.Click
        With frmEdit
            .Edit_Product1(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnDrink1.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListDrink
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditDrink2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditDrink2.Click
        With frmEdit
            .Edit_Product1(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnDrink2.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListDrink
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditDrink3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditDrink3.Click
        With frmEdit
            .Edit_Product1(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnDrink3.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListDrink
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditDrink4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditDrink4.Click
        With frmEdit
            .Edit_Product1(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnDrink4.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListDrink
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditDrink5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditDrink5.Click
        With frmEdit
            .Edit_Product1(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnDrink5.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListDrink
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditDrink6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditDrink6.Click
        With frmEdit
            .Edit_Product1(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnDrink6.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListDrink
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditDrink7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditDrink7.Click
        With frmEdit
            .Edit_Product1(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnDrink7.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListDrink
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditDrink8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditDrink8.Click
        With frmEdit
            .Edit_Product1(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnDrink8.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListDrink
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditDrink9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditDrink9.Click
        With frmEdit
            .Edit_Product1(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnDrink9.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListDrink
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditDrink10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditDrink10.Click
        With frmEdit
            .Edit_Product1(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnDrink10.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListDrink
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditDrink11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditDrink11.Click
        With frmEdit
            .Edit_Product1(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnDrink11.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListDrink
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditDrink12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditDrink12.Click
        With frmEdit
            .Edit_Product1(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnDrink12.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListDrink
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditDrink13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditDrink13.Click
        With frmEdit
            .Edit_Product1(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnDrink13.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListDrink
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditDrink14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditDrink14.Click
        With frmEdit
            .Edit_Product1(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnDrink14.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListDrink
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditDrink15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditDrink15.Click
        With frmEdit
            .Edit_Product1(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnDrink15.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListDrink
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditDrink16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditDrink16.Click
        With frmEdit
            .Edit_Product1(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnDrink16.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListDrink
            .ShowDialog()
        End With
    End Sub

    Private Sub btnEditSpecial1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditSpecial1.Click
        With frmEdit
            .Edit_Product2(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnSpecial1.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListSpecial
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditSpecial2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditSpecial2.Click
        With frmEdit
            .Edit_Product2(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnSpecial2.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListSpecial
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditSpecial3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditSpecial3.Click
        With frmEdit
            .Edit_Product2(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnSpecial3.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListSpecial
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditSpecial4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditSpecial4.Click
        With frmEdit
            .Edit_Product2(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnSpecial4.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListSpecial
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditSpecial5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditSpecial5.Click
        With frmEdit
            .Edit_Product2(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnSpecial5.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListSpecial
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditSpecial6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditSpecial6.Click
        With frmEdit
            .Edit_Product2(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnSpecial6.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListSpecial
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditSpecial7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditSpecial7.Click
        With frmEdit
            .Edit_Product2(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnSpecial7.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListSpecial
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditSpecial8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditSpecial8.Click
        With frmEdit
            .Edit_Product2(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnSpecial8.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListSpecial
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditSpecial9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditSpecial9.Click
        With frmEdit
            .Edit_Product2(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnSpecial9.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListSpecial
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditSpecial10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditSpecial10.Click
        With frmEdit
            .Edit_Product2(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnSpecial10.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListSpecial
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditSpecial11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditSpecial11.Click
        With frmEdit
            .Edit_Product2(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnSpecial11.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListSpecial
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditSpecial12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditSpecial12.Click
        With frmEdit
            .Edit_Product2(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnSpecial12.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListSpecial
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditSpecial13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditSpecial13.Click
        With frmEdit
            .Edit_Product2(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnSpecial13.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListSpecial
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditSpecial14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditSpecial14.Click
        With frmEdit
            .Edit_Product2(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnSpecial14.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListSpecial
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditSpecial15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditSpecial15.Click
        With frmEdit
            .Edit_Product2(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnSpecial15.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListSpecial
            .ShowDialog()
        End With
    End Sub
    Private Sub btnEditSpecial16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditSpecial16.Click
        With frmEdit
            .Edit_Product2(frmEdit.txtPname.Text, frmEdit.txtPprice.Text, Me.btnSpecial16.Name, frmEdit.txtImg.Text)
            .PicImg.ImageList = Me.ImageListSpecial
            .ShowDialog()
        End With
    End Sub

#End Region

#Region " Clear Data "
    Private Sub btnClearBurger1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearBurger1.Click
        Clear_Product(btnBurger1.Name)
        auto()
    End Sub
    Private Sub btnClearBurger2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearBurger2.Click
        Clear_Product(btnBurger2.Name)
        auto()
    End Sub
    Private Sub btnClearBurger3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearBurger3.Click
        Clear_Product(btnBurger3.Name)
        auto()
    End Sub
    Private Sub btnClearBurger4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearBurger4.Click
        Clear_Product(btnBurger4.Name)
        auto()
    End Sub
    Private Sub btnClearBurger5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearBurger5.Click
        Clear_Product(btnBurger5.Name)
        auto()
    End Sub
    Private Sub btnClearBurger6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearBurger6.Click
        Clear_Product(btnBurger6.Name)
        auto()
    End Sub
    Private Sub btnClearBurger7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearBurger7.Click
        Clear_Product(btnBurger7.Name)
        auto()
    End Sub
    Private Sub btnClearBurger8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearBurger8.Click
        Clear_Product(btnBurger8.Name)
        auto()
    End Sub
    Private Sub btnClearBurger9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearBurger9.Click
        Clear_Product(btnBurger9.Name)
        auto()
    End Sub
    Private Sub btnClearBurger10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearBurger10.Click
        Clear_Product(btnBurger10.Name)
        auto()
    End Sub
    Private Sub btnClearBurger11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearBurger11.Click
        Clear_Product(btnBurger11.Name)
        auto()
    End Sub
    Private Sub btnClearBurger12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearBurger12.Click
        Clear_Product(btnBurger12.Name)
        auto()
    End Sub
    Private Sub btnClearBurger13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearBurger13.Click
        Clear_Product(btnBurger13.Name)
        auto()
    End Sub
    Private Sub btnClearBurger14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearBurger14.Click
        Clear_Product(btnBurger14.Name)
        auto()
    End Sub
    Private Sub btnClearBurger15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearBurger15.Click
        Clear_Product(btnBurger15.Name)
        auto()
    End Sub
    Private Sub btnClearBurger16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearBurger16.Click
        Clear_Product(btnBurger16.Name)
        auto()
    End Sub

    Private Sub btnClearDrink1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearDrink1.Click
        Clear_product1(btnDrink1.Name)
        auto()
    End Sub
    Private Sub btnClearDrink2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearDrink2.Click
        Clear_product1(btnDrink2.Name)
        auto()
    End Sub
    Private Sub btnClearDrink3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearDrink3.Click
        Clear_product1(btnDrink3.Name)
        auto()
    End Sub
    Private Sub btnClearDrink4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearDrink4.Click
        Clear_product1(btnDrink4.Name)
        auto()
    End Sub
    Private Sub btnClearDrink5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearDrink5.Click
        Clear_product1(btnDrink5.Name)
        auto()
    End Sub
    Private Sub btnClearDrink6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearDrink6.Click
        Clear_product1(btnDrink6.Name)
        auto()
    End Sub
    Private Sub btnClearDrink7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearDrink7.Click
        Clear_product1(btnDrink7.Name)
        auto()
    End Sub
    Private Sub btnClearDrink8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearDrink8.Click
        Clear_product1(btnDrink8.Name)
        auto()
    End Sub
    Private Sub btnClearDrink9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearDrink9.Click
        Clear_product1(btnDrink9.Name)
        auto()
    End Sub
    Private Sub btnClearDrink10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearDrink10.Click
        Clear_product1(btnDrink10.Name)
        auto()
    End Sub
    Private Sub btnClearDrink11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearDrink11.Click
        Clear_product1(btnDrink11.Name)
        auto()
    End Sub
    Private Sub btnClearDrink12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearDrink12.Click
        Clear_product1(btnDrink12.Name)
        auto()
    End Sub
    Private Sub btnClearDrink13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearDrink13.Click
        Clear_product1(btnDrink13.Name)
        auto()
    End Sub
    Private Sub btnClearDrink14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearDrink14.Click
        Clear_product1(btnDrink14.Name)
        auto()
    End Sub
    Private Sub btnClearDrink15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearDrink15.Click
        Clear_product1(btnDrink15.Name)
        auto()
    End Sub
    Private Sub btnClearDrink16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearDrink16.Click
        Clear_product1(btnDrink16.Name)
        auto()
    End Sub

    Private Sub btnClearSpecial1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearSpecial1.Click
        Clear_Product2(btnSpecial1.Name)
        auto()
    End Sub
    Private Sub btnClearSpecial2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearSpecial2.Click
        Clear_Product2(btnSpecial2.Name)
        auto()
    End Sub
    Private Sub btnClearSpecial3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearSpecial3.Click
        Clear_Product2(btnSpecial3.Name)
        auto()
    End Sub
    Private Sub btnClearSpecial4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearSpecial4.Click
        Clear_Product2(btnSpecial4.Name)
        auto()
    End Sub
    Private Sub btnClearSpecial5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearSpecial5.Click
        Clear_Product2(btnSpecial5.Name)
        auto()
    End Sub
    Private Sub btnClearSpecial6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearSpecial6.Click
        Clear_Product2(btnSpecial6.Name)
        auto()
    End Sub
    Private Sub btnClearSpecial7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearSpecial7.Click
        Clear_Product2(btnSpecial7.Name)
        auto()
    End Sub
    Private Sub btnClearSpecial8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearSpecial8.Click
        Clear_Product2(btnSpecial8.Name)
        auto()
    End Sub
    Private Sub btnClearSpecial9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearSpecial9.Click
        Clear_Product2(btnSpecial9.Name)
        auto()
    End Sub
    Private Sub btnClearSpecial10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearSpecial10.Click
        Clear_Product2(btnSpecial10.Name)
        auto()
    End Sub
    Private Sub btnClearSpecial11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearSpecial11.Click
        Clear_Product2(btnSpecial11.Name)
        auto()
    End Sub
    Private Sub btnClearSpecial12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearSpecial12.Click
        Clear_Product2(btnSpecial12.Name)
        auto()
    End Sub
    Private Sub btnClearSpecial13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearSpecial13.Click
        Clear_Product2(btnSpecial13.Name)
        auto()
    End Sub
    Private Sub btnClearSpecial14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearSpecial14.Click
        Clear_Product2(btnSpecial14.Name)
        auto()
    End Sub
    Private Sub btnClearSpecial15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearSpecial15.Click
        Clear_Product2(btnSpecial15.Name)
        auto()
    End Sub
    Private Sub btnClearSpecial16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearSpecial16.Click
        Clear_Product2(btnSpecial16.Name)
        auto()
    End Sub
#End Region

#Region " Form Events "

    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        End
    End Sub

    Private Sub BtnPayment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPayment.Click
        If Val(txtAmount.Text) <= 0 Then
            MessageBox.Show("Please select a Product before adding payment.", "Invalid Transaction", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            With frmPayment
                .txtAmount.Text = Me.txtAmount.Text
                For i = 0 To Me.LstOrder.Items.Count - 1
                    .LstOrder.Items.Add(LstOrder.Items(i))
                Next i
                .txtCash.Focus()
                .ShowDialog()
            End With
        End If
    End Sub

    Private Sub txtSearchby_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchby.TextChanged
        If txtSearchby.SelectedItem = "Receipt No" Then
            txtSearch.Mask = "BURGER-000000"
            txtSearch.Enabled = True
        ElseIf txtSearchby.SelectedItem = "Date" Then
            txtSearch.Mask = "00/00/0000"
            txtSearch.Text = Format(Date.Now, "MM/dd/yyyy")
            txtSearch.Enabled = True
        Else
            txtSearch.Mask = ""
            txtSearch.Enabled = False
        End If
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Filter_Record()
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        If dtgRecord.RowCount = Nothing Then
            MessageBox.Show("Sorry nothing to export into excel sheet.." & vbCrLf & "Please retrieve data in datagridview", "", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If
        Dim rowsTotal, colsTotal As Short
        Dim I, j, iC As Short
        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
        Dim xlApp As New Excel.Application

        Try
            Dim excelBook As Excel.Workbook = xlApp.Workbooks.Add
            Dim excelWorksheet As Excel.Worksheet = CType(excelBook.Worksheets(1), Excel.Worksheet)
            xlApp.Visible = True

            rowsTotal = dtgRecord.RowCount
            colsTotal = dtgRecord.Columns.Count - 1
            With excelWorksheet
                .Cells.Select()
                .Cells.Delete()
                For iC = 0 To colsTotal
                    .Cells(1, iC + 1).Value = dtgRecord.Columns(iC).HeaderText
                Next
                For I = 0 To rowsTotal - 1
                    For j = 0 To colsTotal
                        .Cells(I + 2, j + 1).value = dtgRecord.Rows(I).Cells(j).Value
                    Next j
                Next I
                .Rows("1:1").Font.FontStyle = "Bold"
                .Rows("1:1").Font.Size = 12

                .Cells.Columns.AutoFit()
                .Cells.Select()
                .Cells.EntireColumn.AutoFit()
                .Cells(1, 1).Select()
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            'RELEASE ALLOACTED RESOURCES
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
            xlApp = Nothing
        End Try
    End Sub

    Private Sub Btnrefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnrefresh.Click
        dtgRecord.Rows.Clear()
        Filter_Record()
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim writer As New StreamWriter(Application.StartupPath & "\Record\Record.txt")
        writer.WriteLine()
        writer.Close()
        dtgRecord.Rows.Clear()
        Filter_Record()
    End Sub

    Private Sub btnAdmin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdmin.Click
        frmChangePass.ShowDialog()
    End Sub

    Private Sub LinkAbout_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkAbout.LinkClicked
        frmAbout.ShowDialog()
    End Sub

#End Region
    

End Class