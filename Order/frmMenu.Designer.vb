﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMenu
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMenu))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnMin = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.LinkAbout = New System.Windows.Forms.LinkLabel()
        Me.btnAdmin = New System.Windows.Forms.Button()
        Me.btnLogout = New System.Windows.Forms.Button()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.lblTime = New System.Windows.Forms.Label()
        Me.lblDate = New System.Windows.Forms.Label()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.LogoutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.BtnCancel = New System.Windows.Forms.Button()
        Me.BtnPayment = New System.Windows.Forms.Button()
        Me.txtAmount = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.BtnClear = New System.Windows.Forms.Button()
        Me.BtnRemove = New System.Windows.Forms.Button()
        Me.LstOrder = New System.Windows.Forms.ListBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.TimerMenu = New System.Windows.Forms.Timer(Me.components)
        Me.ImageListBurger = New System.Windows.Forms.ImageList(Me.components)
        Me.ImageListDrink = New System.Windows.Forms.ImageList(Me.components)
        Me.ImageListSpecial = New System.Windows.Forms.ImageList(Me.components)
        Me.TabControl1 = New DevComponents.DotNetBar.TabControl()
        Me.TabControlPanel1 = New DevComponents.DotNetBar.TabControlPanel()
        Me.TabControl2 = New DevComponents.DotNetBar.TabControl()
        Me.TabControlPanel3 = New DevComponents.DotNetBar.TabControlPanel()
        Me.btnClearDrink16 = New System.Windows.Forms.Button()
        Me.btnClearDrink15 = New System.Windows.Forms.Button()
        Me.btnClearDrink14 = New System.Windows.Forms.Button()
        Me.btnClearDrink13 = New System.Windows.Forms.Button()
        Me.btnClearDrink12 = New System.Windows.Forms.Button()
        Me.btnClearDrink11 = New System.Windows.Forms.Button()
        Me.btnClearDrink10 = New System.Windows.Forms.Button()
        Me.btnClearDrink9 = New System.Windows.Forms.Button()
        Me.btnClearDrink8 = New System.Windows.Forms.Button()
        Me.btnClearDrink7 = New System.Windows.Forms.Button()
        Me.btnClearDrink6 = New System.Windows.Forms.Button()
        Me.btnClearDrink5 = New System.Windows.Forms.Button()
        Me.btnClearDrink4 = New System.Windows.Forms.Button()
        Me.btnClearDrink3 = New System.Windows.Forms.Button()
        Me.btnClearDrink2 = New System.Windows.Forms.Button()
        Me.btnClearDrink1 = New System.Windows.Forms.Button()
        Me.btnEditDrink16 = New System.Windows.Forms.Button()
        Me.btnEditDrink15 = New System.Windows.Forms.Button()
        Me.btnEditDrink14 = New System.Windows.Forms.Button()
        Me.btnEditDrink13 = New System.Windows.Forms.Button()
        Me.btnEditDrink12 = New System.Windows.Forms.Button()
        Me.btnEditDrink11 = New System.Windows.Forms.Button()
        Me.btnEditDrink10 = New System.Windows.Forms.Button()
        Me.btnEditDrink9 = New System.Windows.Forms.Button()
        Me.btnEditDrink8 = New System.Windows.Forms.Button()
        Me.btnEditDrink7 = New System.Windows.Forms.Button()
        Me.btnEditDrink6 = New System.Windows.Forms.Button()
        Me.btnEditDrink5 = New System.Windows.Forms.Button()
        Me.btnEditDrink4 = New System.Windows.Forms.Button()
        Me.btnEditDrink3 = New System.Windows.Forms.Button()
        Me.btnEditDrink2 = New System.Windows.Forms.Button()
        Me.btnEditDrink1 = New System.Windows.Forms.Button()
        Me.lblDrink16 = New System.Windows.Forms.Label()
        Me.lblDrink15 = New System.Windows.Forms.Label()
        Me.lblDrink14 = New System.Windows.Forms.Label()
        Me.lblDrink13 = New System.Windows.Forms.Label()
        Me.lblDrink12 = New System.Windows.Forms.Label()
        Me.lblDrink11 = New System.Windows.Forms.Label()
        Me.lblDrink10 = New System.Windows.Forms.Label()
        Me.lblDrink9 = New System.Windows.Forms.Label()
        Me.lblDrink8 = New System.Windows.Forms.Label()
        Me.lblDrink7 = New System.Windows.Forms.Label()
        Me.lblDrink6 = New System.Windows.Forms.Label()
        Me.lblDrink5 = New System.Windows.Forms.Label()
        Me.lblDrink4 = New System.Windows.Forms.Label()
        Me.lblDrink3 = New System.Windows.Forms.Label()
        Me.lblDrink2 = New System.Windows.Forms.Label()
        Me.lblDrink1 = New System.Windows.Forms.Label()
        Me.btnDrink16 = New System.Windows.Forms.Button()
        Me.btnDrink15 = New System.Windows.Forms.Button()
        Me.btnDrink14 = New System.Windows.Forms.Button()
        Me.btnDrink13 = New System.Windows.Forms.Button()
        Me.btnDrink12 = New System.Windows.Forms.Button()
        Me.btnDrink11 = New System.Windows.Forms.Button()
        Me.btnDrink10 = New System.Windows.Forms.Button()
        Me.btnDrink9 = New System.Windows.Forms.Button()
        Me.btnDrink8 = New System.Windows.Forms.Button()
        Me.btnDrink7 = New System.Windows.Forms.Button()
        Me.btnDrink6 = New System.Windows.Forms.Button()
        Me.btnDrink5 = New System.Windows.Forms.Button()
        Me.btnDrink4 = New System.Windows.Forms.Button()
        Me.btnDrink3 = New System.Windows.Forms.Button()
        Me.btnDrink2 = New System.Windows.Forms.Button()
        Me.btnDrink1 = New System.Windows.Forms.Button()
        Me.TabItem2 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel2 = New DevComponents.DotNetBar.TabControlPanel()
        Me.btnClearBurger16 = New System.Windows.Forms.Button()
        Me.btnClearBurger15 = New System.Windows.Forms.Button()
        Me.btnClearBurger14 = New System.Windows.Forms.Button()
        Me.btnClearBurger13 = New System.Windows.Forms.Button()
        Me.btnClearBurger12 = New System.Windows.Forms.Button()
        Me.btnClearBurger11 = New System.Windows.Forms.Button()
        Me.btnClearBurger10 = New System.Windows.Forms.Button()
        Me.btnClearBurger9 = New System.Windows.Forms.Button()
        Me.btnClearBurger8 = New System.Windows.Forms.Button()
        Me.btnClearBurger7 = New System.Windows.Forms.Button()
        Me.btnClearBurger6 = New System.Windows.Forms.Button()
        Me.btnClearBurger5 = New System.Windows.Forms.Button()
        Me.btnClearBurger4 = New System.Windows.Forms.Button()
        Me.btnClearBurger3 = New System.Windows.Forms.Button()
        Me.btnClearBurger2 = New System.Windows.Forms.Button()
        Me.btnClearBurger1 = New System.Windows.Forms.Button()
        Me.btnEditBurger16 = New System.Windows.Forms.Button()
        Me.btnEditBurger15 = New System.Windows.Forms.Button()
        Me.btnEditBurger14 = New System.Windows.Forms.Button()
        Me.btnEditBurger13 = New System.Windows.Forms.Button()
        Me.btnEditBurger12 = New System.Windows.Forms.Button()
        Me.btnEditBurger11 = New System.Windows.Forms.Button()
        Me.btnEditBurger10 = New System.Windows.Forms.Button()
        Me.btnEditBurger9 = New System.Windows.Forms.Button()
        Me.btnEditBurger8 = New System.Windows.Forms.Button()
        Me.btnEditBurger7 = New System.Windows.Forms.Button()
        Me.btnEditBurger6 = New System.Windows.Forms.Button()
        Me.btnEditBurger5 = New System.Windows.Forms.Button()
        Me.btnEditBurger4 = New System.Windows.Forms.Button()
        Me.btnEditBurger3 = New System.Windows.Forms.Button()
        Me.btnEditBurger2 = New System.Windows.Forms.Button()
        Me.btnEditBurger1 = New System.Windows.Forms.Button()
        Me.lblBurger16 = New System.Windows.Forms.Label()
        Me.lblBurger15 = New System.Windows.Forms.Label()
        Me.lblBurger14 = New System.Windows.Forms.Label()
        Me.lblBurger13 = New System.Windows.Forms.Label()
        Me.lblBurger12 = New System.Windows.Forms.Label()
        Me.lblBurger11 = New System.Windows.Forms.Label()
        Me.lblBurger10 = New System.Windows.Forms.Label()
        Me.lblBurger9 = New System.Windows.Forms.Label()
        Me.lblBurger8 = New System.Windows.Forms.Label()
        Me.lblBurger7 = New System.Windows.Forms.Label()
        Me.lblBurger6 = New System.Windows.Forms.Label()
        Me.lblBurger5 = New System.Windows.Forms.Label()
        Me.lblBurger4 = New System.Windows.Forms.Label()
        Me.lblBurger3 = New System.Windows.Forms.Label()
        Me.lblBurger2 = New System.Windows.Forms.Label()
        Me.lblBurger1 = New System.Windows.Forms.Label()
        Me.btnBurger16 = New System.Windows.Forms.Button()
        Me.btnBurger15 = New System.Windows.Forms.Button()
        Me.btnBurger14 = New System.Windows.Forms.Button()
        Me.btnBurger13 = New System.Windows.Forms.Button()
        Me.btnBurger12 = New System.Windows.Forms.Button()
        Me.btnBurger11 = New System.Windows.Forms.Button()
        Me.btnBurger10 = New System.Windows.Forms.Button()
        Me.btnBurger9 = New System.Windows.Forms.Button()
        Me.btnBurger8 = New System.Windows.Forms.Button()
        Me.btnBurger7 = New System.Windows.Forms.Button()
        Me.btnBurger6 = New System.Windows.Forms.Button()
        Me.btnBurger5 = New System.Windows.Forms.Button()
        Me.btnBurger4 = New System.Windows.Forms.Button()
        Me.btnBurger3 = New System.Windows.Forms.Button()
        Me.btnBurger2 = New System.Windows.Forms.Button()
        Me.btnBurger1 = New System.Windows.Forms.Button()
        Me.TabItem1 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel5 = New DevComponents.DotNetBar.TabControlPanel()
        Me.dtgRecord = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.Btnrefresh = New System.Windows.Forms.Button()
        Me.btnExport = New System.Windows.Forms.Button()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.txtSearch = New System.Windows.Forms.MaskedTextBox()
        Me.txtSearchby = New System.Windows.Forms.ComboBox()
        Me.lblRecords = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TabItem4 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel4 = New DevComponents.DotNetBar.TabControlPanel()
        Me.btnClearSpecial16 = New System.Windows.Forms.Button()
        Me.btnClearSpecial15 = New System.Windows.Forms.Button()
        Me.btnClearSpecial14 = New System.Windows.Forms.Button()
        Me.btnClearSpecial13 = New System.Windows.Forms.Button()
        Me.btnClearSpecial12 = New System.Windows.Forms.Button()
        Me.btnClearSpecial11 = New System.Windows.Forms.Button()
        Me.btnClearSpecial10 = New System.Windows.Forms.Button()
        Me.btnClearSpecial9 = New System.Windows.Forms.Button()
        Me.btnClearSpecial8 = New System.Windows.Forms.Button()
        Me.btnClearSpecial7 = New System.Windows.Forms.Button()
        Me.btnClearSpecial6 = New System.Windows.Forms.Button()
        Me.btnClearSpecial5 = New System.Windows.Forms.Button()
        Me.btnClearSpecial4 = New System.Windows.Forms.Button()
        Me.btnClearSpecial3 = New System.Windows.Forms.Button()
        Me.btnClearSpecial2 = New System.Windows.Forms.Button()
        Me.btnClearSpecial1 = New System.Windows.Forms.Button()
        Me.btnEditSpecial16 = New System.Windows.Forms.Button()
        Me.btnEditSpecial15 = New System.Windows.Forms.Button()
        Me.btnEditSpecial14 = New System.Windows.Forms.Button()
        Me.btnEditSpecial13 = New System.Windows.Forms.Button()
        Me.btnEditSpecial12 = New System.Windows.Forms.Button()
        Me.btnEditSpecial11 = New System.Windows.Forms.Button()
        Me.btnEditSpecial10 = New System.Windows.Forms.Button()
        Me.btnEditSpecial9 = New System.Windows.Forms.Button()
        Me.btnEditSpecial8 = New System.Windows.Forms.Button()
        Me.btnEditSpecial7 = New System.Windows.Forms.Button()
        Me.btnEditSpecial6 = New System.Windows.Forms.Button()
        Me.btnEditSpecial5 = New System.Windows.Forms.Button()
        Me.btnEditSpecial4 = New System.Windows.Forms.Button()
        Me.btnEditSpecial3 = New System.Windows.Forms.Button()
        Me.btnEditSpecial2 = New System.Windows.Forms.Button()
        Me.btnEditSpecial1 = New System.Windows.Forms.Button()
        Me.lblSpecial16 = New System.Windows.Forms.Label()
        Me.lblSpecial15 = New System.Windows.Forms.Label()
        Me.lblSpecial14 = New System.Windows.Forms.Label()
        Me.lblSpecial13 = New System.Windows.Forms.Label()
        Me.lblSpecial12 = New System.Windows.Forms.Label()
        Me.lblSpecial11 = New System.Windows.Forms.Label()
        Me.lblSpecial10 = New System.Windows.Forms.Label()
        Me.lblSpecial9 = New System.Windows.Forms.Label()
        Me.lblSpecial8 = New System.Windows.Forms.Label()
        Me.lblSpecial7 = New System.Windows.Forms.Label()
        Me.lblSpecial6 = New System.Windows.Forms.Label()
        Me.lblSpecial5 = New System.Windows.Forms.Label()
        Me.lblSpecial4 = New System.Windows.Forms.Label()
        Me.lblSpecial3 = New System.Windows.Forms.Label()
        Me.lblSpecial2 = New System.Windows.Forms.Label()
        Me.lblSpecial1 = New System.Windows.Forms.Label()
        Me.btnSpecial16 = New System.Windows.Forms.Button()
        Me.btnSpecial15 = New System.Windows.Forms.Button()
        Me.btnSpecial14 = New System.Windows.Forms.Button()
        Me.btnSpecial13 = New System.Windows.Forms.Button()
        Me.btnSpecial12 = New System.Windows.Forms.Button()
        Me.btnSpecial11 = New System.Windows.Forms.Button()
        Me.btnSpecial10 = New System.Windows.Forms.Button()
        Me.btnSpecial9 = New System.Windows.Forms.Button()
        Me.btnSpecial8 = New System.Windows.Forms.Button()
        Me.btnSpecial7 = New System.Windows.Forms.Button()
        Me.btnSpecial6 = New System.Windows.Forms.Button()
        Me.btnSpecial5 = New System.Windows.Forms.Button()
        Me.btnSpecial4 = New System.Windows.Forms.Button()
        Me.btnSpecial3 = New System.Windows.Forms.Button()
        Me.btnSpecial2 = New System.Windows.Forms.Button()
        Me.btnSpecial1 = New System.Windows.Forms.Button()
        Me.TabItem3 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel5.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.TabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        CType(Me.TabControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl2.SuspendLayout()
        Me.TabControlPanel3.SuspendLayout()
        Me.TabControlPanel2.SuspendLayout()
        Me.TabControlPanel5.SuspendLayout()
        CType(Me.dtgRecord, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel7.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.TabControlPanel4.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Black
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.btnMin)
        Me.Panel1.Controls.Add(Me.btnClose)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(806, 29)
        Me.Panel1.TabIndex = 6
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(9, 6)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(270, 17)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Main Menu - Order Management System"
        '
        'btnMin
        '
        Me.btnMin.BackColor = System.Drawing.Color.Black
        Me.btnMin.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnMin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnMin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver
        Me.btnMin.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMin.Image = CType(resources.GetObject("btnMin.Image"), System.Drawing.Image)
        Me.btnMin.Location = New System.Drawing.Point(754, 3)
        Me.btnMin.Name = "btnMin"
        Me.btnMin.Size = New System.Drawing.Size(22, 22)
        Me.btnMin.TabIndex = 6
        Me.ToolTip1.SetToolTip(Me.btnMin, "Minimize")
        Me.btnMin.UseVisualStyleBackColor = False
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.Black
        Me.btnClose.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClose.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClose.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Image = CType(resources.GetObject("btnClose.Image"), System.Drawing.Image)
        Me.btnClose.Location = New System.Drawing.Point(778, 3)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(22, 22)
        Me.btnClose.TabIndex = 0
        Me.ToolTip1.SetToolTip(Me.btnClose, "Close")
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.White
        Me.Panel2.Controls.Add(Me.LinkAbout)
        Me.Panel2.Controls.Add(Me.btnAdmin)
        Me.Panel2.Controls.Add(Me.btnLogout)
        Me.Panel2.Controls.Add(Me.Panel5)
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.PictureBox1)
        Me.Panel2.Controls.Add(Me.MenuStrip1)
        Me.Panel2.Location = New System.Drawing.Point(10, 29)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(784, 94)
        Me.Panel2.TabIndex = 7
        '
        'LinkAbout
        '
        Me.LinkAbout.ActiveLinkColor = System.Drawing.SystemColors.HotTrack
        Me.LinkAbout.AutoSize = True
        Me.LinkAbout.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkAbout.LinkColor = System.Drawing.SystemColors.Highlight
        Me.LinkAbout.Location = New System.Drawing.Point(116, 62)
        Me.LinkAbout.Name = "LinkAbout"
        Me.LinkAbout.Size = New System.Drawing.Size(144, 20)
        Me.LinkAbout.TabIndex = 17
        Me.LinkAbout.TabStop = True
        Me.LinkAbout.Text = "by Sergio Galvez >>"
        '
        'btnAdmin
        '
        Me.btnAdmin.BackColor = System.Drawing.Color.White
        Me.btnAdmin.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnAdmin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnAdmin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnAdmin.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdmin.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdmin.Image = CType(resources.GetObject("btnAdmin.Image"), System.Drawing.Image)
        Me.btnAdmin.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAdmin.Location = New System.Drawing.Point(376, 8)
        Me.btnAdmin.Name = "btnAdmin"
        Me.btnAdmin.Size = New System.Drawing.Size(43, 37)
        Me.btnAdmin.TabIndex = 16
        Me.ToolTip1.SetToolTip(Me.btnAdmin, "Change Password")
        Me.btnAdmin.UseVisualStyleBackColor = False
        '
        'btnLogout
        '
        Me.btnLogout.BackColor = System.Drawing.Color.White
        Me.btnLogout.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnLogout.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnLogout.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLogout.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLogout.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLogout.Location = New System.Drawing.Point(520, 48)
        Me.btnLogout.Name = "btnLogout"
        Me.btnLogout.Size = New System.Drawing.Size(259, 45)
        Me.btnLogout.TabIndex = 14
        Me.btnLogout.Text = "Logout"
        Me.btnLogout.UseVisualStyleBackColor = False
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.White
        Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel5.Controls.Add(Me.lblTime)
        Me.Panel5.Controls.Add(Me.lblDate)
        Me.Panel5.Controls.Add(Me.PictureBox2)
        Me.Panel5.Location = New System.Drawing.Point(520, 4)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(259, 45)
        Me.Panel5.TabIndex = 11
        '
        'lblTime
        '
        Me.lblTime.AutoSize = True
        Me.lblTime.BackColor = System.Drawing.Color.Transparent
        Me.lblTime.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTime.Location = New System.Drawing.Point(112, 22)
        Me.lblTime.Name = "lblTime"
        Me.lblTime.Size = New System.Drawing.Size(64, 16)
        Me.lblTime.TabIndex = 9
        Me.lblTime.Text = "08:32 PM"
        Me.lblTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDate
        '
        Me.lblDate.AutoSize = True
        Me.lblDate.BackColor = System.Drawing.Color.Transparent
        Me.lblDate.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(67, 3)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(179, 22)
        Me.lblDate.TabIndex = 8
        Me.lblDate.Text = "November 07, 2014"
        Me.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(10, 2)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(42, 38)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 1
        Me.PictureBox2.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(97, 44)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(372, 22)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Dona Soledad, Betterliving, Paranaque City"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Arial", 20.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(97, 13)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(283, 32)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "BURGER REPUBLIC"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(10, -3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(100, 100)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LogoutToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(784, 24)
        Me.MenuStrip1.TabIndex = 15
        Me.MenuStrip1.Text = "MenuStrip1"
        Me.MenuStrip1.Visible = False
        '
        'LogoutToolStripMenuItem
        '
        Me.LogoutToolStripMenuItem.Name = "LogoutToolStripMenuItem"
        Me.LogoutToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.L), System.Windows.Forms.Keys)
        Me.LogoutToolStripMenuItem.Size = New System.Drawing.Size(57, 20)
        Me.LogoutToolStripMenuItem.Text = "Logout"
        Me.LogoutToolStripMenuItem.Visible = False
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "Shop-Closed.png")
        Me.ImageList1.Images.SetKeyName(1, "Burger King-02.png")
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.White
        Me.GroupBox1.Controls.Add(Me.Panel4)
        Me.GroupBox1.Controls.Add(Me.Panel3)
        Me.GroupBox1.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(502, 123)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(292, 469)
        Me.GroupBox1.TabIndex = 8
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Order Details"
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.BtnCancel)
        Me.Panel4.Controls.Add(Me.BtnPayment)
        Me.Panel4.Controls.Add(Me.txtAmount)
        Me.Panel4.Controls.Add(Me.Label1)
        Me.Panel4.Location = New System.Drawing.Point(11, 320)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(271, 137)
        Me.Panel4.TabIndex = 1
        '
        'BtnCancel
        '
        Me.BtnCancel.BackColor = System.Drawing.Color.White
        Me.BtnCancel.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.BtnCancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.BtnCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.BtnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnCancel.Image = CType(resources.GetObject("BtnCancel.Image"), System.Drawing.Image)
        Me.BtnCancel.Location = New System.Drawing.Point(143, 65)
        Me.BtnCancel.Name = "BtnCancel"
        Me.BtnCancel.Size = New System.Drawing.Size(128, 66)
        Me.BtnCancel.TabIndex = 14
        Me.ToolTip1.SetToolTip(Me.BtnCancel, "Exit")
        Me.BtnCancel.UseVisualStyleBackColor = False
        '
        'BtnPayment
        '
        Me.BtnPayment.BackColor = System.Drawing.Color.White
        Me.BtnPayment.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.BtnPayment.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.BtnPayment.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.BtnPayment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnPayment.Image = CType(resources.GetObject("BtnPayment.Image"), System.Drawing.Image)
        Me.BtnPayment.Location = New System.Drawing.Point(0, 65)
        Me.BtnPayment.Name = "BtnPayment"
        Me.BtnPayment.Size = New System.Drawing.Size(145, 66)
        Me.BtnPayment.TabIndex = 13
        Me.BtnPayment.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.ToolTip1.SetToolTip(Me.BtnPayment, "Payment")
        Me.BtnPayment.UseVisualStyleBackColor = False
        '
        'txtAmount
        '
        Me.txtAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAmount.Dock = System.Windows.Forms.DockStyle.Top
        Me.txtAmount.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmount.Location = New System.Drawing.Point(0, 24)
        Me.txtAmount.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtAmount.Name = "txtAmount"
        Me.txtAmount.ReadOnly = True
        Me.txtAmount.Size = New System.Drawing.Size(271, 35)
        Me.txtAmount.TabIndex = 9
        Me.txtAmount.Text = "0.00"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label1.Font = New System.Drawing.Font("Arial", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(0, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(222, 24)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Total Amount (in Peso)"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.BtnClear)
        Me.Panel3.Controls.Add(Me.BtnRemove)
        Me.Panel3.Controls.Add(Me.LstOrder)
        Me.Panel3.Location = New System.Drawing.Point(11, 29)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(271, 266)
        Me.Panel3.TabIndex = 0
        '
        'BtnClear
        '
        Me.BtnClear.BackColor = System.Drawing.Color.White
        Me.BtnClear.Dock = System.Windows.Forms.DockStyle.Right
        Me.BtnClear.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.BtnClear.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.BtnClear.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.BtnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnClear.Image = CType(resources.GetObject("BtnClear.Image"), System.Drawing.Image)
        Me.BtnClear.Location = New System.Drawing.Point(143, 193)
        Me.BtnClear.Name = "BtnClear"
        Me.BtnClear.Size = New System.Drawing.Size(128, 73)
        Me.BtnClear.TabIndex = 11
        Me.ToolTip1.SetToolTip(Me.BtnClear, "Clear")
        Me.BtnClear.UseVisualStyleBackColor = False
        '
        'BtnRemove
        '
        Me.BtnRemove.BackColor = System.Drawing.Color.White
        Me.BtnRemove.Dock = System.Windows.Forms.DockStyle.Left
        Me.BtnRemove.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.BtnRemove.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.BtnRemove.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.BtnRemove.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnRemove.Image = CType(resources.GetObject("BtnRemove.Image"), System.Drawing.Image)
        Me.BtnRemove.Location = New System.Drawing.Point(0, 193)
        Me.BtnRemove.Name = "BtnRemove"
        Me.BtnRemove.Size = New System.Drawing.Size(145, 73)
        Me.BtnRemove.TabIndex = 10
        Me.ToolTip1.SetToolTip(Me.BtnRemove, "Remove")
        Me.BtnRemove.UseVisualStyleBackColor = False
        '
        'LstOrder
        '
        Me.LstOrder.Dock = System.Windows.Forms.DockStyle.Top
        Me.LstOrder.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LstOrder.FormattingEnabled = True
        Me.LstOrder.ItemHeight = 21
        Me.LstOrder.Location = New System.Drawing.Point(0, 0)
        Me.LstOrder.Name = "LstOrder"
        Me.LstOrder.Size = New System.Drawing.Size(271, 193)
        Me.LstOrder.TabIndex = 3
        '
        'TimerMenu
        '
        Me.TimerMenu.Enabled = True
        '
        'ImageListBurger
        '
        Me.ImageListBurger.ImageStream = CType(resources.GetObject("ImageListBurger.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageListBurger.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageListBurger.Images.SetKeyName(0, "Bacon.JPG")
        Me.ImageListBurger.Images.SetKeyName(1, "Beefy.JPG")
        Me.ImageListBurger.Images.SetKeyName(2, "Bloom.JPG")
        Me.ImageListBurger.Images.SetKeyName(3, "Champ.JPG")
        Me.ImageListBurger.Images.SetKeyName(4, "Cheese.JPG")
        Me.ImageListBurger.Images.SetKeyName(5, "Chicken.JPG")
        Me.ImageListBurger.Images.SetKeyName(6, "Garden.JPG")
        Me.ImageListBurger.Images.SetKeyName(7, "Ham.JPG")
        Me.ImageListBurger.Images.SetKeyName(8, "Lettuce.JPG")
        Me.ImageListBurger.Images.SetKeyName(9, "Marinara.JPG")
        Me.ImageListBurger.Images.SetKeyName(10, "Pineapple.JPG")
        Me.ImageListBurger.Images.SetKeyName(11, "Supreme.JPG")
        '
        'ImageListDrink
        '
        Me.ImageListDrink.ImageStream = CType(resources.GetObject("ImageListDrink.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageListDrink.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageListDrink.Images.SetKeyName(0, "Large MDew.JPG")
        Me.ImageListDrink.Images.SetKeyName(1, "Medium MDew.JPG")
        Me.ImageListDrink.Images.SetKeyName(2, "Small MDew.JPG")
        Me.ImageListDrink.Images.SetKeyName(3, "Large 7UP.JPG")
        Me.ImageListDrink.Images.SetKeyName(4, "Medium 7UP.JPG")
        Me.ImageListDrink.Images.SetKeyName(5, "Small 7UP.JPG")
        Me.ImageListDrink.Images.SetKeyName(6, "Large Cola.JPG")
        Me.ImageListDrink.Images.SetKeyName(7, "Medium Cola.JPG")
        Me.ImageListDrink.Images.SetKeyName(8, "Small Cola.JPG")
        '
        'ImageListSpecial
        '
        Me.ImageListSpecial.ImageStream = CType(resources.GetObject("ImageListSpecial.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageListSpecial.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageListSpecial.Images.SetKeyName(0, "Ice Cream.JPG")
        Me.ImageListSpecial.Images.SetKeyName(1, "Halo Halo.JPG")
        Me.ImageListSpecial.Images.SetKeyName(2, "Jolly Float.JPG")
        Me.ImageListSpecial.Images.SetKeyName(3, "Choco Frost.JPG")
        Me.ImageListSpecial.Images.SetKeyName(4, "Cup Cake.JPG")
        Me.ImageListSpecial.Images.SetKeyName(5, "Ice Craze.JPG")
        Me.ImageListSpecial.Images.SetKeyName(6, "Fruit Salad.JPG")
        Me.ImageListSpecial.Images.SetKeyName(7, "Macaroni.JPG")
        Me.ImageListSpecial.Images.SetKeyName(8, "Fruit Shake.JPG")
        Me.ImageListSpecial.Images.SetKeyName(9, "Mango Shake.JPG")
        Me.ImageListSpecial.Images.SetKeyName(10, "Milk Shake.JPG")
        Me.ImageListSpecial.Images.SetKeyName(11, "Sundae.JPG")
        '
        'TabControl1
        '
        Me.TabControl1.CanReorderTabs = True
        Me.TabControl1.ColorScheme.TabBackground = System.Drawing.SystemColors.Highlight
        Me.TabControl1.ColorScheme.TabBorder = System.Drawing.Color.Black
        Me.TabControl1.ColorScheme.TabItemBackground = System.Drawing.Color.White
        Me.TabControl1.ColorScheme.TabItemBackground2 = System.Drawing.Color.Empty
        Me.TabControl1.ColorScheme.TabItemBackgroundColorBlend.AddRange(New DevComponents.DotNetBar.BackgroundColorBlend() {New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(215, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(249, Byte), Integer)), 0.0!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(199, Byte), Integer), CType(CType(220, Byte), Integer), CType(CType(248, Byte), Integer)), 0.45!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(179, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(245, Byte), Integer)), 0.45!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer), CType(CType(247, Byte), Integer)), 1.0!)})
        Me.TabControl1.ColorScheme.TabItemBorder = System.Drawing.Color.Black
        Me.TabControl1.ColorScheme.TabItemBorderDark = System.Drawing.Color.Empty
        Me.TabControl1.ColorScheme.TabItemBorderLight = System.Drawing.Color.Empty
        Me.TabControl1.ColorScheme.TabItemHotBackground = System.Drawing.Color.Empty
        Me.TabControl1.ColorScheme.TabItemHotBackground2 = System.Drawing.Color.Empty
        Me.TabControl1.ColorScheme.TabItemHotBackgroundColorBlend.AddRange(New DevComponents.DotNetBar.BackgroundColorBlend() {New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(235, Byte), Integer)), 0.0!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(168, Byte), Integer)), 0.45!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(89, Byte), Integer)), 0.45!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(141, Byte), Integer)), 1.0!)})
        Me.TabControl1.ColorScheme.TabItemHotText = System.Drawing.Color.Empty
        Me.TabControl1.ColorScheme.TabItemSelectedBackground = System.Drawing.SystemColors.HotTrack
        Me.TabControl1.ColorScheme.TabItemSelectedBackgroundColorBlend.AddRange(New DevComponents.DotNetBar.BackgroundColorBlend() {New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.White, 0.0!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(254, Byte), Integer)), 0.45!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(254, Byte), Integer)), 0.45!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(254, Byte), Integer)), 1.0!)})
        Me.TabControl1.ColorScheme.TabItemSelectedBorderLight = System.Drawing.SystemColors.ControlText
        Me.TabControl1.ColorScheme.TabItemSelectedText = System.Drawing.Color.White
        Me.TabControl1.ColorScheme.TabItemText = System.Drawing.Color.Black
        Me.TabControl1.ColorScheme.TabPanelBackground = System.Drawing.Color.White
        Me.TabControl1.ColorScheme.TabPanelBackground2 = System.Drawing.Color.White
        Me.TabControl1.ColorScheme.TabPanelBorder = System.Drawing.Color.Black
        Me.TabControl1.Controls.Add(Me.TabControlPanel1)
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedTabFont = New System.Drawing.Font("MS UI Gothic", 9.0!, System.Drawing.FontStyle.Bold)
        Me.TabControl1.SelectedTabIndex = -1
        Me.TabControl1.Size = New System.Drawing.Size(256, 152)
        Me.TabControl1.TabIndex = 0
        Me.TabControl1.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox
        '
        'TabControlPanel1
        '
        Me.TabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TabControlPanel1.Name = "TabControlPanel1"
        Me.TabControlPanel1.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel1.Size = New System.Drawing.Size(256, 152)
        Me.TabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.White
        Me.TabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.White
        Me.TabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.Black
        Me.TabControlPanel1.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel1.Style.GradientAngle = 90
        Me.TabControlPanel1.TabIndex = 1
        '
        'TabControl2
        '
        Me.TabControl2.CanReorderTabs = True
        Me.TabControl2.ColorScheme.TabBackground = System.Drawing.SystemColors.Highlight
        Me.TabControl2.ColorScheme.TabBorder = System.Drawing.Color.Black
        Me.TabControl2.ColorScheme.TabItemBackground = System.Drawing.Color.White
        Me.TabControl2.ColorScheme.TabItemBackground2 = System.Drawing.Color.Empty
        Me.TabControl2.ColorScheme.TabItemBackgroundColorBlend.AddRange(New DevComponents.DotNetBar.BackgroundColorBlend() {New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(215, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(249, Byte), Integer)), 0.0!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(199, Byte), Integer), CType(CType(220, Byte), Integer), CType(CType(248, Byte), Integer)), 0.45!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(179, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(245, Byte), Integer)), 0.45!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer), CType(CType(247, Byte), Integer)), 1.0!)})
        Me.TabControl2.ColorScheme.TabItemBorder = System.Drawing.Color.Black
        Me.TabControl2.ColorScheme.TabItemBorderDark = System.Drawing.Color.Empty
        Me.TabControl2.ColorScheme.TabItemBorderLight = System.Drawing.Color.Empty
        Me.TabControl2.ColorScheme.TabItemHotBackground = System.Drawing.Color.Empty
        Me.TabControl2.ColorScheme.TabItemHotBackground2 = System.Drawing.Color.Empty
        Me.TabControl2.ColorScheme.TabItemHotBackgroundColorBlend.AddRange(New DevComponents.DotNetBar.BackgroundColorBlend() {New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(235, Byte), Integer)), 0.0!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(168, Byte), Integer)), 0.45!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(89, Byte), Integer)), 0.45!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(141, Byte), Integer)), 1.0!)})
        Me.TabControl2.ColorScheme.TabItemHotText = System.Drawing.Color.Empty
        Me.TabControl2.ColorScheme.TabItemSelectedBackground = System.Drawing.SystemColors.HotTrack
        Me.TabControl2.ColorScheme.TabItemSelectedBackgroundColorBlend.AddRange(New DevComponents.DotNetBar.BackgroundColorBlend() {New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.White, 0.0!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(254, Byte), Integer)), 0.45!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(254, Byte), Integer)), 0.45!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(254, Byte), Integer)), 1.0!)})
        Me.TabControl2.ColorScheme.TabItemSelectedBorderLight = System.Drawing.SystemColors.ControlText
        Me.TabControl2.ColorScheme.TabItemSelectedText = System.Drawing.Color.White
        Me.TabControl2.ColorScheme.TabItemText = System.Drawing.Color.Black
        Me.TabControl2.ColorScheme.TabPanelBackground = System.Drawing.Color.White
        Me.TabControl2.ColorScheme.TabPanelBackground2 = System.Drawing.Color.White
        Me.TabControl2.ColorScheme.TabPanelBorder = System.Drawing.Color.Black
        Me.TabControl2.Controls.Add(Me.TabControlPanel5)
        Me.TabControl2.Controls.Add(Me.TabControlPanel4)
        Me.TabControl2.Controls.Add(Me.TabControlPanel3)
        Me.TabControl2.Controls.Add(Me.TabControlPanel2)
        Me.TabControl2.FixedTabSize = New System.Drawing.Size(122, 35)
        Me.TabControl2.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl2.Location = New System.Drawing.Point(10, 122)
        Me.TabControl2.Name = "TabControl2"
        Me.TabControl2.SelectedTabFont = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl2.SelectedTabIndex = 0
        Me.TabControl2.Size = New System.Drawing.Size(497, 470)
        Me.TabControl2.Style = DevComponents.DotNetBar.eTabStripStyle.Flat
        Me.TabControl2.TabIndex = 9
        Me.TabControl2.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.MultilineNoNavigationBox
        Me.TabControl2.Tabs.Add(Me.TabItem1)
        Me.TabControl2.Tabs.Add(Me.TabItem2)
        Me.TabControl2.Tabs.Add(Me.TabItem3)
        Me.TabControl2.Tabs.Add(Me.TabItem4)
        Me.TabControl2.Text = "TabControl2"
        '
        'TabControlPanel3
        '
        Me.TabControlPanel3.Controls.Add(Me.btnClearDrink16)
        Me.TabControlPanel3.Controls.Add(Me.btnClearDrink15)
        Me.TabControlPanel3.Controls.Add(Me.btnClearDrink14)
        Me.TabControlPanel3.Controls.Add(Me.btnClearDrink13)
        Me.TabControlPanel3.Controls.Add(Me.btnClearDrink12)
        Me.TabControlPanel3.Controls.Add(Me.btnClearDrink11)
        Me.TabControlPanel3.Controls.Add(Me.btnClearDrink10)
        Me.TabControlPanel3.Controls.Add(Me.btnClearDrink9)
        Me.TabControlPanel3.Controls.Add(Me.btnClearDrink8)
        Me.TabControlPanel3.Controls.Add(Me.btnClearDrink7)
        Me.TabControlPanel3.Controls.Add(Me.btnClearDrink6)
        Me.TabControlPanel3.Controls.Add(Me.btnClearDrink5)
        Me.TabControlPanel3.Controls.Add(Me.btnClearDrink4)
        Me.TabControlPanel3.Controls.Add(Me.btnClearDrink3)
        Me.TabControlPanel3.Controls.Add(Me.btnClearDrink2)
        Me.TabControlPanel3.Controls.Add(Me.btnClearDrink1)
        Me.TabControlPanel3.Controls.Add(Me.btnEditDrink16)
        Me.TabControlPanel3.Controls.Add(Me.btnEditDrink15)
        Me.TabControlPanel3.Controls.Add(Me.btnEditDrink14)
        Me.TabControlPanel3.Controls.Add(Me.btnEditDrink13)
        Me.TabControlPanel3.Controls.Add(Me.btnEditDrink12)
        Me.TabControlPanel3.Controls.Add(Me.btnEditDrink11)
        Me.TabControlPanel3.Controls.Add(Me.btnEditDrink10)
        Me.TabControlPanel3.Controls.Add(Me.btnEditDrink9)
        Me.TabControlPanel3.Controls.Add(Me.btnEditDrink8)
        Me.TabControlPanel3.Controls.Add(Me.btnEditDrink7)
        Me.TabControlPanel3.Controls.Add(Me.btnEditDrink6)
        Me.TabControlPanel3.Controls.Add(Me.btnEditDrink5)
        Me.TabControlPanel3.Controls.Add(Me.btnEditDrink4)
        Me.TabControlPanel3.Controls.Add(Me.btnEditDrink3)
        Me.TabControlPanel3.Controls.Add(Me.btnEditDrink2)
        Me.TabControlPanel3.Controls.Add(Me.btnEditDrink1)
        Me.TabControlPanel3.Controls.Add(Me.lblDrink16)
        Me.TabControlPanel3.Controls.Add(Me.lblDrink15)
        Me.TabControlPanel3.Controls.Add(Me.lblDrink14)
        Me.TabControlPanel3.Controls.Add(Me.lblDrink13)
        Me.TabControlPanel3.Controls.Add(Me.lblDrink12)
        Me.TabControlPanel3.Controls.Add(Me.lblDrink11)
        Me.TabControlPanel3.Controls.Add(Me.lblDrink10)
        Me.TabControlPanel3.Controls.Add(Me.lblDrink9)
        Me.TabControlPanel3.Controls.Add(Me.lblDrink8)
        Me.TabControlPanel3.Controls.Add(Me.lblDrink7)
        Me.TabControlPanel3.Controls.Add(Me.lblDrink6)
        Me.TabControlPanel3.Controls.Add(Me.lblDrink5)
        Me.TabControlPanel3.Controls.Add(Me.lblDrink4)
        Me.TabControlPanel3.Controls.Add(Me.lblDrink3)
        Me.TabControlPanel3.Controls.Add(Me.lblDrink2)
        Me.TabControlPanel3.Controls.Add(Me.lblDrink1)
        Me.TabControlPanel3.Controls.Add(Me.btnDrink16)
        Me.TabControlPanel3.Controls.Add(Me.btnDrink15)
        Me.TabControlPanel3.Controls.Add(Me.btnDrink14)
        Me.TabControlPanel3.Controls.Add(Me.btnDrink13)
        Me.TabControlPanel3.Controls.Add(Me.btnDrink12)
        Me.TabControlPanel3.Controls.Add(Me.btnDrink11)
        Me.TabControlPanel3.Controls.Add(Me.btnDrink10)
        Me.TabControlPanel3.Controls.Add(Me.btnDrink9)
        Me.TabControlPanel3.Controls.Add(Me.btnDrink8)
        Me.TabControlPanel3.Controls.Add(Me.btnDrink7)
        Me.TabControlPanel3.Controls.Add(Me.btnDrink6)
        Me.TabControlPanel3.Controls.Add(Me.btnDrink5)
        Me.TabControlPanel3.Controls.Add(Me.btnDrink4)
        Me.TabControlPanel3.Controls.Add(Me.btnDrink3)
        Me.TabControlPanel3.Controls.Add(Me.btnDrink2)
        Me.TabControlPanel3.Controls.Add(Me.btnDrink1)
        Me.TabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel3.Location = New System.Drawing.Point(0, 40)
        Me.TabControlPanel3.Name = "TabControlPanel3"
        Me.TabControlPanel3.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel3.Size = New System.Drawing.Size(497, 430)
        Me.TabControlPanel3.Style.BackColor1.Color = System.Drawing.Color.White
        Me.TabControlPanel3.Style.BackColor2.Color = System.Drawing.Color.White
        Me.TabControlPanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel3.Style.BorderColor.Color = System.Drawing.Color.Black
        Me.TabControlPanel3.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel3.Style.GradientAngle = 90
        Me.TabControlPanel3.TabIndex = 2
        Me.TabControlPanel3.TabItem = Me.TabItem2
        '
        'btnClearDrink16
        '
        Me.btnClearDrink16.BackColor = System.Drawing.Color.White
        Me.btnClearDrink16.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearDrink16.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearDrink16.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearDrink16.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearDrink16.Image = CType(resources.GetObject("btnClearDrink16.Image"), System.Drawing.Image)
        Me.btnClearDrink16.Location = New System.Drawing.Point(367, 318)
        Me.btnClearDrink16.Name = "btnClearDrink16"
        Me.btnClearDrink16.Size = New System.Drawing.Size(25, 25)
        Me.btnClearDrink16.TabIndex = 269
        Me.btnClearDrink16.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearDrink16.UseVisualStyleBackColor = False
        '
        'btnClearDrink15
        '
        Me.btnClearDrink15.BackColor = System.Drawing.Color.White
        Me.btnClearDrink15.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearDrink15.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearDrink15.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearDrink15.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearDrink15.Image = CType(resources.GetObject("btnClearDrink15.Image"), System.Drawing.Image)
        Me.btnClearDrink15.Location = New System.Drawing.Point(248, 318)
        Me.btnClearDrink15.Name = "btnClearDrink15"
        Me.btnClearDrink15.Size = New System.Drawing.Size(25, 25)
        Me.btnClearDrink15.TabIndex = 268
        Me.btnClearDrink15.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearDrink15.UseVisualStyleBackColor = False
        '
        'btnClearDrink14
        '
        Me.btnClearDrink14.BackColor = System.Drawing.Color.White
        Me.btnClearDrink14.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearDrink14.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearDrink14.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearDrink14.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearDrink14.Image = CType(resources.GetObject("btnClearDrink14.Image"), System.Drawing.Image)
        Me.btnClearDrink14.Location = New System.Drawing.Point(129, 318)
        Me.btnClearDrink14.Name = "btnClearDrink14"
        Me.btnClearDrink14.Size = New System.Drawing.Size(25, 25)
        Me.btnClearDrink14.TabIndex = 267
        Me.btnClearDrink14.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearDrink14.UseVisualStyleBackColor = False
        '
        'btnClearDrink13
        '
        Me.btnClearDrink13.BackColor = System.Drawing.Color.White
        Me.btnClearDrink13.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearDrink13.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearDrink13.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearDrink13.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearDrink13.Image = CType(resources.GetObject("btnClearDrink13.Image"), System.Drawing.Image)
        Me.btnClearDrink13.Location = New System.Drawing.Point(10, 318)
        Me.btnClearDrink13.Name = "btnClearDrink13"
        Me.btnClearDrink13.Size = New System.Drawing.Size(25, 25)
        Me.btnClearDrink13.TabIndex = 266
        Me.btnClearDrink13.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearDrink13.UseVisualStyleBackColor = False
        '
        'btnClearDrink12
        '
        Me.btnClearDrink12.BackColor = System.Drawing.Color.White
        Me.btnClearDrink12.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearDrink12.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearDrink12.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearDrink12.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearDrink12.Image = CType(resources.GetObject("btnClearDrink12.Image"), System.Drawing.Image)
        Me.btnClearDrink12.Location = New System.Drawing.Point(367, 214)
        Me.btnClearDrink12.Name = "btnClearDrink12"
        Me.btnClearDrink12.Size = New System.Drawing.Size(25, 25)
        Me.btnClearDrink12.TabIndex = 265
        Me.btnClearDrink12.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearDrink12.UseVisualStyleBackColor = False
        '
        'btnClearDrink11
        '
        Me.btnClearDrink11.BackColor = System.Drawing.Color.White
        Me.btnClearDrink11.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearDrink11.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearDrink11.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearDrink11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearDrink11.Image = CType(resources.GetObject("btnClearDrink11.Image"), System.Drawing.Image)
        Me.btnClearDrink11.Location = New System.Drawing.Point(248, 214)
        Me.btnClearDrink11.Name = "btnClearDrink11"
        Me.btnClearDrink11.Size = New System.Drawing.Size(25, 25)
        Me.btnClearDrink11.TabIndex = 264
        Me.btnClearDrink11.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearDrink11.UseVisualStyleBackColor = False
        '
        'btnClearDrink10
        '
        Me.btnClearDrink10.BackColor = System.Drawing.Color.White
        Me.btnClearDrink10.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearDrink10.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearDrink10.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearDrink10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearDrink10.Image = CType(resources.GetObject("btnClearDrink10.Image"), System.Drawing.Image)
        Me.btnClearDrink10.Location = New System.Drawing.Point(129, 214)
        Me.btnClearDrink10.Name = "btnClearDrink10"
        Me.btnClearDrink10.Size = New System.Drawing.Size(25, 25)
        Me.btnClearDrink10.TabIndex = 263
        Me.btnClearDrink10.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearDrink10.UseVisualStyleBackColor = False
        '
        'btnClearDrink9
        '
        Me.btnClearDrink9.BackColor = System.Drawing.Color.White
        Me.btnClearDrink9.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearDrink9.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearDrink9.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearDrink9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearDrink9.Image = CType(resources.GetObject("btnClearDrink9.Image"), System.Drawing.Image)
        Me.btnClearDrink9.Location = New System.Drawing.Point(10, 214)
        Me.btnClearDrink9.Name = "btnClearDrink9"
        Me.btnClearDrink9.Size = New System.Drawing.Size(25, 25)
        Me.btnClearDrink9.TabIndex = 262
        Me.btnClearDrink9.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearDrink9.UseVisualStyleBackColor = False
        '
        'btnClearDrink8
        '
        Me.btnClearDrink8.BackColor = System.Drawing.Color.White
        Me.btnClearDrink8.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearDrink8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearDrink8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearDrink8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearDrink8.Image = CType(resources.GetObject("btnClearDrink8.Image"), System.Drawing.Image)
        Me.btnClearDrink8.Location = New System.Drawing.Point(367, 111)
        Me.btnClearDrink8.Name = "btnClearDrink8"
        Me.btnClearDrink8.Size = New System.Drawing.Size(25, 25)
        Me.btnClearDrink8.TabIndex = 261
        Me.btnClearDrink8.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearDrink8.UseVisualStyleBackColor = False
        '
        'btnClearDrink7
        '
        Me.btnClearDrink7.BackColor = System.Drawing.Color.White
        Me.btnClearDrink7.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearDrink7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearDrink7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearDrink7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearDrink7.Image = CType(resources.GetObject("btnClearDrink7.Image"), System.Drawing.Image)
        Me.btnClearDrink7.Location = New System.Drawing.Point(248, 111)
        Me.btnClearDrink7.Name = "btnClearDrink7"
        Me.btnClearDrink7.Size = New System.Drawing.Size(25, 25)
        Me.btnClearDrink7.TabIndex = 260
        Me.btnClearDrink7.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearDrink7.UseVisualStyleBackColor = False
        '
        'btnClearDrink6
        '
        Me.btnClearDrink6.BackColor = System.Drawing.Color.White
        Me.btnClearDrink6.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearDrink6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearDrink6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearDrink6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearDrink6.Image = CType(resources.GetObject("btnClearDrink6.Image"), System.Drawing.Image)
        Me.btnClearDrink6.Location = New System.Drawing.Point(129, 111)
        Me.btnClearDrink6.Name = "btnClearDrink6"
        Me.btnClearDrink6.Size = New System.Drawing.Size(25, 25)
        Me.btnClearDrink6.TabIndex = 259
        Me.btnClearDrink6.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearDrink6.UseVisualStyleBackColor = False
        '
        'btnClearDrink5
        '
        Me.btnClearDrink5.BackColor = System.Drawing.Color.White
        Me.btnClearDrink5.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearDrink5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearDrink5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearDrink5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearDrink5.Image = CType(resources.GetObject("btnClearDrink5.Image"), System.Drawing.Image)
        Me.btnClearDrink5.Location = New System.Drawing.Point(10, 111)
        Me.btnClearDrink5.Name = "btnClearDrink5"
        Me.btnClearDrink5.Size = New System.Drawing.Size(25, 25)
        Me.btnClearDrink5.TabIndex = 258
        Me.btnClearDrink5.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearDrink5.UseVisualStyleBackColor = False
        '
        'btnClearDrink4
        '
        Me.btnClearDrink4.BackColor = System.Drawing.Color.White
        Me.btnClearDrink4.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearDrink4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearDrink4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearDrink4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearDrink4.Image = CType(resources.GetObject("btnClearDrink4.Image"), System.Drawing.Image)
        Me.btnClearDrink4.Location = New System.Drawing.Point(367, 7)
        Me.btnClearDrink4.Name = "btnClearDrink4"
        Me.btnClearDrink4.Size = New System.Drawing.Size(25, 25)
        Me.btnClearDrink4.TabIndex = 257
        Me.btnClearDrink4.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearDrink4.UseVisualStyleBackColor = False
        '
        'btnClearDrink3
        '
        Me.btnClearDrink3.BackColor = System.Drawing.Color.White
        Me.btnClearDrink3.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearDrink3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearDrink3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearDrink3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearDrink3.Image = CType(resources.GetObject("btnClearDrink3.Image"), System.Drawing.Image)
        Me.btnClearDrink3.Location = New System.Drawing.Point(248, 7)
        Me.btnClearDrink3.Name = "btnClearDrink3"
        Me.btnClearDrink3.Size = New System.Drawing.Size(25, 25)
        Me.btnClearDrink3.TabIndex = 256
        Me.btnClearDrink3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearDrink3.UseVisualStyleBackColor = False
        '
        'btnClearDrink2
        '
        Me.btnClearDrink2.BackColor = System.Drawing.Color.White
        Me.btnClearDrink2.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearDrink2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearDrink2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearDrink2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearDrink2.Image = CType(resources.GetObject("btnClearDrink2.Image"), System.Drawing.Image)
        Me.btnClearDrink2.Location = New System.Drawing.Point(129, 7)
        Me.btnClearDrink2.Name = "btnClearDrink2"
        Me.btnClearDrink2.Size = New System.Drawing.Size(25, 25)
        Me.btnClearDrink2.TabIndex = 255
        Me.btnClearDrink2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearDrink2.UseVisualStyleBackColor = False
        '
        'btnClearDrink1
        '
        Me.btnClearDrink1.BackColor = System.Drawing.Color.White
        Me.btnClearDrink1.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearDrink1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearDrink1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearDrink1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearDrink1.Image = CType(resources.GetObject("btnClearDrink1.Image"), System.Drawing.Image)
        Me.btnClearDrink1.Location = New System.Drawing.Point(10, 7)
        Me.btnClearDrink1.Name = "btnClearDrink1"
        Me.btnClearDrink1.Size = New System.Drawing.Size(25, 25)
        Me.btnClearDrink1.TabIndex = 254
        Me.btnClearDrink1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearDrink1.UseVisualStyleBackColor = False
        '
        'btnEditDrink16
        '
        Me.btnEditDrink16.BackColor = System.Drawing.Color.White
        Me.btnEditDrink16.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditDrink16.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditDrink16.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditDrink16.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditDrink16.Image = CType(resources.GetObject("btnEditDrink16.Image"), System.Drawing.Image)
        Me.btnEditDrink16.Location = New System.Drawing.Point(462, 318)
        Me.btnEditDrink16.Name = "btnEditDrink16"
        Me.btnEditDrink16.Size = New System.Drawing.Size(25, 25)
        Me.btnEditDrink16.TabIndex = 253
        Me.btnEditDrink16.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditDrink16.UseVisualStyleBackColor = False
        '
        'btnEditDrink15
        '
        Me.btnEditDrink15.BackColor = System.Drawing.Color.White
        Me.btnEditDrink15.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditDrink15.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditDrink15.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditDrink15.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditDrink15.Image = CType(resources.GetObject("btnEditDrink15.Image"), System.Drawing.Image)
        Me.btnEditDrink15.Location = New System.Drawing.Point(343, 318)
        Me.btnEditDrink15.Name = "btnEditDrink15"
        Me.btnEditDrink15.Size = New System.Drawing.Size(25, 25)
        Me.btnEditDrink15.TabIndex = 252
        Me.btnEditDrink15.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditDrink15.UseVisualStyleBackColor = False
        '
        'btnEditDrink14
        '
        Me.btnEditDrink14.BackColor = System.Drawing.Color.White
        Me.btnEditDrink14.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditDrink14.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditDrink14.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditDrink14.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditDrink14.Image = CType(resources.GetObject("btnEditDrink14.Image"), System.Drawing.Image)
        Me.btnEditDrink14.Location = New System.Drawing.Point(224, 318)
        Me.btnEditDrink14.Name = "btnEditDrink14"
        Me.btnEditDrink14.Size = New System.Drawing.Size(25, 25)
        Me.btnEditDrink14.TabIndex = 251
        Me.btnEditDrink14.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditDrink14.UseVisualStyleBackColor = False
        '
        'btnEditDrink13
        '
        Me.btnEditDrink13.BackColor = System.Drawing.Color.White
        Me.btnEditDrink13.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditDrink13.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditDrink13.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditDrink13.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditDrink13.Image = CType(resources.GetObject("btnEditDrink13.Image"), System.Drawing.Image)
        Me.btnEditDrink13.Location = New System.Drawing.Point(105, 318)
        Me.btnEditDrink13.Name = "btnEditDrink13"
        Me.btnEditDrink13.Size = New System.Drawing.Size(25, 25)
        Me.btnEditDrink13.TabIndex = 250
        Me.btnEditDrink13.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditDrink13.UseVisualStyleBackColor = False
        '
        'btnEditDrink12
        '
        Me.btnEditDrink12.BackColor = System.Drawing.Color.White
        Me.btnEditDrink12.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditDrink12.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditDrink12.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditDrink12.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditDrink12.Image = CType(resources.GetObject("btnEditDrink12.Image"), System.Drawing.Image)
        Me.btnEditDrink12.Location = New System.Drawing.Point(462, 214)
        Me.btnEditDrink12.Name = "btnEditDrink12"
        Me.btnEditDrink12.Size = New System.Drawing.Size(25, 25)
        Me.btnEditDrink12.TabIndex = 249
        Me.btnEditDrink12.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditDrink12.UseVisualStyleBackColor = False
        '
        'btnEditDrink11
        '
        Me.btnEditDrink11.BackColor = System.Drawing.Color.White
        Me.btnEditDrink11.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditDrink11.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditDrink11.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditDrink11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditDrink11.Image = CType(resources.GetObject("btnEditDrink11.Image"), System.Drawing.Image)
        Me.btnEditDrink11.Location = New System.Drawing.Point(343, 214)
        Me.btnEditDrink11.Name = "btnEditDrink11"
        Me.btnEditDrink11.Size = New System.Drawing.Size(25, 25)
        Me.btnEditDrink11.TabIndex = 248
        Me.btnEditDrink11.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditDrink11.UseVisualStyleBackColor = False
        '
        'btnEditDrink10
        '
        Me.btnEditDrink10.BackColor = System.Drawing.Color.White
        Me.btnEditDrink10.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditDrink10.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditDrink10.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditDrink10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditDrink10.Image = CType(resources.GetObject("btnEditDrink10.Image"), System.Drawing.Image)
        Me.btnEditDrink10.Location = New System.Drawing.Point(224, 214)
        Me.btnEditDrink10.Name = "btnEditDrink10"
        Me.btnEditDrink10.Size = New System.Drawing.Size(25, 25)
        Me.btnEditDrink10.TabIndex = 247
        Me.btnEditDrink10.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditDrink10.UseVisualStyleBackColor = False
        '
        'btnEditDrink9
        '
        Me.btnEditDrink9.BackColor = System.Drawing.Color.White
        Me.btnEditDrink9.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditDrink9.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditDrink9.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditDrink9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditDrink9.Image = CType(resources.GetObject("btnEditDrink9.Image"), System.Drawing.Image)
        Me.btnEditDrink9.Location = New System.Drawing.Point(105, 214)
        Me.btnEditDrink9.Name = "btnEditDrink9"
        Me.btnEditDrink9.Size = New System.Drawing.Size(25, 25)
        Me.btnEditDrink9.TabIndex = 246
        Me.btnEditDrink9.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditDrink9.UseVisualStyleBackColor = False
        '
        'btnEditDrink8
        '
        Me.btnEditDrink8.BackColor = System.Drawing.Color.White
        Me.btnEditDrink8.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditDrink8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditDrink8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditDrink8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditDrink8.Image = CType(resources.GetObject("btnEditDrink8.Image"), System.Drawing.Image)
        Me.btnEditDrink8.Location = New System.Drawing.Point(462, 111)
        Me.btnEditDrink8.Name = "btnEditDrink8"
        Me.btnEditDrink8.Size = New System.Drawing.Size(25, 25)
        Me.btnEditDrink8.TabIndex = 245
        Me.btnEditDrink8.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditDrink8.UseVisualStyleBackColor = False
        '
        'btnEditDrink7
        '
        Me.btnEditDrink7.BackColor = System.Drawing.Color.White
        Me.btnEditDrink7.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditDrink7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditDrink7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditDrink7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditDrink7.Image = CType(resources.GetObject("btnEditDrink7.Image"), System.Drawing.Image)
        Me.btnEditDrink7.Location = New System.Drawing.Point(343, 111)
        Me.btnEditDrink7.Name = "btnEditDrink7"
        Me.btnEditDrink7.Size = New System.Drawing.Size(25, 25)
        Me.btnEditDrink7.TabIndex = 244
        Me.btnEditDrink7.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditDrink7.UseVisualStyleBackColor = False
        '
        'btnEditDrink6
        '
        Me.btnEditDrink6.BackColor = System.Drawing.Color.White
        Me.btnEditDrink6.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditDrink6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditDrink6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditDrink6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditDrink6.Image = CType(resources.GetObject("btnEditDrink6.Image"), System.Drawing.Image)
        Me.btnEditDrink6.Location = New System.Drawing.Point(224, 111)
        Me.btnEditDrink6.Name = "btnEditDrink6"
        Me.btnEditDrink6.Size = New System.Drawing.Size(25, 25)
        Me.btnEditDrink6.TabIndex = 243
        Me.btnEditDrink6.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditDrink6.UseVisualStyleBackColor = False
        '
        'btnEditDrink5
        '
        Me.btnEditDrink5.BackColor = System.Drawing.Color.White
        Me.btnEditDrink5.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditDrink5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditDrink5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditDrink5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditDrink5.Image = CType(resources.GetObject("btnEditDrink5.Image"), System.Drawing.Image)
        Me.btnEditDrink5.Location = New System.Drawing.Point(105, 111)
        Me.btnEditDrink5.Name = "btnEditDrink5"
        Me.btnEditDrink5.Size = New System.Drawing.Size(25, 25)
        Me.btnEditDrink5.TabIndex = 242
        Me.btnEditDrink5.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditDrink5.UseVisualStyleBackColor = False
        '
        'btnEditDrink4
        '
        Me.btnEditDrink4.BackColor = System.Drawing.Color.White
        Me.btnEditDrink4.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditDrink4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditDrink4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditDrink4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditDrink4.Image = CType(resources.GetObject("btnEditDrink4.Image"), System.Drawing.Image)
        Me.btnEditDrink4.Location = New System.Drawing.Point(462, 7)
        Me.btnEditDrink4.Name = "btnEditDrink4"
        Me.btnEditDrink4.Size = New System.Drawing.Size(25, 25)
        Me.btnEditDrink4.TabIndex = 241
        Me.btnEditDrink4.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditDrink4.UseVisualStyleBackColor = False
        '
        'btnEditDrink3
        '
        Me.btnEditDrink3.BackColor = System.Drawing.Color.White
        Me.btnEditDrink3.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditDrink3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditDrink3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditDrink3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditDrink3.Image = CType(resources.GetObject("btnEditDrink3.Image"), System.Drawing.Image)
        Me.btnEditDrink3.Location = New System.Drawing.Point(343, 7)
        Me.btnEditDrink3.Name = "btnEditDrink3"
        Me.btnEditDrink3.Size = New System.Drawing.Size(25, 25)
        Me.btnEditDrink3.TabIndex = 240
        Me.btnEditDrink3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditDrink3.UseVisualStyleBackColor = False
        '
        'btnEditDrink2
        '
        Me.btnEditDrink2.BackColor = System.Drawing.Color.White
        Me.btnEditDrink2.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditDrink2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditDrink2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditDrink2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditDrink2.Image = CType(resources.GetObject("btnEditDrink2.Image"), System.Drawing.Image)
        Me.btnEditDrink2.Location = New System.Drawing.Point(224, 7)
        Me.btnEditDrink2.Name = "btnEditDrink2"
        Me.btnEditDrink2.Size = New System.Drawing.Size(25, 25)
        Me.btnEditDrink2.TabIndex = 239
        Me.btnEditDrink2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditDrink2.UseVisualStyleBackColor = False
        '
        'btnEditDrink1
        '
        Me.btnEditDrink1.BackColor = System.Drawing.Color.White
        Me.btnEditDrink1.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditDrink1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditDrink1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditDrink1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditDrink1.Image = CType(resources.GetObject("btnEditDrink1.Image"), System.Drawing.Image)
        Me.btnEditDrink1.Location = New System.Drawing.Point(105, 7)
        Me.btnEditDrink1.Name = "btnEditDrink1"
        Me.btnEditDrink1.Size = New System.Drawing.Size(25, 25)
        Me.btnEditDrink1.TabIndex = 238
        Me.btnEditDrink1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditDrink1.UseVisualStyleBackColor = False
        '
        'lblDrink16
        '
        Me.lblDrink16.AutoSize = True
        Me.lblDrink16.BackColor = System.Drawing.Color.Transparent
        Me.lblDrink16.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDrink16.Location = New System.Drawing.Point(408, 402)
        Me.lblDrink16.Name = "lblDrink16"
        Me.lblDrink16.Size = New System.Drawing.Size(39, 17)
        Me.lblDrink16.TabIndex = 237
        Me.lblDrink16.Text = "00.00"
        Me.lblDrink16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDrink15
        '
        Me.lblDrink15.AutoSize = True
        Me.lblDrink15.BackColor = System.Drawing.Color.Transparent
        Me.lblDrink15.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDrink15.Location = New System.Drawing.Point(291, 402)
        Me.lblDrink15.Name = "lblDrink15"
        Me.lblDrink15.Size = New System.Drawing.Size(39, 17)
        Me.lblDrink15.TabIndex = 236
        Me.lblDrink15.Text = "00.00"
        Me.lblDrink15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDrink14
        '
        Me.lblDrink14.AutoSize = True
        Me.lblDrink14.BackColor = System.Drawing.Color.Transparent
        Me.lblDrink14.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDrink14.Location = New System.Drawing.Point(170, 402)
        Me.lblDrink14.Name = "lblDrink14"
        Me.lblDrink14.Size = New System.Drawing.Size(39, 17)
        Me.lblDrink14.TabIndex = 235
        Me.lblDrink14.Text = "00.00"
        Me.lblDrink14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDrink13
        '
        Me.lblDrink13.AutoSize = True
        Me.lblDrink13.BackColor = System.Drawing.Color.Transparent
        Me.lblDrink13.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDrink13.Location = New System.Drawing.Point(50, 402)
        Me.lblDrink13.Name = "lblDrink13"
        Me.lblDrink13.Size = New System.Drawing.Size(39, 17)
        Me.lblDrink13.TabIndex = 234
        Me.lblDrink13.Text = "00.00"
        Me.lblDrink13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDrink12
        '
        Me.lblDrink12.AutoSize = True
        Me.lblDrink12.BackColor = System.Drawing.Color.Transparent
        Me.lblDrink12.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDrink12.Location = New System.Drawing.Point(408, 297)
        Me.lblDrink12.Name = "lblDrink12"
        Me.lblDrink12.Size = New System.Drawing.Size(39, 17)
        Me.lblDrink12.TabIndex = 233
        Me.lblDrink12.Text = "00.00"
        Me.lblDrink12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDrink11
        '
        Me.lblDrink11.AutoSize = True
        Me.lblDrink11.BackColor = System.Drawing.Color.Transparent
        Me.lblDrink11.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDrink11.Location = New System.Drawing.Point(291, 297)
        Me.lblDrink11.Name = "lblDrink11"
        Me.lblDrink11.Size = New System.Drawing.Size(39, 17)
        Me.lblDrink11.TabIndex = 232
        Me.lblDrink11.Text = "00.00"
        Me.lblDrink11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDrink10
        '
        Me.lblDrink10.AutoSize = True
        Me.lblDrink10.BackColor = System.Drawing.Color.Transparent
        Me.lblDrink10.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDrink10.Location = New System.Drawing.Point(170, 297)
        Me.lblDrink10.Name = "lblDrink10"
        Me.lblDrink10.Size = New System.Drawing.Size(39, 17)
        Me.lblDrink10.TabIndex = 231
        Me.lblDrink10.Text = "00.00"
        Me.lblDrink10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDrink9
        '
        Me.lblDrink9.AutoSize = True
        Me.lblDrink9.BackColor = System.Drawing.Color.Transparent
        Me.lblDrink9.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDrink9.Location = New System.Drawing.Point(50, 297)
        Me.lblDrink9.Name = "lblDrink9"
        Me.lblDrink9.Size = New System.Drawing.Size(39, 17)
        Me.lblDrink9.TabIndex = 230
        Me.lblDrink9.Text = "00.00"
        Me.lblDrink9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDrink8
        '
        Me.lblDrink8.AutoSize = True
        Me.lblDrink8.BackColor = System.Drawing.Color.Transparent
        Me.lblDrink8.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDrink8.Location = New System.Drawing.Point(408, 194)
        Me.lblDrink8.Name = "lblDrink8"
        Me.lblDrink8.Size = New System.Drawing.Size(39, 17)
        Me.lblDrink8.TabIndex = 229
        Me.lblDrink8.Text = "00.00"
        Me.lblDrink8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDrink7
        '
        Me.lblDrink7.AutoSize = True
        Me.lblDrink7.BackColor = System.Drawing.Color.Transparent
        Me.lblDrink7.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDrink7.Location = New System.Drawing.Point(291, 194)
        Me.lblDrink7.Name = "lblDrink7"
        Me.lblDrink7.Size = New System.Drawing.Size(39, 17)
        Me.lblDrink7.TabIndex = 228
        Me.lblDrink7.Text = "00.00"
        Me.lblDrink7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDrink6
        '
        Me.lblDrink6.AutoSize = True
        Me.lblDrink6.BackColor = System.Drawing.Color.Transparent
        Me.lblDrink6.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDrink6.Location = New System.Drawing.Point(170, 194)
        Me.lblDrink6.Name = "lblDrink6"
        Me.lblDrink6.Size = New System.Drawing.Size(39, 17)
        Me.lblDrink6.TabIndex = 227
        Me.lblDrink6.Text = "00.00"
        Me.lblDrink6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDrink5
        '
        Me.lblDrink5.AutoSize = True
        Me.lblDrink5.BackColor = System.Drawing.Color.Transparent
        Me.lblDrink5.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDrink5.Location = New System.Drawing.Point(50, 194)
        Me.lblDrink5.Name = "lblDrink5"
        Me.lblDrink5.Size = New System.Drawing.Size(39, 17)
        Me.lblDrink5.TabIndex = 226
        Me.lblDrink5.Text = "00.00"
        Me.lblDrink5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDrink4
        '
        Me.lblDrink4.AutoSize = True
        Me.lblDrink4.BackColor = System.Drawing.Color.Transparent
        Me.lblDrink4.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDrink4.Location = New System.Drawing.Point(408, 90)
        Me.lblDrink4.Name = "lblDrink4"
        Me.lblDrink4.Size = New System.Drawing.Size(39, 17)
        Me.lblDrink4.TabIndex = 225
        Me.lblDrink4.Text = "00.00"
        Me.lblDrink4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDrink3
        '
        Me.lblDrink3.AutoSize = True
        Me.lblDrink3.BackColor = System.Drawing.Color.Transparent
        Me.lblDrink3.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDrink3.Location = New System.Drawing.Point(291, 90)
        Me.lblDrink3.Name = "lblDrink3"
        Me.lblDrink3.Size = New System.Drawing.Size(39, 17)
        Me.lblDrink3.TabIndex = 224
        Me.lblDrink3.Text = "00.00"
        Me.lblDrink3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDrink2
        '
        Me.lblDrink2.AutoSize = True
        Me.lblDrink2.BackColor = System.Drawing.Color.Transparent
        Me.lblDrink2.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDrink2.Location = New System.Drawing.Point(170, 90)
        Me.lblDrink2.Name = "lblDrink2"
        Me.lblDrink2.Size = New System.Drawing.Size(39, 17)
        Me.lblDrink2.TabIndex = 223
        Me.lblDrink2.Text = "00.00"
        Me.lblDrink2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDrink1
        '
        Me.lblDrink1.AutoSize = True
        Me.lblDrink1.BackColor = System.Drawing.Color.Transparent
        Me.lblDrink1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDrink1.Location = New System.Drawing.Point(50, 90)
        Me.lblDrink1.Name = "lblDrink1"
        Me.lblDrink1.Size = New System.Drawing.Size(39, 17)
        Me.lblDrink1.TabIndex = 222
        Me.lblDrink1.Text = "00.00"
        Me.lblDrink1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnDrink16
        '
        Me.btnDrink16.BackColor = System.Drawing.Color.White
        Me.btnDrink16.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnDrink16.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnDrink16.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnDrink16.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDrink16.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDrink16.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnDrink16.ImageIndex = 1
        Me.btnDrink16.ImageList = Me.ImageList1
        Me.btnDrink16.Location = New System.Drawing.Point(367, 318)
        Me.btnDrink16.Name = "btnDrink16"
        Me.btnDrink16.Size = New System.Drawing.Size(120, 105)
        Me.btnDrink16.TabIndex = 221
        Me.btnDrink16.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnDrink16.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnDrink16.UseVisualStyleBackColor = False
        '
        'btnDrink15
        '
        Me.btnDrink15.BackColor = System.Drawing.Color.White
        Me.btnDrink15.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnDrink15.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnDrink15.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnDrink15.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDrink15.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDrink15.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnDrink15.ImageIndex = 1
        Me.btnDrink15.ImageList = Me.ImageList1
        Me.btnDrink15.Location = New System.Drawing.Point(248, 318)
        Me.btnDrink15.Name = "btnDrink15"
        Me.btnDrink15.Size = New System.Drawing.Size(120, 105)
        Me.btnDrink15.TabIndex = 220
        Me.btnDrink15.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnDrink15.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnDrink15.UseVisualStyleBackColor = False
        '
        'btnDrink14
        '
        Me.btnDrink14.BackColor = System.Drawing.Color.White
        Me.btnDrink14.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnDrink14.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnDrink14.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnDrink14.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDrink14.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDrink14.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnDrink14.ImageIndex = 1
        Me.btnDrink14.ImageList = Me.ImageList1
        Me.btnDrink14.Location = New System.Drawing.Point(129, 318)
        Me.btnDrink14.Name = "btnDrink14"
        Me.btnDrink14.Size = New System.Drawing.Size(120, 105)
        Me.btnDrink14.TabIndex = 219
        Me.btnDrink14.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnDrink14.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnDrink14.UseVisualStyleBackColor = False
        '
        'btnDrink13
        '
        Me.btnDrink13.BackColor = System.Drawing.Color.White
        Me.btnDrink13.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnDrink13.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnDrink13.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnDrink13.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDrink13.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDrink13.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnDrink13.ImageIndex = 1
        Me.btnDrink13.ImageList = Me.ImageList1
        Me.btnDrink13.Location = New System.Drawing.Point(10, 318)
        Me.btnDrink13.Name = "btnDrink13"
        Me.btnDrink13.Size = New System.Drawing.Size(120, 105)
        Me.btnDrink13.TabIndex = 218
        Me.btnDrink13.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnDrink13.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnDrink13.UseVisualStyleBackColor = False
        '
        'btnDrink12
        '
        Me.btnDrink12.BackColor = System.Drawing.Color.White
        Me.btnDrink12.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnDrink12.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnDrink12.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnDrink12.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDrink12.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDrink12.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnDrink12.ImageIndex = 1
        Me.btnDrink12.ImageList = Me.ImageList1
        Me.btnDrink12.Location = New System.Drawing.Point(367, 214)
        Me.btnDrink12.Name = "btnDrink12"
        Me.btnDrink12.Size = New System.Drawing.Size(120, 105)
        Me.btnDrink12.TabIndex = 217
        Me.btnDrink12.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnDrink12.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnDrink12.UseVisualStyleBackColor = False
        '
        'btnDrink11
        '
        Me.btnDrink11.BackColor = System.Drawing.Color.White
        Me.btnDrink11.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnDrink11.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnDrink11.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnDrink11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDrink11.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDrink11.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnDrink11.ImageIndex = 1
        Me.btnDrink11.ImageList = Me.ImageList1
        Me.btnDrink11.Location = New System.Drawing.Point(248, 214)
        Me.btnDrink11.Name = "btnDrink11"
        Me.btnDrink11.Size = New System.Drawing.Size(120, 105)
        Me.btnDrink11.TabIndex = 216
        Me.btnDrink11.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnDrink11.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnDrink11.UseVisualStyleBackColor = False
        '
        'btnDrink10
        '
        Me.btnDrink10.BackColor = System.Drawing.Color.White
        Me.btnDrink10.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnDrink10.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnDrink10.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnDrink10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDrink10.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDrink10.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnDrink10.ImageIndex = 1
        Me.btnDrink10.ImageList = Me.ImageList1
        Me.btnDrink10.Location = New System.Drawing.Point(129, 214)
        Me.btnDrink10.Name = "btnDrink10"
        Me.btnDrink10.Size = New System.Drawing.Size(120, 105)
        Me.btnDrink10.TabIndex = 215
        Me.btnDrink10.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnDrink10.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnDrink10.UseVisualStyleBackColor = False
        '
        'btnDrink9
        '
        Me.btnDrink9.BackColor = System.Drawing.Color.White
        Me.btnDrink9.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnDrink9.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnDrink9.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnDrink9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDrink9.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDrink9.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnDrink9.ImageIndex = 1
        Me.btnDrink9.ImageList = Me.ImageList1
        Me.btnDrink9.Location = New System.Drawing.Point(10, 214)
        Me.btnDrink9.Name = "btnDrink9"
        Me.btnDrink9.Size = New System.Drawing.Size(120, 105)
        Me.btnDrink9.TabIndex = 214
        Me.btnDrink9.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnDrink9.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnDrink9.UseVisualStyleBackColor = False
        '
        'btnDrink8
        '
        Me.btnDrink8.BackColor = System.Drawing.Color.White
        Me.btnDrink8.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnDrink8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnDrink8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnDrink8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDrink8.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDrink8.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnDrink8.ImageIndex = 1
        Me.btnDrink8.ImageList = Me.ImageList1
        Me.btnDrink8.Location = New System.Drawing.Point(367, 111)
        Me.btnDrink8.Name = "btnDrink8"
        Me.btnDrink8.Size = New System.Drawing.Size(120, 105)
        Me.btnDrink8.TabIndex = 213
        Me.btnDrink8.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnDrink8.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnDrink8.UseVisualStyleBackColor = False
        '
        'btnDrink7
        '
        Me.btnDrink7.BackColor = System.Drawing.Color.White
        Me.btnDrink7.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnDrink7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnDrink7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnDrink7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDrink7.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDrink7.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnDrink7.ImageIndex = 1
        Me.btnDrink7.ImageList = Me.ImageList1
        Me.btnDrink7.Location = New System.Drawing.Point(248, 111)
        Me.btnDrink7.Name = "btnDrink7"
        Me.btnDrink7.Size = New System.Drawing.Size(120, 105)
        Me.btnDrink7.TabIndex = 212
        Me.btnDrink7.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnDrink7.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnDrink7.UseVisualStyleBackColor = False
        '
        'btnDrink6
        '
        Me.btnDrink6.BackColor = System.Drawing.Color.White
        Me.btnDrink6.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnDrink6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnDrink6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnDrink6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDrink6.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDrink6.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnDrink6.ImageIndex = 1
        Me.btnDrink6.ImageList = Me.ImageList1
        Me.btnDrink6.Location = New System.Drawing.Point(129, 111)
        Me.btnDrink6.Name = "btnDrink6"
        Me.btnDrink6.Size = New System.Drawing.Size(120, 105)
        Me.btnDrink6.TabIndex = 211
        Me.btnDrink6.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnDrink6.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnDrink6.UseVisualStyleBackColor = False
        '
        'btnDrink5
        '
        Me.btnDrink5.BackColor = System.Drawing.Color.White
        Me.btnDrink5.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnDrink5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnDrink5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnDrink5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDrink5.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDrink5.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnDrink5.ImageIndex = 1
        Me.btnDrink5.ImageList = Me.ImageList1
        Me.btnDrink5.Location = New System.Drawing.Point(10, 111)
        Me.btnDrink5.Name = "btnDrink5"
        Me.btnDrink5.Size = New System.Drawing.Size(120, 105)
        Me.btnDrink5.TabIndex = 210
        Me.btnDrink5.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnDrink5.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnDrink5.UseVisualStyleBackColor = False
        '
        'btnDrink4
        '
        Me.btnDrink4.BackColor = System.Drawing.Color.White
        Me.btnDrink4.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnDrink4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnDrink4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnDrink4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDrink4.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDrink4.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnDrink4.ImageIndex = 1
        Me.btnDrink4.ImageList = Me.ImageList1
        Me.btnDrink4.Location = New System.Drawing.Point(367, 7)
        Me.btnDrink4.Name = "btnDrink4"
        Me.btnDrink4.Size = New System.Drawing.Size(120, 105)
        Me.btnDrink4.TabIndex = 209
        Me.btnDrink4.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnDrink4.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnDrink4.UseVisualStyleBackColor = False
        '
        'btnDrink3
        '
        Me.btnDrink3.BackColor = System.Drawing.Color.White
        Me.btnDrink3.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnDrink3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnDrink3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnDrink3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDrink3.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDrink3.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnDrink3.ImageIndex = 1
        Me.btnDrink3.ImageList = Me.ImageList1
        Me.btnDrink3.Location = New System.Drawing.Point(248, 7)
        Me.btnDrink3.Name = "btnDrink3"
        Me.btnDrink3.Size = New System.Drawing.Size(120, 105)
        Me.btnDrink3.TabIndex = 208
        Me.btnDrink3.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnDrink3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnDrink3.UseVisualStyleBackColor = False
        '
        'btnDrink2
        '
        Me.btnDrink2.BackColor = System.Drawing.Color.White
        Me.btnDrink2.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnDrink2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnDrink2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnDrink2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDrink2.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDrink2.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnDrink2.ImageIndex = 1
        Me.btnDrink2.ImageList = Me.ImageList1
        Me.btnDrink2.Location = New System.Drawing.Point(129, 7)
        Me.btnDrink2.Name = "btnDrink2"
        Me.btnDrink2.Size = New System.Drawing.Size(120, 105)
        Me.btnDrink2.TabIndex = 207
        Me.btnDrink2.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnDrink2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnDrink2.UseVisualStyleBackColor = False
        '
        'btnDrink1
        '
        Me.btnDrink1.BackColor = System.Drawing.Color.White
        Me.btnDrink1.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnDrink1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnDrink1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnDrink1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDrink1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDrink1.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnDrink1.ImageIndex = 1
        Me.btnDrink1.ImageList = Me.ImageList1
        Me.btnDrink1.Location = New System.Drawing.Point(10, 7)
        Me.btnDrink1.Name = "btnDrink1"
        Me.btnDrink1.Size = New System.Drawing.Size(120, 105)
        Me.btnDrink1.TabIndex = 206
        Me.btnDrink1.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnDrink1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnDrink1.UseVisualStyleBackColor = False
        '
        'TabItem2
        '
        Me.TabItem2.AttachedControl = Me.TabControlPanel3
        Me.TabItem2.Image = CType(resources.GetObject("TabItem2.Image"), System.Drawing.Image)
        Me.TabItem2.Name = "TabItem2"
        Me.TabItem2.Text = "Drinks"
        '
        'TabControlPanel2
        '
        Me.TabControlPanel2.Controls.Add(Me.btnClearBurger16)
        Me.TabControlPanel2.Controls.Add(Me.btnClearBurger15)
        Me.TabControlPanel2.Controls.Add(Me.btnClearBurger14)
        Me.TabControlPanel2.Controls.Add(Me.btnClearBurger13)
        Me.TabControlPanel2.Controls.Add(Me.btnClearBurger12)
        Me.TabControlPanel2.Controls.Add(Me.btnClearBurger11)
        Me.TabControlPanel2.Controls.Add(Me.btnClearBurger10)
        Me.TabControlPanel2.Controls.Add(Me.btnClearBurger9)
        Me.TabControlPanel2.Controls.Add(Me.btnClearBurger8)
        Me.TabControlPanel2.Controls.Add(Me.btnClearBurger7)
        Me.TabControlPanel2.Controls.Add(Me.btnClearBurger6)
        Me.TabControlPanel2.Controls.Add(Me.btnClearBurger5)
        Me.TabControlPanel2.Controls.Add(Me.btnClearBurger4)
        Me.TabControlPanel2.Controls.Add(Me.btnClearBurger3)
        Me.TabControlPanel2.Controls.Add(Me.btnClearBurger2)
        Me.TabControlPanel2.Controls.Add(Me.btnClearBurger1)
        Me.TabControlPanel2.Controls.Add(Me.btnEditBurger16)
        Me.TabControlPanel2.Controls.Add(Me.btnEditBurger15)
        Me.TabControlPanel2.Controls.Add(Me.btnEditBurger14)
        Me.TabControlPanel2.Controls.Add(Me.btnEditBurger13)
        Me.TabControlPanel2.Controls.Add(Me.btnEditBurger12)
        Me.TabControlPanel2.Controls.Add(Me.btnEditBurger11)
        Me.TabControlPanel2.Controls.Add(Me.btnEditBurger10)
        Me.TabControlPanel2.Controls.Add(Me.btnEditBurger9)
        Me.TabControlPanel2.Controls.Add(Me.btnEditBurger8)
        Me.TabControlPanel2.Controls.Add(Me.btnEditBurger7)
        Me.TabControlPanel2.Controls.Add(Me.btnEditBurger6)
        Me.TabControlPanel2.Controls.Add(Me.btnEditBurger5)
        Me.TabControlPanel2.Controls.Add(Me.btnEditBurger4)
        Me.TabControlPanel2.Controls.Add(Me.btnEditBurger3)
        Me.TabControlPanel2.Controls.Add(Me.btnEditBurger2)
        Me.TabControlPanel2.Controls.Add(Me.btnEditBurger1)
        Me.TabControlPanel2.Controls.Add(Me.lblBurger16)
        Me.TabControlPanel2.Controls.Add(Me.lblBurger15)
        Me.TabControlPanel2.Controls.Add(Me.lblBurger14)
        Me.TabControlPanel2.Controls.Add(Me.lblBurger13)
        Me.TabControlPanel2.Controls.Add(Me.lblBurger12)
        Me.TabControlPanel2.Controls.Add(Me.lblBurger11)
        Me.TabControlPanel2.Controls.Add(Me.lblBurger10)
        Me.TabControlPanel2.Controls.Add(Me.lblBurger9)
        Me.TabControlPanel2.Controls.Add(Me.lblBurger8)
        Me.TabControlPanel2.Controls.Add(Me.lblBurger7)
        Me.TabControlPanel2.Controls.Add(Me.lblBurger6)
        Me.TabControlPanel2.Controls.Add(Me.lblBurger5)
        Me.TabControlPanel2.Controls.Add(Me.lblBurger4)
        Me.TabControlPanel2.Controls.Add(Me.lblBurger3)
        Me.TabControlPanel2.Controls.Add(Me.lblBurger2)
        Me.TabControlPanel2.Controls.Add(Me.lblBurger1)
        Me.TabControlPanel2.Controls.Add(Me.btnBurger16)
        Me.TabControlPanel2.Controls.Add(Me.btnBurger15)
        Me.TabControlPanel2.Controls.Add(Me.btnBurger14)
        Me.TabControlPanel2.Controls.Add(Me.btnBurger13)
        Me.TabControlPanel2.Controls.Add(Me.btnBurger12)
        Me.TabControlPanel2.Controls.Add(Me.btnBurger11)
        Me.TabControlPanel2.Controls.Add(Me.btnBurger10)
        Me.TabControlPanel2.Controls.Add(Me.btnBurger9)
        Me.TabControlPanel2.Controls.Add(Me.btnBurger8)
        Me.TabControlPanel2.Controls.Add(Me.btnBurger7)
        Me.TabControlPanel2.Controls.Add(Me.btnBurger6)
        Me.TabControlPanel2.Controls.Add(Me.btnBurger5)
        Me.TabControlPanel2.Controls.Add(Me.btnBurger4)
        Me.TabControlPanel2.Controls.Add(Me.btnBurger3)
        Me.TabControlPanel2.Controls.Add(Me.btnBurger2)
        Me.TabControlPanel2.Controls.Add(Me.btnBurger1)
        Me.TabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel2.Location = New System.Drawing.Point(0, 40)
        Me.TabControlPanel2.Name = "TabControlPanel2"
        Me.TabControlPanel2.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel2.Size = New System.Drawing.Size(497, 430)
        Me.TabControlPanel2.Style.BackColor1.Color = System.Drawing.Color.White
        Me.TabControlPanel2.Style.BackColor2.Color = System.Drawing.Color.White
        Me.TabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel2.Style.BorderColor.Color = System.Drawing.Color.Black
        Me.TabControlPanel2.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel2.Style.GradientAngle = 90
        Me.TabControlPanel2.TabIndex = 1
        Me.TabControlPanel2.TabItem = Me.TabItem1
        '
        'btnClearBurger16
        '
        Me.btnClearBurger16.BackColor = System.Drawing.Color.White
        Me.btnClearBurger16.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearBurger16.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearBurger16.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearBurger16.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearBurger16.Image = CType(resources.GetObject("btnClearBurger16.Image"), System.Drawing.Image)
        Me.btnClearBurger16.Location = New System.Drawing.Point(367, 318)
        Me.btnClearBurger16.Name = "btnClearBurger16"
        Me.btnClearBurger16.Size = New System.Drawing.Size(25, 25)
        Me.btnClearBurger16.TabIndex = 205
        Me.btnClearBurger16.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearBurger16.UseVisualStyleBackColor = False
        '
        'btnClearBurger15
        '
        Me.btnClearBurger15.BackColor = System.Drawing.Color.White
        Me.btnClearBurger15.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearBurger15.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearBurger15.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearBurger15.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearBurger15.Image = CType(resources.GetObject("btnClearBurger15.Image"), System.Drawing.Image)
        Me.btnClearBurger15.Location = New System.Drawing.Point(248, 318)
        Me.btnClearBurger15.Name = "btnClearBurger15"
        Me.btnClearBurger15.Size = New System.Drawing.Size(25, 25)
        Me.btnClearBurger15.TabIndex = 204
        Me.btnClearBurger15.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearBurger15.UseVisualStyleBackColor = False
        '
        'btnClearBurger14
        '
        Me.btnClearBurger14.BackColor = System.Drawing.Color.White
        Me.btnClearBurger14.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearBurger14.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearBurger14.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearBurger14.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearBurger14.Image = CType(resources.GetObject("btnClearBurger14.Image"), System.Drawing.Image)
        Me.btnClearBurger14.Location = New System.Drawing.Point(129, 318)
        Me.btnClearBurger14.Name = "btnClearBurger14"
        Me.btnClearBurger14.Size = New System.Drawing.Size(25, 25)
        Me.btnClearBurger14.TabIndex = 203
        Me.btnClearBurger14.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearBurger14.UseVisualStyleBackColor = False
        '
        'btnClearBurger13
        '
        Me.btnClearBurger13.BackColor = System.Drawing.Color.White
        Me.btnClearBurger13.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearBurger13.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearBurger13.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearBurger13.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearBurger13.Image = CType(resources.GetObject("btnClearBurger13.Image"), System.Drawing.Image)
        Me.btnClearBurger13.Location = New System.Drawing.Point(10, 318)
        Me.btnClearBurger13.Name = "btnClearBurger13"
        Me.btnClearBurger13.Size = New System.Drawing.Size(25, 25)
        Me.btnClearBurger13.TabIndex = 202
        Me.btnClearBurger13.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearBurger13.UseVisualStyleBackColor = False
        '
        'btnClearBurger12
        '
        Me.btnClearBurger12.BackColor = System.Drawing.Color.White
        Me.btnClearBurger12.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearBurger12.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearBurger12.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearBurger12.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearBurger12.Image = CType(resources.GetObject("btnClearBurger12.Image"), System.Drawing.Image)
        Me.btnClearBurger12.Location = New System.Drawing.Point(367, 214)
        Me.btnClearBurger12.Name = "btnClearBurger12"
        Me.btnClearBurger12.Size = New System.Drawing.Size(25, 25)
        Me.btnClearBurger12.TabIndex = 201
        Me.btnClearBurger12.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearBurger12.UseVisualStyleBackColor = False
        '
        'btnClearBurger11
        '
        Me.btnClearBurger11.BackColor = System.Drawing.Color.White
        Me.btnClearBurger11.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearBurger11.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearBurger11.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearBurger11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearBurger11.Image = CType(resources.GetObject("btnClearBurger11.Image"), System.Drawing.Image)
        Me.btnClearBurger11.Location = New System.Drawing.Point(248, 214)
        Me.btnClearBurger11.Name = "btnClearBurger11"
        Me.btnClearBurger11.Size = New System.Drawing.Size(25, 25)
        Me.btnClearBurger11.TabIndex = 200
        Me.btnClearBurger11.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearBurger11.UseVisualStyleBackColor = False
        '
        'btnClearBurger10
        '
        Me.btnClearBurger10.BackColor = System.Drawing.Color.White
        Me.btnClearBurger10.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearBurger10.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearBurger10.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearBurger10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearBurger10.Image = CType(resources.GetObject("btnClearBurger10.Image"), System.Drawing.Image)
        Me.btnClearBurger10.Location = New System.Drawing.Point(129, 214)
        Me.btnClearBurger10.Name = "btnClearBurger10"
        Me.btnClearBurger10.Size = New System.Drawing.Size(25, 25)
        Me.btnClearBurger10.TabIndex = 199
        Me.btnClearBurger10.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearBurger10.UseVisualStyleBackColor = False
        '
        'btnClearBurger9
        '
        Me.btnClearBurger9.BackColor = System.Drawing.Color.White
        Me.btnClearBurger9.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearBurger9.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearBurger9.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearBurger9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearBurger9.Image = CType(resources.GetObject("btnClearBurger9.Image"), System.Drawing.Image)
        Me.btnClearBurger9.Location = New System.Drawing.Point(10, 214)
        Me.btnClearBurger9.Name = "btnClearBurger9"
        Me.btnClearBurger9.Size = New System.Drawing.Size(25, 25)
        Me.btnClearBurger9.TabIndex = 198
        Me.btnClearBurger9.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearBurger9.UseVisualStyleBackColor = False
        '
        'btnClearBurger8
        '
        Me.btnClearBurger8.BackColor = System.Drawing.Color.White
        Me.btnClearBurger8.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearBurger8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearBurger8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearBurger8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearBurger8.Image = CType(resources.GetObject("btnClearBurger8.Image"), System.Drawing.Image)
        Me.btnClearBurger8.Location = New System.Drawing.Point(367, 111)
        Me.btnClearBurger8.Name = "btnClearBurger8"
        Me.btnClearBurger8.Size = New System.Drawing.Size(25, 25)
        Me.btnClearBurger8.TabIndex = 197
        Me.btnClearBurger8.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearBurger8.UseVisualStyleBackColor = False
        '
        'btnClearBurger7
        '
        Me.btnClearBurger7.BackColor = System.Drawing.Color.White
        Me.btnClearBurger7.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearBurger7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearBurger7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearBurger7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearBurger7.Image = CType(resources.GetObject("btnClearBurger7.Image"), System.Drawing.Image)
        Me.btnClearBurger7.Location = New System.Drawing.Point(248, 111)
        Me.btnClearBurger7.Name = "btnClearBurger7"
        Me.btnClearBurger7.Size = New System.Drawing.Size(25, 25)
        Me.btnClearBurger7.TabIndex = 196
        Me.btnClearBurger7.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearBurger7.UseVisualStyleBackColor = False
        '
        'btnClearBurger6
        '
        Me.btnClearBurger6.BackColor = System.Drawing.Color.White
        Me.btnClearBurger6.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearBurger6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearBurger6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearBurger6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearBurger6.Image = CType(resources.GetObject("btnClearBurger6.Image"), System.Drawing.Image)
        Me.btnClearBurger6.Location = New System.Drawing.Point(129, 111)
        Me.btnClearBurger6.Name = "btnClearBurger6"
        Me.btnClearBurger6.Size = New System.Drawing.Size(25, 25)
        Me.btnClearBurger6.TabIndex = 195
        Me.btnClearBurger6.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearBurger6.UseVisualStyleBackColor = False
        '
        'btnClearBurger5
        '
        Me.btnClearBurger5.BackColor = System.Drawing.Color.White
        Me.btnClearBurger5.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearBurger5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearBurger5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearBurger5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearBurger5.Image = CType(resources.GetObject("btnClearBurger5.Image"), System.Drawing.Image)
        Me.btnClearBurger5.Location = New System.Drawing.Point(10, 111)
        Me.btnClearBurger5.Name = "btnClearBurger5"
        Me.btnClearBurger5.Size = New System.Drawing.Size(25, 25)
        Me.btnClearBurger5.TabIndex = 194
        Me.btnClearBurger5.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearBurger5.UseVisualStyleBackColor = False
        '
        'btnClearBurger4
        '
        Me.btnClearBurger4.BackColor = System.Drawing.Color.White
        Me.btnClearBurger4.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearBurger4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearBurger4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearBurger4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearBurger4.Image = CType(resources.GetObject("btnClearBurger4.Image"), System.Drawing.Image)
        Me.btnClearBurger4.Location = New System.Drawing.Point(367, 7)
        Me.btnClearBurger4.Name = "btnClearBurger4"
        Me.btnClearBurger4.Size = New System.Drawing.Size(25, 25)
        Me.btnClearBurger4.TabIndex = 193
        Me.btnClearBurger4.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearBurger4.UseVisualStyleBackColor = False
        '
        'btnClearBurger3
        '
        Me.btnClearBurger3.BackColor = System.Drawing.Color.White
        Me.btnClearBurger3.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearBurger3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearBurger3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearBurger3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearBurger3.Image = CType(resources.GetObject("btnClearBurger3.Image"), System.Drawing.Image)
        Me.btnClearBurger3.Location = New System.Drawing.Point(248, 7)
        Me.btnClearBurger3.Name = "btnClearBurger3"
        Me.btnClearBurger3.Size = New System.Drawing.Size(25, 25)
        Me.btnClearBurger3.TabIndex = 192
        Me.btnClearBurger3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearBurger3.UseVisualStyleBackColor = False
        '
        'btnClearBurger2
        '
        Me.btnClearBurger2.BackColor = System.Drawing.Color.White
        Me.btnClearBurger2.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearBurger2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearBurger2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearBurger2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearBurger2.Image = CType(resources.GetObject("btnClearBurger2.Image"), System.Drawing.Image)
        Me.btnClearBurger2.Location = New System.Drawing.Point(129, 7)
        Me.btnClearBurger2.Name = "btnClearBurger2"
        Me.btnClearBurger2.Size = New System.Drawing.Size(25, 25)
        Me.btnClearBurger2.TabIndex = 191
        Me.btnClearBurger2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearBurger2.UseVisualStyleBackColor = False
        '
        'btnClearBurger1
        '
        Me.btnClearBurger1.BackColor = System.Drawing.Color.White
        Me.btnClearBurger1.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearBurger1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearBurger1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearBurger1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearBurger1.Image = CType(resources.GetObject("btnClearBurger1.Image"), System.Drawing.Image)
        Me.btnClearBurger1.Location = New System.Drawing.Point(10, 7)
        Me.btnClearBurger1.Name = "btnClearBurger1"
        Me.btnClearBurger1.Size = New System.Drawing.Size(25, 25)
        Me.btnClearBurger1.TabIndex = 190
        Me.btnClearBurger1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearBurger1.UseVisualStyleBackColor = False
        '
        'btnEditBurger16
        '
        Me.btnEditBurger16.BackColor = System.Drawing.Color.White
        Me.btnEditBurger16.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditBurger16.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditBurger16.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditBurger16.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditBurger16.Image = CType(resources.GetObject("btnEditBurger16.Image"), System.Drawing.Image)
        Me.btnEditBurger16.Location = New System.Drawing.Point(462, 318)
        Me.btnEditBurger16.Name = "btnEditBurger16"
        Me.btnEditBurger16.Size = New System.Drawing.Size(25, 25)
        Me.btnEditBurger16.TabIndex = 189
        Me.btnEditBurger16.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditBurger16.UseVisualStyleBackColor = False
        '
        'btnEditBurger15
        '
        Me.btnEditBurger15.BackColor = System.Drawing.Color.White
        Me.btnEditBurger15.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditBurger15.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditBurger15.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditBurger15.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditBurger15.Image = CType(resources.GetObject("btnEditBurger15.Image"), System.Drawing.Image)
        Me.btnEditBurger15.Location = New System.Drawing.Point(343, 318)
        Me.btnEditBurger15.Name = "btnEditBurger15"
        Me.btnEditBurger15.Size = New System.Drawing.Size(25, 25)
        Me.btnEditBurger15.TabIndex = 188
        Me.btnEditBurger15.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditBurger15.UseVisualStyleBackColor = False
        '
        'btnEditBurger14
        '
        Me.btnEditBurger14.BackColor = System.Drawing.Color.White
        Me.btnEditBurger14.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditBurger14.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditBurger14.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditBurger14.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditBurger14.Image = CType(resources.GetObject("btnEditBurger14.Image"), System.Drawing.Image)
        Me.btnEditBurger14.Location = New System.Drawing.Point(224, 318)
        Me.btnEditBurger14.Name = "btnEditBurger14"
        Me.btnEditBurger14.Size = New System.Drawing.Size(25, 25)
        Me.btnEditBurger14.TabIndex = 187
        Me.btnEditBurger14.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditBurger14.UseVisualStyleBackColor = False
        '
        'btnEditBurger13
        '
        Me.btnEditBurger13.BackColor = System.Drawing.Color.White
        Me.btnEditBurger13.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditBurger13.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditBurger13.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditBurger13.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditBurger13.Image = CType(resources.GetObject("btnEditBurger13.Image"), System.Drawing.Image)
        Me.btnEditBurger13.Location = New System.Drawing.Point(105, 318)
        Me.btnEditBurger13.Name = "btnEditBurger13"
        Me.btnEditBurger13.Size = New System.Drawing.Size(25, 25)
        Me.btnEditBurger13.TabIndex = 186
        Me.btnEditBurger13.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditBurger13.UseVisualStyleBackColor = False
        '
        'btnEditBurger12
        '
        Me.btnEditBurger12.BackColor = System.Drawing.Color.White
        Me.btnEditBurger12.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditBurger12.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditBurger12.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditBurger12.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditBurger12.Image = CType(resources.GetObject("btnEditBurger12.Image"), System.Drawing.Image)
        Me.btnEditBurger12.Location = New System.Drawing.Point(462, 214)
        Me.btnEditBurger12.Name = "btnEditBurger12"
        Me.btnEditBurger12.Size = New System.Drawing.Size(25, 25)
        Me.btnEditBurger12.TabIndex = 185
        Me.btnEditBurger12.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditBurger12.UseVisualStyleBackColor = False
        '
        'btnEditBurger11
        '
        Me.btnEditBurger11.BackColor = System.Drawing.Color.White
        Me.btnEditBurger11.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditBurger11.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditBurger11.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditBurger11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditBurger11.Image = CType(resources.GetObject("btnEditBurger11.Image"), System.Drawing.Image)
        Me.btnEditBurger11.Location = New System.Drawing.Point(343, 214)
        Me.btnEditBurger11.Name = "btnEditBurger11"
        Me.btnEditBurger11.Size = New System.Drawing.Size(25, 25)
        Me.btnEditBurger11.TabIndex = 184
        Me.btnEditBurger11.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditBurger11.UseVisualStyleBackColor = False
        '
        'btnEditBurger10
        '
        Me.btnEditBurger10.BackColor = System.Drawing.Color.White
        Me.btnEditBurger10.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditBurger10.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditBurger10.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditBurger10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditBurger10.Image = CType(resources.GetObject("btnEditBurger10.Image"), System.Drawing.Image)
        Me.btnEditBurger10.Location = New System.Drawing.Point(224, 214)
        Me.btnEditBurger10.Name = "btnEditBurger10"
        Me.btnEditBurger10.Size = New System.Drawing.Size(25, 25)
        Me.btnEditBurger10.TabIndex = 183
        Me.btnEditBurger10.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditBurger10.UseVisualStyleBackColor = False
        '
        'btnEditBurger9
        '
        Me.btnEditBurger9.BackColor = System.Drawing.Color.White
        Me.btnEditBurger9.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditBurger9.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditBurger9.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditBurger9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditBurger9.Image = CType(resources.GetObject("btnEditBurger9.Image"), System.Drawing.Image)
        Me.btnEditBurger9.Location = New System.Drawing.Point(105, 214)
        Me.btnEditBurger9.Name = "btnEditBurger9"
        Me.btnEditBurger9.Size = New System.Drawing.Size(25, 25)
        Me.btnEditBurger9.TabIndex = 182
        Me.btnEditBurger9.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditBurger9.UseVisualStyleBackColor = False
        '
        'btnEditBurger8
        '
        Me.btnEditBurger8.BackColor = System.Drawing.Color.White
        Me.btnEditBurger8.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditBurger8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditBurger8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditBurger8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditBurger8.Image = CType(resources.GetObject("btnEditBurger8.Image"), System.Drawing.Image)
        Me.btnEditBurger8.Location = New System.Drawing.Point(462, 111)
        Me.btnEditBurger8.Name = "btnEditBurger8"
        Me.btnEditBurger8.Size = New System.Drawing.Size(25, 25)
        Me.btnEditBurger8.TabIndex = 181
        Me.btnEditBurger8.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditBurger8.UseVisualStyleBackColor = False
        '
        'btnEditBurger7
        '
        Me.btnEditBurger7.BackColor = System.Drawing.Color.White
        Me.btnEditBurger7.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditBurger7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditBurger7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditBurger7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditBurger7.Image = CType(resources.GetObject("btnEditBurger7.Image"), System.Drawing.Image)
        Me.btnEditBurger7.Location = New System.Drawing.Point(343, 111)
        Me.btnEditBurger7.Name = "btnEditBurger7"
        Me.btnEditBurger7.Size = New System.Drawing.Size(25, 25)
        Me.btnEditBurger7.TabIndex = 180
        Me.btnEditBurger7.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditBurger7.UseVisualStyleBackColor = False
        '
        'btnEditBurger6
        '
        Me.btnEditBurger6.BackColor = System.Drawing.Color.White
        Me.btnEditBurger6.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditBurger6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditBurger6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditBurger6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditBurger6.Image = CType(resources.GetObject("btnEditBurger6.Image"), System.Drawing.Image)
        Me.btnEditBurger6.Location = New System.Drawing.Point(224, 111)
        Me.btnEditBurger6.Name = "btnEditBurger6"
        Me.btnEditBurger6.Size = New System.Drawing.Size(25, 25)
        Me.btnEditBurger6.TabIndex = 179
        Me.btnEditBurger6.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditBurger6.UseVisualStyleBackColor = False
        '
        'btnEditBurger5
        '
        Me.btnEditBurger5.BackColor = System.Drawing.Color.White
        Me.btnEditBurger5.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditBurger5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditBurger5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditBurger5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditBurger5.Image = CType(resources.GetObject("btnEditBurger5.Image"), System.Drawing.Image)
        Me.btnEditBurger5.Location = New System.Drawing.Point(105, 111)
        Me.btnEditBurger5.Name = "btnEditBurger5"
        Me.btnEditBurger5.Size = New System.Drawing.Size(25, 25)
        Me.btnEditBurger5.TabIndex = 178
        Me.btnEditBurger5.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditBurger5.UseVisualStyleBackColor = False
        '
        'btnEditBurger4
        '
        Me.btnEditBurger4.BackColor = System.Drawing.Color.White
        Me.btnEditBurger4.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditBurger4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditBurger4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditBurger4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditBurger4.Image = CType(resources.GetObject("btnEditBurger4.Image"), System.Drawing.Image)
        Me.btnEditBurger4.Location = New System.Drawing.Point(462, 7)
        Me.btnEditBurger4.Name = "btnEditBurger4"
        Me.btnEditBurger4.Size = New System.Drawing.Size(25, 25)
        Me.btnEditBurger4.TabIndex = 177
        Me.btnEditBurger4.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditBurger4.UseVisualStyleBackColor = False
        '
        'btnEditBurger3
        '
        Me.btnEditBurger3.BackColor = System.Drawing.Color.White
        Me.btnEditBurger3.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditBurger3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditBurger3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditBurger3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditBurger3.Image = CType(resources.GetObject("btnEditBurger3.Image"), System.Drawing.Image)
        Me.btnEditBurger3.Location = New System.Drawing.Point(343, 7)
        Me.btnEditBurger3.Name = "btnEditBurger3"
        Me.btnEditBurger3.Size = New System.Drawing.Size(25, 25)
        Me.btnEditBurger3.TabIndex = 176
        Me.btnEditBurger3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditBurger3.UseVisualStyleBackColor = False
        '
        'btnEditBurger2
        '
        Me.btnEditBurger2.BackColor = System.Drawing.Color.White
        Me.btnEditBurger2.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditBurger2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditBurger2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditBurger2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditBurger2.Image = CType(resources.GetObject("btnEditBurger2.Image"), System.Drawing.Image)
        Me.btnEditBurger2.Location = New System.Drawing.Point(224, 7)
        Me.btnEditBurger2.Name = "btnEditBurger2"
        Me.btnEditBurger2.Size = New System.Drawing.Size(25, 25)
        Me.btnEditBurger2.TabIndex = 175
        Me.btnEditBurger2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditBurger2.UseVisualStyleBackColor = False
        '
        'btnEditBurger1
        '
        Me.btnEditBurger1.BackColor = System.Drawing.Color.White
        Me.btnEditBurger1.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditBurger1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditBurger1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditBurger1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditBurger1.Image = CType(resources.GetObject("btnEditBurger1.Image"), System.Drawing.Image)
        Me.btnEditBurger1.Location = New System.Drawing.Point(105, 7)
        Me.btnEditBurger1.Name = "btnEditBurger1"
        Me.btnEditBurger1.Size = New System.Drawing.Size(25, 25)
        Me.btnEditBurger1.TabIndex = 174
        Me.btnEditBurger1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditBurger1.UseVisualStyleBackColor = False
        '
        'lblBurger16
        '
        Me.lblBurger16.AutoSize = True
        Me.lblBurger16.BackColor = System.Drawing.Color.Transparent
        Me.lblBurger16.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBurger16.Location = New System.Drawing.Point(408, 401)
        Me.lblBurger16.Name = "lblBurger16"
        Me.lblBurger16.Size = New System.Drawing.Size(39, 17)
        Me.lblBurger16.TabIndex = 173
        Me.lblBurger16.Text = "00.00"
        Me.lblBurger16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblBurger15
        '
        Me.lblBurger15.AutoSize = True
        Me.lblBurger15.BackColor = System.Drawing.Color.Transparent
        Me.lblBurger15.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBurger15.Location = New System.Drawing.Point(291, 401)
        Me.lblBurger15.Name = "lblBurger15"
        Me.lblBurger15.Size = New System.Drawing.Size(39, 17)
        Me.lblBurger15.TabIndex = 172
        Me.lblBurger15.Text = "00.00"
        Me.lblBurger15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblBurger14
        '
        Me.lblBurger14.AutoSize = True
        Me.lblBurger14.BackColor = System.Drawing.Color.Transparent
        Me.lblBurger14.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBurger14.Location = New System.Drawing.Point(170, 401)
        Me.lblBurger14.Name = "lblBurger14"
        Me.lblBurger14.Size = New System.Drawing.Size(39, 17)
        Me.lblBurger14.TabIndex = 171
        Me.lblBurger14.Text = "00.00"
        Me.lblBurger14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblBurger13
        '
        Me.lblBurger13.AutoSize = True
        Me.lblBurger13.BackColor = System.Drawing.Color.Transparent
        Me.lblBurger13.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBurger13.Location = New System.Drawing.Point(50, 401)
        Me.lblBurger13.Name = "lblBurger13"
        Me.lblBurger13.Size = New System.Drawing.Size(39, 17)
        Me.lblBurger13.TabIndex = 170
        Me.lblBurger13.Text = "00.00"
        Me.lblBurger13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblBurger12
        '
        Me.lblBurger12.AutoSize = True
        Me.lblBurger12.BackColor = System.Drawing.Color.Transparent
        Me.lblBurger12.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBurger12.Location = New System.Drawing.Point(408, 297)
        Me.lblBurger12.Name = "lblBurger12"
        Me.lblBurger12.Size = New System.Drawing.Size(39, 17)
        Me.lblBurger12.TabIndex = 169
        Me.lblBurger12.Text = "00.00"
        Me.lblBurger12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblBurger11
        '
        Me.lblBurger11.AutoSize = True
        Me.lblBurger11.BackColor = System.Drawing.Color.Transparent
        Me.lblBurger11.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBurger11.Location = New System.Drawing.Point(291, 297)
        Me.lblBurger11.Name = "lblBurger11"
        Me.lblBurger11.Size = New System.Drawing.Size(39, 17)
        Me.lblBurger11.TabIndex = 168
        Me.lblBurger11.Text = "00.00"
        Me.lblBurger11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblBurger10
        '
        Me.lblBurger10.AutoSize = True
        Me.lblBurger10.BackColor = System.Drawing.Color.Transparent
        Me.lblBurger10.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBurger10.Location = New System.Drawing.Point(170, 297)
        Me.lblBurger10.Name = "lblBurger10"
        Me.lblBurger10.Size = New System.Drawing.Size(39, 17)
        Me.lblBurger10.TabIndex = 167
        Me.lblBurger10.Text = "00.00"
        Me.lblBurger10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblBurger9
        '
        Me.lblBurger9.AutoSize = True
        Me.lblBurger9.BackColor = System.Drawing.Color.Transparent
        Me.lblBurger9.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBurger9.Location = New System.Drawing.Point(50, 297)
        Me.lblBurger9.Name = "lblBurger9"
        Me.lblBurger9.Size = New System.Drawing.Size(39, 17)
        Me.lblBurger9.TabIndex = 166
        Me.lblBurger9.Text = "00.00"
        Me.lblBurger9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblBurger8
        '
        Me.lblBurger8.AutoSize = True
        Me.lblBurger8.BackColor = System.Drawing.Color.Transparent
        Me.lblBurger8.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBurger8.Location = New System.Drawing.Point(408, 194)
        Me.lblBurger8.Name = "lblBurger8"
        Me.lblBurger8.Size = New System.Drawing.Size(39, 17)
        Me.lblBurger8.TabIndex = 165
        Me.lblBurger8.Text = "00.00"
        Me.lblBurger8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblBurger7
        '
        Me.lblBurger7.AutoSize = True
        Me.lblBurger7.BackColor = System.Drawing.Color.Transparent
        Me.lblBurger7.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBurger7.Location = New System.Drawing.Point(291, 194)
        Me.lblBurger7.Name = "lblBurger7"
        Me.lblBurger7.Size = New System.Drawing.Size(39, 17)
        Me.lblBurger7.TabIndex = 164
        Me.lblBurger7.Text = "00.00"
        Me.lblBurger7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblBurger6
        '
        Me.lblBurger6.AutoSize = True
        Me.lblBurger6.BackColor = System.Drawing.Color.Transparent
        Me.lblBurger6.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBurger6.Location = New System.Drawing.Point(170, 194)
        Me.lblBurger6.Name = "lblBurger6"
        Me.lblBurger6.Size = New System.Drawing.Size(39, 17)
        Me.lblBurger6.TabIndex = 163
        Me.lblBurger6.Text = "00.00"
        Me.lblBurger6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblBurger5
        '
        Me.lblBurger5.AutoSize = True
        Me.lblBurger5.BackColor = System.Drawing.Color.Transparent
        Me.lblBurger5.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBurger5.Location = New System.Drawing.Point(50, 194)
        Me.lblBurger5.Name = "lblBurger5"
        Me.lblBurger5.Size = New System.Drawing.Size(39, 17)
        Me.lblBurger5.TabIndex = 162
        Me.lblBurger5.Text = "00.00"
        Me.lblBurger5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblBurger4
        '
        Me.lblBurger4.AutoSize = True
        Me.lblBurger4.BackColor = System.Drawing.Color.Transparent
        Me.lblBurger4.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBurger4.Location = New System.Drawing.Point(408, 90)
        Me.lblBurger4.Name = "lblBurger4"
        Me.lblBurger4.Size = New System.Drawing.Size(39, 17)
        Me.lblBurger4.TabIndex = 161
        Me.lblBurger4.Text = "00.00"
        Me.lblBurger4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblBurger3
        '
        Me.lblBurger3.AutoSize = True
        Me.lblBurger3.BackColor = System.Drawing.Color.Transparent
        Me.lblBurger3.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBurger3.Location = New System.Drawing.Point(291, 90)
        Me.lblBurger3.Name = "lblBurger3"
        Me.lblBurger3.Size = New System.Drawing.Size(39, 17)
        Me.lblBurger3.TabIndex = 160
        Me.lblBurger3.Text = "00.00"
        Me.lblBurger3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblBurger2
        '
        Me.lblBurger2.AutoSize = True
        Me.lblBurger2.BackColor = System.Drawing.Color.Transparent
        Me.lblBurger2.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBurger2.Location = New System.Drawing.Point(170, 90)
        Me.lblBurger2.Name = "lblBurger2"
        Me.lblBurger2.Size = New System.Drawing.Size(39, 17)
        Me.lblBurger2.TabIndex = 159
        Me.lblBurger2.Text = "00.00"
        Me.lblBurger2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblBurger1
        '
        Me.lblBurger1.AutoSize = True
        Me.lblBurger1.BackColor = System.Drawing.Color.Transparent
        Me.lblBurger1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBurger1.Location = New System.Drawing.Point(50, 90)
        Me.lblBurger1.Name = "lblBurger1"
        Me.lblBurger1.Size = New System.Drawing.Size(39, 17)
        Me.lblBurger1.TabIndex = 158
        Me.lblBurger1.Text = "00.00"
        Me.lblBurger1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnBurger16
        '
        Me.btnBurger16.BackColor = System.Drawing.Color.White
        Me.btnBurger16.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnBurger16.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnBurger16.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnBurger16.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBurger16.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBurger16.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnBurger16.ImageIndex = 1
        Me.btnBurger16.ImageList = Me.ImageList1
        Me.btnBurger16.Location = New System.Drawing.Point(367, 318)
        Me.btnBurger16.Name = "btnBurger16"
        Me.btnBurger16.Size = New System.Drawing.Size(120, 105)
        Me.btnBurger16.TabIndex = 157
        Me.btnBurger16.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnBurger16.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnBurger16.UseVisualStyleBackColor = False
        '
        'btnBurger15
        '
        Me.btnBurger15.BackColor = System.Drawing.Color.White
        Me.btnBurger15.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnBurger15.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnBurger15.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnBurger15.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBurger15.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBurger15.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnBurger15.ImageIndex = 1
        Me.btnBurger15.ImageList = Me.ImageList1
        Me.btnBurger15.Location = New System.Drawing.Point(248, 318)
        Me.btnBurger15.Name = "btnBurger15"
        Me.btnBurger15.Size = New System.Drawing.Size(120, 105)
        Me.btnBurger15.TabIndex = 156
        Me.btnBurger15.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnBurger15.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnBurger15.UseVisualStyleBackColor = False
        '
        'btnBurger14
        '
        Me.btnBurger14.BackColor = System.Drawing.Color.White
        Me.btnBurger14.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnBurger14.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnBurger14.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnBurger14.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBurger14.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBurger14.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnBurger14.ImageIndex = 1
        Me.btnBurger14.ImageList = Me.ImageList1
        Me.btnBurger14.Location = New System.Drawing.Point(129, 318)
        Me.btnBurger14.Name = "btnBurger14"
        Me.btnBurger14.Size = New System.Drawing.Size(120, 105)
        Me.btnBurger14.TabIndex = 155
        Me.btnBurger14.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnBurger14.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnBurger14.UseVisualStyleBackColor = False
        '
        'btnBurger13
        '
        Me.btnBurger13.BackColor = System.Drawing.Color.White
        Me.btnBurger13.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnBurger13.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnBurger13.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnBurger13.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBurger13.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBurger13.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnBurger13.ImageIndex = 1
        Me.btnBurger13.ImageList = Me.ImageList1
        Me.btnBurger13.Location = New System.Drawing.Point(10, 318)
        Me.btnBurger13.Name = "btnBurger13"
        Me.btnBurger13.Size = New System.Drawing.Size(120, 105)
        Me.btnBurger13.TabIndex = 154
        Me.btnBurger13.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnBurger13.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnBurger13.UseVisualStyleBackColor = False
        '
        'btnBurger12
        '
        Me.btnBurger12.BackColor = System.Drawing.Color.White
        Me.btnBurger12.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnBurger12.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnBurger12.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnBurger12.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBurger12.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBurger12.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnBurger12.ImageIndex = 1
        Me.btnBurger12.ImageList = Me.ImageList1
        Me.btnBurger12.Location = New System.Drawing.Point(367, 214)
        Me.btnBurger12.Name = "btnBurger12"
        Me.btnBurger12.Size = New System.Drawing.Size(120, 105)
        Me.btnBurger12.TabIndex = 153
        Me.btnBurger12.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnBurger12.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnBurger12.UseVisualStyleBackColor = False
        '
        'btnBurger11
        '
        Me.btnBurger11.BackColor = System.Drawing.Color.White
        Me.btnBurger11.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnBurger11.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnBurger11.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnBurger11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBurger11.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBurger11.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnBurger11.ImageIndex = 1
        Me.btnBurger11.ImageList = Me.ImageList1
        Me.btnBurger11.Location = New System.Drawing.Point(248, 214)
        Me.btnBurger11.Name = "btnBurger11"
        Me.btnBurger11.Size = New System.Drawing.Size(120, 105)
        Me.btnBurger11.TabIndex = 152
        Me.btnBurger11.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnBurger11.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnBurger11.UseVisualStyleBackColor = False
        '
        'btnBurger10
        '
        Me.btnBurger10.BackColor = System.Drawing.Color.White
        Me.btnBurger10.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnBurger10.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnBurger10.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnBurger10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBurger10.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBurger10.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnBurger10.ImageIndex = 1
        Me.btnBurger10.ImageList = Me.ImageList1
        Me.btnBurger10.Location = New System.Drawing.Point(129, 214)
        Me.btnBurger10.Name = "btnBurger10"
        Me.btnBurger10.Size = New System.Drawing.Size(120, 105)
        Me.btnBurger10.TabIndex = 151
        Me.btnBurger10.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnBurger10.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnBurger10.UseVisualStyleBackColor = False
        '
        'btnBurger9
        '
        Me.btnBurger9.BackColor = System.Drawing.Color.White
        Me.btnBurger9.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnBurger9.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnBurger9.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnBurger9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBurger9.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBurger9.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnBurger9.ImageIndex = 1
        Me.btnBurger9.ImageList = Me.ImageList1
        Me.btnBurger9.Location = New System.Drawing.Point(10, 214)
        Me.btnBurger9.Name = "btnBurger9"
        Me.btnBurger9.Size = New System.Drawing.Size(120, 105)
        Me.btnBurger9.TabIndex = 150
        Me.btnBurger9.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnBurger9.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnBurger9.UseVisualStyleBackColor = False
        '
        'btnBurger8
        '
        Me.btnBurger8.BackColor = System.Drawing.Color.White
        Me.btnBurger8.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnBurger8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnBurger8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnBurger8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBurger8.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBurger8.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnBurger8.ImageIndex = 1
        Me.btnBurger8.ImageList = Me.ImageList1
        Me.btnBurger8.Location = New System.Drawing.Point(367, 111)
        Me.btnBurger8.Name = "btnBurger8"
        Me.btnBurger8.Size = New System.Drawing.Size(120, 105)
        Me.btnBurger8.TabIndex = 149
        Me.btnBurger8.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnBurger8.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnBurger8.UseVisualStyleBackColor = False
        '
        'btnBurger7
        '
        Me.btnBurger7.BackColor = System.Drawing.Color.White
        Me.btnBurger7.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnBurger7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnBurger7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnBurger7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBurger7.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBurger7.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnBurger7.ImageIndex = 1
        Me.btnBurger7.ImageList = Me.ImageList1
        Me.btnBurger7.Location = New System.Drawing.Point(248, 111)
        Me.btnBurger7.Name = "btnBurger7"
        Me.btnBurger7.Size = New System.Drawing.Size(120, 105)
        Me.btnBurger7.TabIndex = 148
        Me.btnBurger7.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnBurger7.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnBurger7.UseVisualStyleBackColor = False
        '
        'btnBurger6
        '
        Me.btnBurger6.BackColor = System.Drawing.Color.White
        Me.btnBurger6.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnBurger6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnBurger6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnBurger6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBurger6.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBurger6.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnBurger6.ImageIndex = 1
        Me.btnBurger6.ImageList = Me.ImageList1
        Me.btnBurger6.Location = New System.Drawing.Point(129, 111)
        Me.btnBurger6.Name = "btnBurger6"
        Me.btnBurger6.Size = New System.Drawing.Size(120, 105)
        Me.btnBurger6.TabIndex = 147
        Me.btnBurger6.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnBurger6.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnBurger6.UseVisualStyleBackColor = False
        '
        'btnBurger5
        '
        Me.btnBurger5.BackColor = System.Drawing.Color.White
        Me.btnBurger5.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnBurger5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnBurger5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnBurger5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBurger5.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBurger5.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnBurger5.ImageIndex = 1
        Me.btnBurger5.ImageList = Me.ImageList1
        Me.btnBurger5.Location = New System.Drawing.Point(10, 111)
        Me.btnBurger5.Name = "btnBurger5"
        Me.btnBurger5.Size = New System.Drawing.Size(120, 105)
        Me.btnBurger5.TabIndex = 146
        Me.btnBurger5.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnBurger5.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnBurger5.UseVisualStyleBackColor = False
        '
        'btnBurger4
        '
        Me.btnBurger4.BackColor = System.Drawing.Color.White
        Me.btnBurger4.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnBurger4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnBurger4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnBurger4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBurger4.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBurger4.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnBurger4.ImageIndex = 1
        Me.btnBurger4.ImageList = Me.ImageList1
        Me.btnBurger4.Location = New System.Drawing.Point(367, 7)
        Me.btnBurger4.Name = "btnBurger4"
        Me.btnBurger4.Size = New System.Drawing.Size(120, 105)
        Me.btnBurger4.TabIndex = 145
        Me.btnBurger4.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnBurger4.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnBurger4.UseVisualStyleBackColor = False
        '
        'btnBurger3
        '
        Me.btnBurger3.BackColor = System.Drawing.Color.White
        Me.btnBurger3.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnBurger3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnBurger3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnBurger3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBurger3.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBurger3.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnBurger3.ImageIndex = 1
        Me.btnBurger3.ImageList = Me.ImageList1
        Me.btnBurger3.Location = New System.Drawing.Point(248, 7)
        Me.btnBurger3.Name = "btnBurger3"
        Me.btnBurger3.Size = New System.Drawing.Size(120, 105)
        Me.btnBurger3.TabIndex = 144
        Me.btnBurger3.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnBurger3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnBurger3.UseVisualStyleBackColor = False
        '
        'btnBurger2
        '
        Me.btnBurger2.BackColor = System.Drawing.Color.White
        Me.btnBurger2.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnBurger2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnBurger2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnBurger2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBurger2.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBurger2.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnBurger2.ImageIndex = 1
        Me.btnBurger2.ImageList = Me.ImageList1
        Me.btnBurger2.Location = New System.Drawing.Point(129, 7)
        Me.btnBurger2.Name = "btnBurger2"
        Me.btnBurger2.Size = New System.Drawing.Size(120, 105)
        Me.btnBurger2.TabIndex = 143
        Me.btnBurger2.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnBurger2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnBurger2.UseVisualStyleBackColor = False
        '
        'btnBurger1
        '
        Me.btnBurger1.BackColor = System.Drawing.Color.White
        Me.btnBurger1.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnBurger1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnBurger1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnBurger1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBurger1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBurger1.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnBurger1.ImageIndex = 1
        Me.btnBurger1.ImageList = Me.ImageList1
        Me.btnBurger1.Location = New System.Drawing.Point(10, 7)
        Me.btnBurger1.Name = "btnBurger1"
        Me.btnBurger1.Size = New System.Drawing.Size(120, 105)
        Me.btnBurger1.TabIndex = 142
        Me.btnBurger1.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnBurger1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnBurger1.UseVisualStyleBackColor = False
        '
        'TabItem1
        '
        Me.TabItem1.AttachedControl = Me.TabControlPanel2
        Me.TabItem1.Image = CType(resources.GetObject("TabItem1.Image"), System.Drawing.Image)
        Me.TabItem1.Name = "TabItem1"
        Me.TabItem1.Text = "Burgers"
        '
        'TabControlPanel5
        '
        Me.TabControlPanel5.Controls.Add(Me.dtgRecord)
        Me.TabControlPanel5.Controls.Add(Me.Panel7)
        Me.TabControlPanel5.Controls.Add(Me.Panel6)
        Me.TabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel5.Location = New System.Drawing.Point(0, 40)
        Me.TabControlPanel5.Name = "TabControlPanel5"
        Me.TabControlPanel5.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel5.Size = New System.Drawing.Size(497, 430)
        Me.TabControlPanel5.Style.BackColor1.Color = System.Drawing.Color.White
        Me.TabControlPanel5.Style.BackColor2.Color = System.Drawing.Color.White
        Me.TabControlPanel5.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel5.Style.BorderColor.Color = System.Drawing.Color.Black
        Me.TabControlPanel5.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel5.Style.GradientAngle = 90
        Me.TabControlPanel5.TabIndex = 4
        Me.TabControlPanel5.TabItem = Me.TabItem4
        '
        'dtgRecord
        '
        Me.dtgRecord.AllowUserToAddRows = False
        Me.dtgRecord.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White
        Me.dtgRecord.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dtgRecord.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        Me.dtgRecord.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dtgRecord.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgRecord.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.Column3, Me.DataGridViewTextBoxColumn3, Me.Column1, Me.Column2})
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.White
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dtgRecord.DefaultCellStyle = DataGridViewCellStyle5
        Me.dtgRecord.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dtgRecord.EnableHeadersVisualStyles = False
        Me.dtgRecord.GridColor = System.Drawing.Color.Gray
        Me.dtgRecord.Location = New System.Drawing.Point(1, 36)
        Me.dtgRecord.Name = "dtgRecord"
        Me.dtgRecord.ReadOnly = True
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgRecord.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dtgRecord.RowHeadersVisible = False
        Me.dtgRecord.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgRecord.Size = New System.Drawing.Size(495, 358)
        Me.dtgRecord.TabIndex = 39
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "movie_id"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Receipt No"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 140
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "title"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.DataGridViewTextBoxColumn2.DefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridViewTextBoxColumn2.HeaderText = "Date"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 110
        '
        'Column3
        '
        Me.Column3.HeaderText = "Items Bought"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        Me.Column3.Width = 300
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "rating"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridViewTextBoxColumn3.HeaderText = "Total Amount"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 120
        '
        'Column1
        '
        Me.Column1.HeaderText = "Cash Payment"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Width = 120
        '
        'Column2
        '
        Me.Column2.HeaderText = "Change / Due"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        Me.Column2.Width = 120
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.White
        Me.Panel7.Controls.Add(Me.btnDelete)
        Me.Panel7.Controls.Add(Me.Btnrefresh)
        Me.Panel7.Controls.Add(Me.btnExport)
        Me.Panel7.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel7.Location = New System.Drawing.Point(1, 394)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(495, 35)
        Me.Panel7.TabIndex = 41
        '
        'btnDelete
        '
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnDelete.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnDelete.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.Location = New System.Drawing.Point(210, 5)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(79, 26)
        Me.btnDelete.TabIndex = 16
        Me.btnDelete.Text = "Delete All"
        Me.btnDelete.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnDelete.UseVisualStyleBackColor = False
        '
        'Btnrefresh
        '
        Me.Btnrefresh.BackColor = System.Drawing.Color.White
        Me.Btnrefresh.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.Btnrefresh.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.Btnrefresh.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Btnrefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Btnrefresh.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btnrefresh.Location = New System.Drawing.Point(5, 5)
        Me.Btnrefresh.Name = "Btnrefresh"
        Me.Btnrefresh.Size = New System.Drawing.Size(79, 26)
        Me.Btnrefresh.TabIndex = 15
        Me.Btnrefresh.Text = "Refresh"
        Me.Btnrefresh.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Btnrefresh.UseVisualStyleBackColor = False
        '
        'btnExport
        '
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnExport.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnExport.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.Location = New System.Drawing.Point(90, 5)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.Size = New System.Drawing.Size(115, 26)
        Me.btnExport.TabIndex = 14
        Me.btnExport.Text = "Export to Excel"
        Me.btnExport.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnExport.UseVisualStyleBackColor = False
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.White
        Me.Panel6.Controls.Add(Me.txtSearch)
        Me.Panel6.Controls.Add(Me.txtSearchby)
        Me.Panel6.Controls.Add(Me.lblRecords)
        Me.Panel6.Controls.Add(Me.Label6)
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel6.Location = New System.Drawing.Point(1, 1)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(495, 35)
        Me.Panel6.TabIndex = 40
        '
        'txtSearch
        '
        Me.txtSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSearch.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearch.Location = New System.Drawing.Point(218, 4)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(161, 27)
        Me.txtSearch.TabIndex = 12
        Me.txtSearch.ValidatingType = GetType(Date)
        '
        'txtSearchby
        '
        Me.txtSearchby.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.txtSearchby.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchby.FormattingEnabled = True
        Me.txtSearchby.Items.AddRange(New Object() {"Receipt No", "Date"})
        Me.txtSearchby.Location = New System.Drawing.Point(68, 5)
        Me.txtSearchby.Name = "txtSearchby"
        Me.txtSearchby.Size = New System.Drawing.Size(145, 25)
        Me.txtSearchby.TabIndex = 11
        Me.txtSearchby.Text = "Select here"
        '
        'lblRecords
        '
        Me.lblRecords.AutoSize = True
        Me.lblRecords.BackColor = System.Drawing.Color.Transparent
        Me.lblRecords.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRecords.Location = New System.Drawing.Point(402, 9)
        Me.lblRecords.Name = "lblRecords"
        Me.lblRecords.Size = New System.Drawing.Size(75, 18)
        Me.lblRecords.TabIndex = 10
        Me.lblRecords.Text = "Records: "
        Me.lblRecords.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(6, 9)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(58, 18)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "Search"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TabItem4
        '
        Me.TabItem4.AttachedControl = Me.TabControlPanel5
        Me.TabItem4.Image = CType(resources.GetObject("TabItem4.Image"), System.Drawing.Image)
        Me.TabItem4.Name = "TabItem4"
        Me.TabItem4.Text = "Records"
        '
        'TabControlPanel4
        '
        Me.TabControlPanel4.Controls.Add(Me.btnClearSpecial16)
        Me.TabControlPanel4.Controls.Add(Me.btnClearSpecial15)
        Me.TabControlPanel4.Controls.Add(Me.btnClearSpecial14)
        Me.TabControlPanel4.Controls.Add(Me.btnClearSpecial13)
        Me.TabControlPanel4.Controls.Add(Me.btnClearSpecial12)
        Me.TabControlPanel4.Controls.Add(Me.btnClearSpecial11)
        Me.TabControlPanel4.Controls.Add(Me.btnClearSpecial10)
        Me.TabControlPanel4.Controls.Add(Me.btnClearSpecial9)
        Me.TabControlPanel4.Controls.Add(Me.btnClearSpecial8)
        Me.TabControlPanel4.Controls.Add(Me.btnClearSpecial7)
        Me.TabControlPanel4.Controls.Add(Me.btnClearSpecial6)
        Me.TabControlPanel4.Controls.Add(Me.btnClearSpecial5)
        Me.TabControlPanel4.Controls.Add(Me.btnClearSpecial4)
        Me.TabControlPanel4.Controls.Add(Me.btnClearSpecial3)
        Me.TabControlPanel4.Controls.Add(Me.btnClearSpecial2)
        Me.TabControlPanel4.Controls.Add(Me.btnClearSpecial1)
        Me.TabControlPanel4.Controls.Add(Me.btnEditSpecial16)
        Me.TabControlPanel4.Controls.Add(Me.btnEditSpecial15)
        Me.TabControlPanel4.Controls.Add(Me.btnEditSpecial14)
        Me.TabControlPanel4.Controls.Add(Me.btnEditSpecial13)
        Me.TabControlPanel4.Controls.Add(Me.btnEditSpecial12)
        Me.TabControlPanel4.Controls.Add(Me.btnEditSpecial11)
        Me.TabControlPanel4.Controls.Add(Me.btnEditSpecial10)
        Me.TabControlPanel4.Controls.Add(Me.btnEditSpecial9)
        Me.TabControlPanel4.Controls.Add(Me.btnEditSpecial8)
        Me.TabControlPanel4.Controls.Add(Me.btnEditSpecial7)
        Me.TabControlPanel4.Controls.Add(Me.btnEditSpecial6)
        Me.TabControlPanel4.Controls.Add(Me.btnEditSpecial5)
        Me.TabControlPanel4.Controls.Add(Me.btnEditSpecial4)
        Me.TabControlPanel4.Controls.Add(Me.btnEditSpecial3)
        Me.TabControlPanel4.Controls.Add(Me.btnEditSpecial2)
        Me.TabControlPanel4.Controls.Add(Me.btnEditSpecial1)
        Me.TabControlPanel4.Controls.Add(Me.lblSpecial16)
        Me.TabControlPanel4.Controls.Add(Me.lblSpecial15)
        Me.TabControlPanel4.Controls.Add(Me.lblSpecial14)
        Me.TabControlPanel4.Controls.Add(Me.lblSpecial13)
        Me.TabControlPanel4.Controls.Add(Me.lblSpecial12)
        Me.TabControlPanel4.Controls.Add(Me.lblSpecial11)
        Me.TabControlPanel4.Controls.Add(Me.lblSpecial10)
        Me.TabControlPanel4.Controls.Add(Me.lblSpecial9)
        Me.TabControlPanel4.Controls.Add(Me.lblSpecial8)
        Me.TabControlPanel4.Controls.Add(Me.lblSpecial7)
        Me.TabControlPanel4.Controls.Add(Me.lblSpecial6)
        Me.TabControlPanel4.Controls.Add(Me.lblSpecial5)
        Me.TabControlPanel4.Controls.Add(Me.lblSpecial4)
        Me.TabControlPanel4.Controls.Add(Me.lblSpecial3)
        Me.TabControlPanel4.Controls.Add(Me.lblSpecial2)
        Me.TabControlPanel4.Controls.Add(Me.lblSpecial1)
        Me.TabControlPanel4.Controls.Add(Me.btnSpecial16)
        Me.TabControlPanel4.Controls.Add(Me.btnSpecial15)
        Me.TabControlPanel4.Controls.Add(Me.btnSpecial14)
        Me.TabControlPanel4.Controls.Add(Me.btnSpecial13)
        Me.TabControlPanel4.Controls.Add(Me.btnSpecial12)
        Me.TabControlPanel4.Controls.Add(Me.btnSpecial11)
        Me.TabControlPanel4.Controls.Add(Me.btnSpecial10)
        Me.TabControlPanel4.Controls.Add(Me.btnSpecial9)
        Me.TabControlPanel4.Controls.Add(Me.btnSpecial8)
        Me.TabControlPanel4.Controls.Add(Me.btnSpecial7)
        Me.TabControlPanel4.Controls.Add(Me.btnSpecial6)
        Me.TabControlPanel4.Controls.Add(Me.btnSpecial5)
        Me.TabControlPanel4.Controls.Add(Me.btnSpecial4)
        Me.TabControlPanel4.Controls.Add(Me.btnSpecial3)
        Me.TabControlPanel4.Controls.Add(Me.btnSpecial2)
        Me.TabControlPanel4.Controls.Add(Me.btnSpecial1)
        Me.TabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel4.Location = New System.Drawing.Point(0, 40)
        Me.TabControlPanel4.Name = "TabControlPanel4"
        Me.TabControlPanel4.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel4.Size = New System.Drawing.Size(497, 430)
        Me.TabControlPanel4.Style.BackColor1.Color = System.Drawing.Color.White
        Me.TabControlPanel4.Style.BackColor2.Color = System.Drawing.Color.White
        Me.TabControlPanel4.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel4.Style.BorderColor.Color = System.Drawing.Color.Black
        Me.TabControlPanel4.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel4.Style.GradientAngle = 90
        Me.TabControlPanel4.TabIndex = 3
        Me.TabControlPanel4.TabItem = Me.TabItem3
        '
        'btnClearSpecial16
        '
        Me.btnClearSpecial16.BackColor = System.Drawing.Color.White
        Me.btnClearSpecial16.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearSpecial16.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearSpecial16.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearSpecial16.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearSpecial16.Image = CType(resources.GetObject("btnClearSpecial16.Image"), System.Drawing.Image)
        Me.btnClearSpecial16.Location = New System.Drawing.Point(367, 318)
        Me.btnClearSpecial16.Name = "btnClearSpecial16"
        Me.btnClearSpecial16.Size = New System.Drawing.Size(25, 25)
        Me.btnClearSpecial16.TabIndex = 269
        Me.btnClearSpecial16.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearSpecial16.UseVisualStyleBackColor = False
        '
        'btnClearSpecial15
        '
        Me.btnClearSpecial15.BackColor = System.Drawing.Color.White
        Me.btnClearSpecial15.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearSpecial15.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearSpecial15.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearSpecial15.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearSpecial15.Image = CType(resources.GetObject("btnClearSpecial15.Image"), System.Drawing.Image)
        Me.btnClearSpecial15.Location = New System.Drawing.Point(248, 318)
        Me.btnClearSpecial15.Name = "btnClearSpecial15"
        Me.btnClearSpecial15.Size = New System.Drawing.Size(25, 25)
        Me.btnClearSpecial15.TabIndex = 268
        Me.btnClearSpecial15.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearSpecial15.UseVisualStyleBackColor = False
        '
        'btnClearSpecial14
        '
        Me.btnClearSpecial14.BackColor = System.Drawing.Color.White
        Me.btnClearSpecial14.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearSpecial14.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearSpecial14.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearSpecial14.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearSpecial14.Image = CType(resources.GetObject("btnClearSpecial14.Image"), System.Drawing.Image)
        Me.btnClearSpecial14.Location = New System.Drawing.Point(129, 318)
        Me.btnClearSpecial14.Name = "btnClearSpecial14"
        Me.btnClearSpecial14.Size = New System.Drawing.Size(25, 25)
        Me.btnClearSpecial14.TabIndex = 267
        Me.btnClearSpecial14.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearSpecial14.UseVisualStyleBackColor = False
        '
        'btnClearSpecial13
        '
        Me.btnClearSpecial13.BackColor = System.Drawing.Color.White
        Me.btnClearSpecial13.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearSpecial13.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearSpecial13.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearSpecial13.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearSpecial13.Image = CType(resources.GetObject("btnClearSpecial13.Image"), System.Drawing.Image)
        Me.btnClearSpecial13.Location = New System.Drawing.Point(10, 318)
        Me.btnClearSpecial13.Name = "btnClearSpecial13"
        Me.btnClearSpecial13.Size = New System.Drawing.Size(25, 25)
        Me.btnClearSpecial13.TabIndex = 266
        Me.btnClearSpecial13.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearSpecial13.UseVisualStyleBackColor = False
        '
        'btnClearSpecial12
        '
        Me.btnClearSpecial12.BackColor = System.Drawing.Color.White
        Me.btnClearSpecial12.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearSpecial12.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearSpecial12.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearSpecial12.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearSpecial12.Image = CType(resources.GetObject("btnClearSpecial12.Image"), System.Drawing.Image)
        Me.btnClearSpecial12.Location = New System.Drawing.Point(367, 214)
        Me.btnClearSpecial12.Name = "btnClearSpecial12"
        Me.btnClearSpecial12.Size = New System.Drawing.Size(25, 25)
        Me.btnClearSpecial12.TabIndex = 265
        Me.btnClearSpecial12.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearSpecial12.UseVisualStyleBackColor = False
        '
        'btnClearSpecial11
        '
        Me.btnClearSpecial11.BackColor = System.Drawing.Color.White
        Me.btnClearSpecial11.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearSpecial11.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearSpecial11.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearSpecial11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearSpecial11.Image = CType(resources.GetObject("btnClearSpecial11.Image"), System.Drawing.Image)
        Me.btnClearSpecial11.Location = New System.Drawing.Point(248, 214)
        Me.btnClearSpecial11.Name = "btnClearSpecial11"
        Me.btnClearSpecial11.Size = New System.Drawing.Size(25, 25)
        Me.btnClearSpecial11.TabIndex = 264
        Me.btnClearSpecial11.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearSpecial11.UseVisualStyleBackColor = False
        '
        'btnClearSpecial10
        '
        Me.btnClearSpecial10.BackColor = System.Drawing.Color.White
        Me.btnClearSpecial10.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearSpecial10.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearSpecial10.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearSpecial10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearSpecial10.Image = CType(resources.GetObject("btnClearSpecial10.Image"), System.Drawing.Image)
        Me.btnClearSpecial10.Location = New System.Drawing.Point(129, 214)
        Me.btnClearSpecial10.Name = "btnClearSpecial10"
        Me.btnClearSpecial10.Size = New System.Drawing.Size(25, 25)
        Me.btnClearSpecial10.TabIndex = 263
        Me.btnClearSpecial10.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearSpecial10.UseVisualStyleBackColor = False
        '
        'btnClearSpecial9
        '
        Me.btnClearSpecial9.BackColor = System.Drawing.Color.White
        Me.btnClearSpecial9.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearSpecial9.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearSpecial9.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearSpecial9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearSpecial9.Image = CType(resources.GetObject("btnClearSpecial9.Image"), System.Drawing.Image)
        Me.btnClearSpecial9.Location = New System.Drawing.Point(10, 214)
        Me.btnClearSpecial9.Name = "btnClearSpecial9"
        Me.btnClearSpecial9.Size = New System.Drawing.Size(25, 25)
        Me.btnClearSpecial9.TabIndex = 262
        Me.btnClearSpecial9.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearSpecial9.UseVisualStyleBackColor = False
        '
        'btnClearSpecial8
        '
        Me.btnClearSpecial8.BackColor = System.Drawing.Color.White
        Me.btnClearSpecial8.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearSpecial8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearSpecial8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearSpecial8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearSpecial8.Image = CType(resources.GetObject("btnClearSpecial8.Image"), System.Drawing.Image)
        Me.btnClearSpecial8.Location = New System.Drawing.Point(367, 111)
        Me.btnClearSpecial8.Name = "btnClearSpecial8"
        Me.btnClearSpecial8.Size = New System.Drawing.Size(25, 25)
        Me.btnClearSpecial8.TabIndex = 261
        Me.btnClearSpecial8.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearSpecial8.UseVisualStyleBackColor = False
        '
        'btnClearSpecial7
        '
        Me.btnClearSpecial7.BackColor = System.Drawing.Color.White
        Me.btnClearSpecial7.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearSpecial7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearSpecial7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearSpecial7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearSpecial7.Image = CType(resources.GetObject("btnClearSpecial7.Image"), System.Drawing.Image)
        Me.btnClearSpecial7.Location = New System.Drawing.Point(248, 111)
        Me.btnClearSpecial7.Name = "btnClearSpecial7"
        Me.btnClearSpecial7.Size = New System.Drawing.Size(25, 25)
        Me.btnClearSpecial7.TabIndex = 260
        Me.btnClearSpecial7.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearSpecial7.UseVisualStyleBackColor = False
        '
        'btnClearSpecial6
        '
        Me.btnClearSpecial6.BackColor = System.Drawing.Color.White
        Me.btnClearSpecial6.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearSpecial6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearSpecial6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearSpecial6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearSpecial6.Image = CType(resources.GetObject("btnClearSpecial6.Image"), System.Drawing.Image)
        Me.btnClearSpecial6.Location = New System.Drawing.Point(129, 111)
        Me.btnClearSpecial6.Name = "btnClearSpecial6"
        Me.btnClearSpecial6.Size = New System.Drawing.Size(25, 25)
        Me.btnClearSpecial6.TabIndex = 259
        Me.btnClearSpecial6.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearSpecial6.UseVisualStyleBackColor = False
        '
        'btnClearSpecial5
        '
        Me.btnClearSpecial5.BackColor = System.Drawing.Color.White
        Me.btnClearSpecial5.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearSpecial5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearSpecial5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearSpecial5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearSpecial5.Image = CType(resources.GetObject("btnClearSpecial5.Image"), System.Drawing.Image)
        Me.btnClearSpecial5.Location = New System.Drawing.Point(10, 111)
        Me.btnClearSpecial5.Name = "btnClearSpecial5"
        Me.btnClearSpecial5.Size = New System.Drawing.Size(25, 25)
        Me.btnClearSpecial5.TabIndex = 258
        Me.btnClearSpecial5.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearSpecial5.UseVisualStyleBackColor = False
        '
        'btnClearSpecial4
        '
        Me.btnClearSpecial4.BackColor = System.Drawing.Color.White
        Me.btnClearSpecial4.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearSpecial4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearSpecial4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearSpecial4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearSpecial4.Image = CType(resources.GetObject("btnClearSpecial4.Image"), System.Drawing.Image)
        Me.btnClearSpecial4.Location = New System.Drawing.Point(367, 7)
        Me.btnClearSpecial4.Name = "btnClearSpecial4"
        Me.btnClearSpecial4.Size = New System.Drawing.Size(25, 25)
        Me.btnClearSpecial4.TabIndex = 257
        Me.btnClearSpecial4.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearSpecial4.UseVisualStyleBackColor = False
        '
        'btnClearSpecial3
        '
        Me.btnClearSpecial3.BackColor = System.Drawing.Color.White
        Me.btnClearSpecial3.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearSpecial3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearSpecial3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearSpecial3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearSpecial3.Image = CType(resources.GetObject("btnClearSpecial3.Image"), System.Drawing.Image)
        Me.btnClearSpecial3.Location = New System.Drawing.Point(248, 7)
        Me.btnClearSpecial3.Name = "btnClearSpecial3"
        Me.btnClearSpecial3.Size = New System.Drawing.Size(25, 25)
        Me.btnClearSpecial3.TabIndex = 256
        Me.btnClearSpecial3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearSpecial3.UseVisualStyleBackColor = False
        '
        'btnClearSpecial2
        '
        Me.btnClearSpecial2.BackColor = System.Drawing.Color.White
        Me.btnClearSpecial2.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearSpecial2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearSpecial2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearSpecial2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearSpecial2.Image = CType(resources.GetObject("btnClearSpecial2.Image"), System.Drawing.Image)
        Me.btnClearSpecial2.Location = New System.Drawing.Point(129, 7)
        Me.btnClearSpecial2.Name = "btnClearSpecial2"
        Me.btnClearSpecial2.Size = New System.Drawing.Size(25, 25)
        Me.btnClearSpecial2.TabIndex = 255
        Me.btnClearSpecial2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearSpecial2.UseVisualStyleBackColor = False
        '
        'btnClearSpecial1
        '
        Me.btnClearSpecial1.BackColor = System.Drawing.Color.White
        Me.btnClearSpecial1.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnClearSpecial1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnClearSpecial1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnClearSpecial1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearSpecial1.Image = CType(resources.GetObject("btnClearSpecial1.Image"), System.Drawing.Image)
        Me.btnClearSpecial1.Location = New System.Drawing.Point(10, 7)
        Me.btnClearSpecial1.Name = "btnClearSpecial1"
        Me.btnClearSpecial1.Size = New System.Drawing.Size(25, 25)
        Me.btnClearSpecial1.TabIndex = 254
        Me.btnClearSpecial1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnClearSpecial1.UseVisualStyleBackColor = False
        '
        'btnEditSpecial16
        '
        Me.btnEditSpecial16.BackColor = System.Drawing.Color.White
        Me.btnEditSpecial16.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditSpecial16.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditSpecial16.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditSpecial16.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditSpecial16.Image = CType(resources.GetObject("btnEditSpecial16.Image"), System.Drawing.Image)
        Me.btnEditSpecial16.Location = New System.Drawing.Point(462, 318)
        Me.btnEditSpecial16.Name = "btnEditSpecial16"
        Me.btnEditSpecial16.Size = New System.Drawing.Size(25, 25)
        Me.btnEditSpecial16.TabIndex = 253
        Me.btnEditSpecial16.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditSpecial16.UseVisualStyleBackColor = False
        '
        'btnEditSpecial15
        '
        Me.btnEditSpecial15.BackColor = System.Drawing.Color.White
        Me.btnEditSpecial15.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditSpecial15.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditSpecial15.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditSpecial15.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditSpecial15.Image = CType(resources.GetObject("btnEditSpecial15.Image"), System.Drawing.Image)
        Me.btnEditSpecial15.Location = New System.Drawing.Point(343, 318)
        Me.btnEditSpecial15.Name = "btnEditSpecial15"
        Me.btnEditSpecial15.Size = New System.Drawing.Size(25, 25)
        Me.btnEditSpecial15.TabIndex = 252
        Me.btnEditSpecial15.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditSpecial15.UseVisualStyleBackColor = False
        '
        'btnEditSpecial14
        '
        Me.btnEditSpecial14.BackColor = System.Drawing.Color.White
        Me.btnEditSpecial14.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditSpecial14.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditSpecial14.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditSpecial14.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditSpecial14.Image = CType(resources.GetObject("btnEditSpecial14.Image"), System.Drawing.Image)
        Me.btnEditSpecial14.Location = New System.Drawing.Point(224, 318)
        Me.btnEditSpecial14.Name = "btnEditSpecial14"
        Me.btnEditSpecial14.Size = New System.Drawing.Size(25, 25)
        Me.btnEditSpecial14.TabIndex = 251
        Me.btnEditSpecial14.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditSpecial14.UseVisualStyleBackColor = False
        '
        'btnEditSpecial13
        '
        Me.btnEditSpecial13.BackColor = System.Drawing.Color.White
        Me.btnEditSpecial13.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditSpecial13.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditSpecial13.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditSpecial13.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditSpecial13.Image = CType(resources.GetObject("btnEditSpecial13.Image"), System.Drawing.Image)
        Me.btnEditSpecial13.Location = New System.Drawing.Point(105, 318)
        Me.btnEditSpecial13.Name = "btnEditSpecial13"
        Me.btnEditSpecial13.Size = New System.Drawing.Size(25, 25)
        Me.btnEditSpecial13.TabIndex = 250
        Me.btnEditSpecial13.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditSpecial13.UseVisualStyleBackColor = False
        '
        'btnEditSpecial12
        '
        Me.btnEditSpecial12.BackColor = System.Drawing.Color.White
        Me.btnEditSpecial12.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditSpecial12.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditSpecial12.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditSpecial12.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditSpecial12.Image = CType(resources.GetObject("btnEditSpecial12.Image"), System.Drawing.Image)
        Me.btnEditSpecial12.Location = New System.Drawing.Point(462, 214)
        Me.btnEditSpecial12.Name = "btnEditSpecial12"
        Me.btnEditSpecial12.Size = New System.Drawing.Size(25, 25)
        Me.btnEditSpecial12.TabIndex = 249
        Me.btnEditSpecial12.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditSpecial12.UseVisualStyleBackColor = False
        '
        'btnEditSpecial11
        '
        Me.btnEditSpecial11.BackColor = System.Drawing.Color.White
        Me.btnEditSpecial11.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditSpecial11.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditSpecial11.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditSpecial11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditSpecial11.Image = CType(resources.GetObject("btnEditSpecial11.Image"), System.Drawing.Image)
        Me.btnEditSpecial11.Location = New System.Drawing.Point(343, 214)
        Me.btnEditSpecial11.Name = "btnEditSpecial11"
        Me.btnEditSpecial11.Size = New System.Drawing.Size(25, 25)
        Me.btnEditSpecial11.TabIndex = 248
        Me.btnEditSpecial11.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditSpecial11.UseVisualStyleBackColor = False
        '
        'btnEditSpecial10
        '
        Me.btnEditSpecial10.BackColor = System.Drawing.Color.White
        Me.btnEditSpecial10.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditSpecial10.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditSpecial10.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditSpecial10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditSpecial10.Image = CType(resources.GetObject("btnEditSpecial10.Image"), System.Drawing.Image)
        Me.btnEditSpecial10.Location = New System.Drawing.Point(224, 214)
        Me.btnEditSpecial10.Name = "btnEditSpecial10"
        Me.btnEditSpecial10.Size = New System.Drawing.Size(25, 25)
        Me.btnEditSpecial10.TabIndex = 247
        Me.btnEditSpecial10.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditSpecial10.UseVisualStyleBackColor = False
        '
        'btnEditSpecial9
        '
        Me.btnEditSpecial9.BackColor = System.Drawing.Color.White
        Me.btnEditSpecial9.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditSpecial9.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditSpecial9.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditSpecial9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditSpecial9.Image = CType(resources.GetObject("btnEditSpecial9.Image"), System.Drawing.Image)
        Me.btnEditSpecial9.Location = New System.Drawing.Point(105, 214)
        Me.btnEditSpecial9.Name = "btnEditSpecial9"
        Me.btnEditSpecial9.Size = New System.Drawing.Size(25, 25)
        Me.btnEditSpecial9.TabIndex = 246
        Me.btnEditSpecial9.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditSpecial9.UseVisualStyleBackColor = False
        '
        'btnEditSpecial8
        '
        Me.btnEditSpecial8.BackColor = System.Drawing.Color.White
        Me.btnEditSpecial8.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditSpecial8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditSpecial8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditSpecial8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditSpecial8.Image = CType(resources.GetObject("btnEditSpecial8.Image"), System.Drawing.Image)
        Me.btnEditSpecial8.Location = New System.Drawing.Point(462, 111)
        Me.btnEditSpecial8.Name = "btnEditSpecial8"
        Me.btnEditSpecial8.Size = New System.Drawing.Size(25, 25)
        Me.btnEditSpecial8.TabIndex = 245
        Me.btnEditSpecial8.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditSpecial8.UseVisualStyleBackColor = False
        '
        'btnEditSpecial7
        '
        Me.btnEditSpecial7.BackColor = System.Drawing.Color.White
        Me.btnEditSpecial7.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditSpecial7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditSpecial7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditSpecial7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditSpecial7.Image = CType(resources.GetObject("btnEditSpecial7.Image"), System.Drawing.Image)
        Me.btnEditSpecial7.Location = New System.Drawing.Point(343, 111)
        Me.btnEditSpecial7.Name = "btnEditSpecial7"
        Me.btnEditSpecial7.Size = New System.Drawing.Size(25, 25)
        Me.btnEditSpecial7.TabIndex = 244
        Me.btnEditSpecial7.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditSpecial7.UseVisualStyleBackColor = False
        '
        'btnEditSpecial6
        '
        Me.btnEditSpecial6.BackColor = System.Drawing.Color.White
        Me.btnEditSpecial6.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditSpecial6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditSpecial6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditSpecial6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditSpecial6.Image = CType(resources.GetObject("btnEditSpecial6.Image"), System.Drawing.Image)
        Me.btnEditSpecial6.Location = New System.Drawing.Point(224, 111)
        Me.btnEditSpecial6.Name = "btnEditSpecial6"
        Me.btnEditSpecial6.Size = New System.Drawing.Size(25, 25)
        Me.btnEditSpecial6.TabIndex = 243
        Me.btnEditSpecial6.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditSpecial6.UseVisualStyleBackColor = False
        '
        'btnEditSpecial5
        '
        Me.btnEditSpecial5.BackColor = System.Drawing.Color.White
        Me.btnEditSpecial5.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditSpecial5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditSpecial5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditSpecial5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditSpecial5.Image = CType(resources.GetObject("btnEditSpecial5.Image"), System.Drawing.Image)
        Me.btnEditSpecial5.Location = New System.Drawing.Point(105, 111)
        Me.btnEditSpecial5.Name = "btnEditSpecial5"
        Me.btnEditSpecial5.Size = New System.Drawing.Size(25, 25)
        Me.btnEditSpecial5.TabIndex = 242
        Me.btnEditSpecial5.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditSpecial5.UseVisualStyleBackColor = False
        '
        'btnEditSpecial4
        '
        Me.btnEditSpecial4.BackColor = System.Drawing.Color.White
        Me.btnEditSpecial4.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditSpecial4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditSpecial4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditSpecial4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditSpecial4.Image = CType(resources.GetObject("btnEditSpecial4.Image"), System.Drawing.Image)
        Me.btnEditSpecial4.Location = New System.Drawing.Point(462, 7)
        Me.btnEditSpecial4.Name = "btnEditSpecial4"
        Me.btnEditSpecial4.Size = New System.Drawing.Size(25, 25)
        Me.btnEditSpecial4.TabIndex = 241
        Me.btnEditSpecial4.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditSpecial4.UseVisualStyleBackColor = False
        '
        'btnEditSpecial3
        '
        Me.btnEditSpecial3.BackColor = System.Drawing.Color.White
        Me.btnEditSpecial3.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditSpecial3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditSpecial3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditSpecial3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditSpecial3.Image = CType(resources.GetObject("btnEditSpecial3.Image"), System.Drawing.Image)
        Me.btnEditSpecial3.Location = New System.Drawing.Point(343, 7)
        Me.btnEditSpecial3.Name = "btnEditSpecial3"
        Me.btnEditSpecial3.Size = New System.Drawing.Size(25, 25)
        Me.btnEditSpecial3.TabIndex = 240
        Me.btnEditSpecial3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditSpecial3.UseVisualStyleBackColor = False
        '
        'btnEditSpecial2
        '
        Me.btnEditSpecial2.BackColor = System.Drawing.Color.White
        Me.btnEditSpecial2.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditSpecial2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditSpecial2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditSpecial2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditSpecial2.Image = CType(resources.GetObject("btnEditSpecial2.Image"), System.Drawing.Image)
        Me.btnEditSpecial2.Location = New System.Drawing.Point(224, 7)
        Me.btnEditSpecial2.Name = "btnEditSpecial2"
        Me.btnEditSpecial2.Size = New System.Drawing.Size(25, 25)
        Me.btnEditSpecial2.TabIndex = 239
        Me.btnEditSpecial2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditSpecial2.UseVisualStyleBackColor = False
        '
        'btnEditSpecial1
        '
        Me.btnEditSpecial1.BackColor = System.Drawing.Color.White
        Me.btnEditSpecial1.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnEditSpecial1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnEditSpecial1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditSpecial1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditSpecial1.Image = CType(resources.GetObject("btnEditSpecial1.Image"), System.Drawing.Image)
        Me.btnEditSpecial1.Location = New System.Drawing.Point(105, 7)
        Me.btnEditSpecial1.Name = "btnEditSpecial1"
        Me.btnEditSpecial1.Size = New System.Drawing.Size(25, 25)
        Me.btnEditSpecial1.TabIndex = 238
        Me.btnEditSpecial1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditSpecial1.UseVisualStyleBackColor = False
        '
        'lblSpecial16
        '
        Me.lblSpecial16.AutoSize = True
        Me.lblSpecial16.BackColor = System.Drawing.Color.Transparent
        Me.lblSpecial16.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSpecial16.Location = New System.Drawing.Point(408, 401)
        Me.lblSpecial16.Name = "lblSpecial16"
        Me.lblSpecial16.Size = New System.Drawing.Size(39, 17)
        Me.lblSpecial16.TabIndex = 237
        Me.lblSpecial16.Text = "00.00"
        Me.lblSpecial16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblSpecial15
        '
        Me.lblSpecial15.AutoSize = True
        Me.lblSpecial15.BackColor = System.Drawing.Color.Transparent
        Me.lblSpecial15.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSpecial15.Location = New System.Drawing.Point(291, 401)
        Me.lblSpecial15.Name = "lblSpecial15"
        Me.lblSpecial15.Size = New System.Drawing.Size(39, 17)
        Me.lblSpecial15.TabIndex = 236
        Me.lblSpecial15.Text = "00.00"
        Me.lblSpecial15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblSpecial14
        '
        Me.lblSpecial14.AutoSize = True
        Me.lblSpecial14.BackColor = System.Drawing.Color.Transparent
        Me.lblSpecial14.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSpecial14.Location = New System.Drawing.Point(170, 401)
        Me.lblSpecial14.Name = "lblSpecial14"
        Me.lblSpecial14.Size = New System.Drawing.Size(39, 17)
        Me.lblSpecial14.TabIndex = 235
        Me.lblSpecial14.Text = "00.00"
        Me.lblSpecial14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblSpecial13
        '
        Me.lblSpecial13.AutoSize = True
        Me.lblSpecial13.BackColor = System.Drawing.Color.Transparent
        Me.lblSpecial13.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSpecial13.Location = New System.Drawing.Point(50, 401)
        Me.lblSpecial13.Name = "lblSpecial13"
        Me.lblSpecial13.Size = New System.Drawing.Size(39, 17)
        Me.lblSpecial13.TabIndex = 234
        Me.lblSpecial13.Text = "00.00"
        Me.lblSpecial13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblSpecial12
        '
        Me.lblSpecial12.AutoSize = True
        Me.lblSpecial12.BackColor = System.Drawing.Color.Transparent
        Me.lblSpecial12.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSpecial12.Location = New System.Drawing.Point(408, 297)
        Me.lblSpecial12.Name = "lblSpecial12"
        Me.lblSpecial12.Size = New System.Drawing.Size(39, 17)
        Me.lblSpecial12.TabIndex = 233
        Me.lblSpecial12.Text = "00.00"
        Me.lblSpecial12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblSpecial11
        '
        Me.lblSpecial11.AutoSize = True
        Me.lblSpecial11.BackColor = System.Drawing.Color.Transparent
        Me.lblSpecial11.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSpecial11.Location = New System.Drawing.Point(291, 297)
        Me.lblSpecial11.Name = "lblSpecial11"
        Me.lblSpecial11.Size = New System.Drawing.Size(39, 17)
        Me.lblSpecial11.TabIndex = 232
        Me.lblSpecial11.Text = "00.00"
        Me.lblSpecial11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblSpecial10
        '
        Me.lblSpecial10.AutoSize = True
        Me.lblSpecial10.BackColor = System.Drawing.Color.Transparent
        Me.lblSpecial10.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSpecial10.Location = New System.Drawing.Point(170, 297)
        Me.lblSpecial10.Name = "lblSpecial10"
        Me.lblSpecial10.Size = New System.Drawing.Size(39, 17)
        Me.lblSpecial10.TabIndex = 231
        Me.lblSpecial10.Text = "00.00"
        Me.lblSpecial10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblSpecial9
        '
        Me.lblSpecial9.AutoSize = True
        Me.lblSpecial9.BackColor = System.Drawing.Color.Transparent
        Me.lblSpecial9.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSpecial9.Location = New System.Drawing.Point(50, 297)
        Me.lblSpecial9.Name = "lblSpecial9"
        Me.lblSpecial9.Size = New System.Drawing.Size(39, 17)
        Me.lblSpecial9.TabIndex = 230
        Me.lblSpecial9.Text = "00.00"
        Me.lblSpecial9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblSpecial8
        '
        Me.lblSpecial8.AutoSize = True
        Me.lblSpecial8.BackColor = System.Drawing.Color.Transparent
        Me.lblSpecial8.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSpecial8.Location = New System.Drawing.Point(408, 194)
        Me.lblSpecial8.Name = "lblSpecial8"
        Me.lblSpecial8.Size = New System.Drawing.Size(39, 17)
        Me.lblSpecial8.TabIndex = 229
        Me.lblSpecial8.Text = "00.00"
        Me.lblSpecial8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblSpecial7
        '
        Me.lblSpecial7.AutoSize = True
        Me.lblSpecial7.BackColor = System.Drawing.Color.Transparent
        Me.lblSpecial7.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSpecial7.Location = New System.Drawing.Point(291, 194)
        Me.lblSpecial7.Name = "lblSpecial7"
        Me.lblSpecial7.Size = New System.Drawing.Size(39, 17)
        Me.lblSpecial7.TabIndex = 228
        Me.lblSpecial7.Text = "00.00"
        Me.lblSpecial7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblSpecial6
        '
        Me.lblSpecial6.AutoSize = True
        Me.lblSpecial6.BackColor = System.Drawing.Color.Transparent
        Me.lblSpecial6.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSpecial6.Location = New System.Drawing.Point(170, 194)
        Me.lblSpecial6.Name = "lblSpecial6"
        Me.lblSpecial6.Size = New System.Drawing.Size(39, 17)
        Me.lblSpecial6.TabIndex = 227
        Me.lblSpecial6.Text = "00.00"
        Me.lblSpecial6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblSpecial5
        '
        Me.lblSpecial5.AutoSize = True
        Me.lblSpecial5.BackColor = System.Drawing.Color.Transparent
        Me.lblSpecial5.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSpecial5.Location = New System.Drawing.Point(50, 194)
        Me.lblSpecial5.Name = "lblSpecial5"
        Me.lblSpecial5.Size = New System.Drawing.Size(39, 17)
        Me.lblSpecial5.TabIndex = 226
        Me.lblSpecial5.Text = "00.00"
        Me.lblSpecial5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblSpecial4
        '
        Me.lblSpecial4.AutoSize = True
        Me.lblSpecial4.BackColor = System.Drawing.Color.Transparent
        Me.lblSpecial4.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSpecial4.Location = New System.Drawing.Point(408, 90)
        Me.lblSpecial4.Name = "lblSpecial4"
        Me.lblSpecial4.Size = New System.Drawing.Size(39, 17)
        Me.lblSpecial4.TabIndex = 225
        Me.lblSpecial4.Text = "00.00"
        Me.lblSpecial4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblSpecial3
        '
        Me.lblSpecial3.AutoSize = True
        Me.lblSpecial3.BackColor = System.Drawing.Color.Transparent
        Me.lblSpecial3.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSpecial3.Location = New System.Drawing.Point(291, 90)
        Me.lblSpecial3.Name = "lblSpecial3"
        Me.lblSpecial3.Size = New System.Drawing.Size(39, 17)
        Me.lblSpecial3.TabIndex = 224
        Me.lblSpecial3.Text = "00.00"
        Me.lblSpecial3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblSpecial2
        '
        Me.lblSpecial2.AutoSize = True
        Me.lblSpecial2.BackColor = System.Drawing.Color.Transparent
        Me.lblSpecial2.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSpecial2.Location = New System.Drawing.Point(170, 90)
        Me.lblSpecial2.Name = "lblSpecial2"
        Me.lblSpecial2.Size = New System.Drawing.Size(39, 17)
        Me.lblSpecial2.TabIndex = 223
        Me.lblSpecial2.Text = "00.00"
        Me.lblSpecial2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblSpecial1
        '
        Me.lblSpecial1.AutoSize = True
        Me.lblSpecial1.BackColor = System.Drawing.Color.Transparent
        Me.lblSpecial1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSpecial1.Location = New System.Drawing.Point(50, 90)
        Me.lblSpecial1.Name = "lblSpecial1"
        Me.lblSpecial1.Size = New System.Drawing.Size(39, 17)
        Me.lblSpecial1.TabIndex = 222
        Me.lblSpecial1.Text = "00.00"
        Me.lblSpecial1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnSpecial16
        '
        Me.btnSpecial16.BackColor = System.Drawing.Color.White
        Me.btnSpecial16.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnSpecial16.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnSpecial16.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnSpecial16.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSpecial16.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSpecial16.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnSpecial16.ImageIndex = 1
        Me.btnSpecial16.ImageList = Me.ImageList1
        Me.btnSpecial16.Location = New System.Drawing.Point(367, 318)
        Me.btnSpecial16.Name = "btnSpecial16"
        Me.btnSpecial16.Size = New System.Drawing.Size(120, 105)
        Me.btnSpecial16.TabIndex = 221
        Me.btnSpecial16.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnSpecial16.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnSpecial16.UseVisualStyleBackColor = False
        '
        'btnSpecial15
        '
        Me.btnSpecial15.BackColor = System.Drawing.Color.White
        Me.btnSpecial15.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnSpecial15.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnSpecial15.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnSpecial15.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSpecial15.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSpecial15.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnSpecial15.ImageIndex = 1
        Me.btnSpecial15.ImageList = Me.ImageList1
        Me.btnSpecial15.Location = New System.Drawing.Point(248, 318)
        Me.btnSpecial15.Name = "btnSpecial15"
        Me.btnSpecial15.Size = New System.Drawing.Size(120, 105)
        Me.btnSpecial15.TabIndex = 220
        Me.btnSpecial15.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnSpecial15.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnSpecial15.UseVisualStyleBackColor = False
        '
        'btnSpecial14
        '
        Me.btnSpecial14.BackColor = System.Drawing.Color.White
        Me.btnSpecial14.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnSpecial14.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnSpecial14.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnSpecial14.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSpecial14.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSpecial14.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnSpecial14.ImageIndex = 1
        Me.btnSpecial14.ImageList = Me.ImageList1
        Me.btnSpecial14.Location = New System.Drawing.Point(129, 318)
        Me.btnSpecial14.Name = "btnSpecial14"
        Me.btnSpecial14.Size = New System.Drawing.Size(120, 105)
        Me.btnSpecial14.TabIndex = 219
        Me.btnSpecial14.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnSpecial14.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnSpecial14.UseVisualStyleBackColor = False
        '
        'btnSpecial13
        '
        Me.btnSpecial13.BackColor = System.Drawing.Color.White
        Me.btnSpecial13.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnSpecial13.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnSpecial13.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnSpecial13.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSpecial13.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSpecial13.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnSpecial13.ImageIndex = 1
        Me.btnSpecial13.ImageList = Me.ImageList1
        Me.btnSpecial13.Location = New System.Drawing.Point(10, 318)
        Me.btnSpecial13.Name = "btnSpecial13"
        Me.btnSpecial13.Size = New System.Drawing.Size(120, 105)
        Me.btnSpecial13.TabIndex = 218
        Me.btnSpecial13.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnSpecial13.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnSpecial13.UseVisualStyleBackColor = False
        '
        'btnSpecial12
        '
        Me.btnSpecial12.BackColor = System.Drawing.Color.White
        Me.btnSpecial12.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnSpecial12.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnSpecial12.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnSpecial12.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSpecial12.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSpecial12.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnSpecial12.ImageIndex = 1
        Me.btnSpecial12.ImageList = Me.ImageList1
        Me.btnSpecial12.Location = New System.Drawing.Point(367, 214)
        Me.btnSpecial12.Name = "btnSpecial12"
        Me.btnSpecial12.Size = New System.Drawing.Size(120, 105)
        Me.btnSpecial12.TabIndex = 217
        Me.btnSpecial12.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnSpecial12.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnSpecial12.UseVisualStyleBackColor = False
        '
        'btnSpecial11
        '
        Me.btnSpecial11.BackColor = System.Drawing.Color.White
        Me.btnSpecial11.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnSpecial11.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnSpecial11.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnSpecial11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSpecial11.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSpecial11.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnSpecial11.ImageIndex = 1
        Me.btnSpecial11.ImageList = Me.ImageList1
        Me.btnSpecial11.Location = New System.Drawing.Point(248, 214)
        Me.btnSpecial11.Name = "btnSpecial11"
        Me.btnSpecial11.Size = New System.Drawing.Size(120, 105)
        Me.btnSpecial11.TabIndex = 216
        Me.btnSpecial11.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnSpecial11.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnSpecial11.UseVisualStyleBackColor = False
        '
        'btnSpecial10
        '
        Me.btnSpecial10.BackColor = System.Drawing.Color.White
        Me.btnSpecial10.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnSpecial10.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnSpecial10.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnSpecial10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSpecial10.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSpecial10.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnSpecial10.ImageIndex = 1
        Me.btnSpecial10.ImageList = Me.ImageList1
        Me.btnSpecial10.Location = New System.Drawing.Point(129, 214)
        Me.btnSpecial10.Name = "btnSpecial10"
        Me.btnSpecial10.Size = New System.Drawing.Size(120, 105)
        Me.btnSpecial10.TabIndex = 215
        Me.btnSpecial10.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnSpecial10.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnSpecial10.UseVisualStyleBackColor = False
        '
        'btnSpecial9
        '
        Me.btnSpecial9.BackColor = System.Drawing.Color.White
        Me.btnSpecial9.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnSpecial9.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnSpecial9.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnSpecial9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSpecial9.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSpecial9.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnSpecial9.ImageIndex = 1
        Me.btnSpecial9.ImageList = Me.ImageList1
        Me.btnSpecial9.Location = New System.Drawing.Point(10, 214)
        Me.btnSpecial9.Name = "btnSpecial9"
        Me.btnSpecial9.Size = New System.Drawing.Size(120, 105)
        Me.btnSpecial9.TabIndex = 214
        Me.btnSpecial9.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnSpecial9.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnSpecial9.UseVisualStyleBackColor = False
        '
        'btnSpecial8
        '
        Me.btnSpecial8.BackColor = System.Drawing.Color.White
        Me.btnSpecial8.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnSpecial8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnSpecial8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnSpecial8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSpecial8.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSpecial8.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnSpecial8.ImageIndex = 1
        Me.btnSpecial8.ImageList = Me.ImageList1
        Me.btnSpecial8.Location = New System.Drawing.Point(367, 111)
        Me.btnSpecial8.Name = "btnSpecial8"
        Me.btnSpecial8.Size = New System.Drawing.Size(120, 105)
        Me.btnSpecial8.TabIndex = 213
        Me.btnSpecial8.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnSpecial8.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnSpecial8.UseVisualStyleBackColor = False
        '
        'btnSpecial7
        '
        Me.btnSpecial7.BackColor = System.Drawing.Color.White
        Me.btnSpecial7.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnSpecial7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnSpecial7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnSpecial7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSpecial7.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSpecial7.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnSpecial7.ImageIndex = 1
        Me.btnSpecial7.ImageList = Me.ImageList1
        Me.btnSpecial7.Location = New System.Drawing.Point(248, 111)
        Me.btnSpecial7.Name = "btnSpecial7"
        Me.btnSpecial7.Size = New System.Drawing.Size(120, 105)
        Me.btnSpecial7.TabIndex = 212
        Me.btnSpecial7.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnSpecial7.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnSpecial7.UseVisualStyleBackColor = False
        '
        'btnSpecial6
        '
        Me.btnSpecial6.BackColor = System.Drawing.Color.White
        Me.btnSpecial6.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnSpecial6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnSpecial6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnSpecial6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSpecial6.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSpecial6.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnSpecial6.ImageIndex = 1
        Me.btnSpecial6.ImageList = Me.ImageList1
        Me.btnSpecial6.Location = New System.Drawing.Point(129, 111)
        Me.btnSpecial6.Name = "btnSpecial6"
        Me.btnSpecial6.Size = New System.Drawing.Size(120, 105)
        Me.btnSpecial6.TabIndex = 211
        Me.btnSpecial6.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnSpecial6.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnSpecial6.UseVisualStyleBackColor = False
        '
        'btnSpecial5
        '
        Me.btnSpecial5.BackColor = System.Drawing.Color.White
        Me.btnSpecial5.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnSpecial5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnSpecial5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnSpecial5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSpecial5.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSpecial5.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnSpecial5.ImageIndex = 1
        Me.btnSpecial5.ImageList = Me.ImageList1
        Me.btnSpecial5.Location = New System.Drawing.Point(10, 111)
        Me.btnSpecial5.Name = "btnSpecial5"
        Me.btnSpecial5.Size = New System.Drawing.Size(120, 105)
        Me.btnSpecial5.TabIndex = 210
        Me.btnSpecial5.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnSpecial5.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnSpecial5.UseVisualStyleBackColor = False
        '
        'btnSpecial4
        '
        Me.btnSpecial4.BackColor = System.Drawing.Color.White
        Me.btnSpecial4.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnSpecial4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnSpecial4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnSpecial4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSpecial4.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSpecial4.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnSpecial4.ImageIndex = 1
        Me.btnSpecial4.ImageList = Me.ImageList1
        Me.btnSpecial4.Location = New System.Drawing.Point(367, 7)
        Me.btnSpecial4.Name = "btnSpecial4"
        Me.btnSpecial4.Size = New System.Drawing.Size(120, 105)
        Me.btnSpecial4.TabIndex = 209
        Me.btnSpecial4.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnSpecial4.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnSpecial4.UseVisualStyleBackColor = False
        '
        'btnSpecial3
        '
        Me.btnSpecial3.BackColor = System.Drawing.Color.White
        Me.btnSpecial3.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnSpecial3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnSpecial3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnSpecial3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSpecial3.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSpecial3.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnSpecial3.ImageIndex = 1
        Me.btnSpecial3.ImageList = Me.ImageList1
        Me.btnSpecial3.Location = New System.Drawing.Point(248, 7)
        Me.btnSpecial3.Name = "btnSpecial3"
        Me.btnSpecial3.Size = New System.Drawing.Size(120, 105)
        Me.btnSpecial3.TabIndex = 208
        Me.btnSpecial3.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnSpecial3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnSpecial3.UseVisualStyleBackColor = False
        '
        'btnSpecial2
        '
        Me.btnSpecial2.BackColor = System.Drawing.Color.White
        Me.btnSpecial2.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnSpecial2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnSpecial2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnSpecial2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSpecial2.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSpecial2.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnSpecial2.ImageIndex = 1
        Me.btnSpecial2.ImageList = Me.ImageList1
        Me.btnSpecial2.Location = New System.Drawing.Point(129, 7)
        Me.btnSpecial2.Name = "btnSpecial2"
        Me.btnSpecial2.Size = New System.Drawing.Size(120, 105)
        Me.btnSpecial2.TabIndex = 207
        Me.btnSpecial2.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnSpecial2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnSpecial2.UseVisualStyleBackColor = False
        '
        'btnSpecial1
        '
        Me.btnSpecial1.BackColor = System.Drawing.Color.White
        Me.btnSpecial1.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnSpecial1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.btnSpecial1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnSpecial1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSpecial1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSpecial1.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnSpecial1.ImageIndex = 1
        Me.btnSpecial1.ImageList = Me.ImageList1
        Me.btnSpecial1.Location = New System.Drawing.Point(10, 7)
        Me.btnSpecial1.Name = "btnSpecial1"
        Me.btnSpecial1.Size = New System.Drawing.Size(120, 105)
        Me.btnSpecial1.TabIndex = 206
        Me.btnSpecial1.Text = "NOT AVAILABLE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnSpecial1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnSpecial1.UseVisualStyleBackColor = False
        '
        'TabItem3
        '
        Me.TabItem3.AttachedControl = Me.TabControlPanel4
        Me.TabItem3.Image = CType(resources.GetObject("TabItem3.Image"), System.Drawing.Image)
        Me.TabItem3.Name = "TabItem3"
        Me.TabItem3.Text = "Special"
        '
        'frmMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.BackgroundImage = Global.Order.My.Resources.Resources.Form_2
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(806, 604)
        Me.Controls.Add(Me.TabControl2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmMenu"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Main Menu - Order Management System"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        CType(Me.TabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        CType(Me.TabControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl2.ResumeLayout(False)
        Me.TabControlPanel3.ResumeLayout(False)
        Me.TabControlPanel3.PerformLayout()
        Me.TabControlPanel2.ResumeLayout(False)
        Me.TabControlPanel2.PerformLayout()
        Me.TabControlPanel5.ResumeLayout(False)
        CType(Me.dtgRecord, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel7.ResumeLayout(False)
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.TabControlPanel4.ResumeLayout(False)
        Me.TabControlPanel4.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnMin As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents LstOrder As System.Windows.Forms.ListBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtAmount As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents TimerMenu As System.Windows.Forms.Timer
    Friend WithEvents BtnCancel As System.Windows.Forms.Button
    Friend WithEvents BtnPayment As System.Windows.Forms.Button
    Friend WithEvents BtnClear As System.Windows.Forms.Button
    Friend WithEvents BtnRemove As System.Windows.Forms.Button
    Friend WithEvents btnLogout As System.Windows.Forms.Button
    Friend WithEvents lblTime As System.Windows.Forms.Label
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents LogoutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Public WithEvents ImageListBurger As System.Windows.Forms.ImageList
    Friend WithEvents ImageListDrink As System.Windows.Forms.ImageList
    Friend WithEvents ImageListSpecial As System.Windows.Forms.ImageList
    Friend WithEvents btnAdmin As System.Windows.Forms.Button
    Friend WithEvents LinkAbout As System.Windows.Forms.LinkLabel
    Friend WithEvents TabControl1 As DevComponents.DotNetBar.TabControl
    Friend WithEvents TabControlPanel1 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabControl2 As DevComponents.DotNetBar.TabControl
    Friend WithEvents TabControlPanel2 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem1 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel3 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem2 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel5 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem4 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel4 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem3 As DevComponents.DotNetBar.TabItem
    Friend WithEvents btnClearBurger16 As System.Windows.Forms.Button
    Friend WithEvents btnClearBurger15 As System.Windows.Forms.Button
    Friend WithEvents btnClearBurger14 As System.Windows.Forms.Button
    Friend WithEvents btnClearBurger13 As System.Windows.Forms.Button
    Friend WithEvents btnClearBurger12 As System.Windows.Forms.Button
    Friend WithEvents btnClearBurger11 As System.Windows.Forms.Button
    Friend WithEvents btnClearBurger10 As System.Windows.Forms.Button
    Friend WithEvents btnClearBurger9 As System.Windows.Forms.Button
    Friend WithEvents btnClearBurger8 As System.Windows.Forms.Button
    Friend WithEvents btnClearBurger7 As System.Windows.Forms.Button
    Friend WithEvents btnClearBurger6 As System.Windows.Forms.Button
    Friend WithEvents btnClearBurger5 As System.Windows.Forms.Button
    Friend WithEvents btnClearBurger4 As System.Windows.Forms.Button
    Friend WithEvents btnClearBurger3 As System.Windows.Forms.Button
    Friend WithEvents btnClearBurger2 As System.Windows.Forms.Button
    Friend WithEvents btnClearBurger1 As System.Windows.Forms.Button
    Friend WithEvents btnEditBurger16 As System.Windows.Forms.Button
    Friend WithEvents btnEditBurger15 As System.Windows.Forms.Button
    Friend WithEvents btnEditBurger14 As System.Windows.Forms.Button
    Friend WithEvents btnEditBurger13 As System.Windows.Forms.Button
    Friend WithEvents btnEditBurger12 As System.Windows.Forms.Button
    Friend WithEvents btnEditBurger11 As System.Windows.Forms.Button
    Friend WithEvents btnEditBurger10 As System.Windows.Forms.Button
    Friend WithEvents btnEditBurger9 As System.Windows.Forms.Button
    Friend WithEvents btnEditBurger8 As System.Windows.Forms.Button
    Friend WithEvents btnEditBurger7 As System.Windows.Forms.Button
    Friend WithEvents btnEditBurger6 As System.Windows.Forms.Button
    Friend WithEvents btnEditBurger5 As System.Windows.Forms.Button
    Friend WithEvents btnEditBurger4 As System.Windows.Forms.Button
    Friend WithEvents btnEditBurger3 As System.Windows.Forms.Button
    Friend WithEvents btnEditBurger2 As System.Windows.Forms.Button
    Friend WithEvents btnEditBurger1 As System.Windows.Forms.Button
    Friend WithEvents lblBurger16 As System.Windows.Forms.Label
    Friend WithEvents lblBurger15 As System.Windows.Forms.Label
    Friend WithEvents lblBurger14 As System.Windows.Forms.Label
    Friend WithEvents lblBurger13 As System.Windows.Forms.Label
    Friend WithEvents lblBurger12 As System.Windows.Forms.Label
    Friend WithEvents lblBurger11 As System.Windows.Forms.Label
    Friend WithEvents lblBurger10 As System.Windows.Forms.Label
    Friend WithEvents lblBurger9 As System.Windows.Forms.Label
    Friend WithEvents lblBurger8 As System.Windows.Forms.Label
    Friend WithEvents lblBurger7 As System.Windows.Forms.Label
    Friend WithEvents lblBurger6 As System.Windows.Forms.Label
    Friend WithEvents lblBurger5 As System.Windows.Forms.Label
    Friend WithEvents lblBurger4 As System.Windows.Forms.Label
    Friend WithEvents lblBurger3 As System.Windows.Forms.Label
    Friend WithEvents lblBurger2 As System.Windows.Forms.Label
    Friend WithEvents lblBurger1 As System.Windows.Forms.Label
    Friend WithEvents btnBurger16 As System.Windows.Forms.Button
    Friend WithEvents btnBurger15 As System.Windows.Forms.Button
    Friend WithEvents btnBurger14 As System.Windows.Forms.Button
    Friend WithEvents btnBurger13 As System.Windows.Forms.Button
    Friend WithEvents btnBurger12 As System.Windows.Forms.Button
    Friend WithEvents btnBurger11 As System.Windows.Forms.Button
    Friend WithEvents btnBurger10 As System.Windows.Forms.Button
    Friend WithEvents btnBurger9 As System.Windows.Forms.Button
    Friend WithEvents btnBurger8 As System.Windows.Forms.Button
    Friend WithEvents btnBurger7 As System.Windows.Forms.Button
    Friend WithEvents btnBurger6 As System.Windows.Forms.Button
    Friend WithEvents btnBurger5 As System.Windows.Forms.Button
    Friend WithEvents btnBurger4 As System.Windows.Forms.Button
    Friend WithEvents btnBurger3 As System.Windows.Forms.Button
    Friend WithEvents btnBurger2 As System.Windows.Forms.Button
    Friend WithEvents btnBurger1 As System.Windows.Forms.Button
    Friend WithEvents btnClearDrink16 As System.Windows.Forms.Button
    Friend WithEvents btnClearDrink15 As System.Windows.Forms.Button
    Friend WithEvents btnClearDrink14 As System.Windows.Forms.Button
    Friend WithEvents btnClearDrink13 As System.Windows.Forms.Button
    Friend WithEvents btnClearDrink12 As System.Windows.Forms.Button
    Friend WithEvents btnClearDrink11 As System.Windows.Forms.Button
    Friend WithEvents btnClearDrink10 As System.Windows.Forms.Button
    Friend WithEvents btnClearDrink9 As System.Windows.Forms.Button
    Friend WithEvents btnClearDrink8 As System.Windows.Forms.Button
    Friend WithEvents btnClearDrink7 As System.Windows.Forms.Button
    Friend WithEvents btnClearDrink6 As System.Windows.Forms.Button
    Friend WithEvents btnClearDrink5 As System.Windows.Forms.Button
    Friend WithEvents btnClearDrink4 As System.Windows.Forms.Button
    Friend WithEvents btnClearDrink3 As System.Windows.Forms.Button
    Friend WithEvents btnClearDrink2 As System.Windows.Forms.Button
    Friend WithEvents btnClearDrink1 As System.Windows.Forms.Button
    Friend WithEvents btnEditDrink16 As System.Windows.Forms.Button
    Friend WithEvents btnEditDrink15 As System.Windows.Forms.Button
    Friend WithEvents btnEditDrink14 As System.Windows.Forms.Button
    Friend WithEvents btnEditDrink13 As System.Windows.Forms.Button
    Friend WithEvents btnEditDrink12 As System.Windows.Forms.Button
    Friend WithEvents btnEditDrink11 As System.Windows.Forms.Button
    Friend WithEvents btnEditDrink10 As System.Windows.Forms.Button
    Friend WithEvents btnEditDrink9 As System.Windows.Forms.Button
    Friend WithEvents btnEditDrink8 As System.Windows.Forms.Button
    Friend WithEvents btnEditDrink7 As System.Windows.Forms.Button
    Friend WithEvents btnEditDrink6 As System.Windows.Forms.Button
    Friend WithEvents btnEditDrink5 As System.Windows.Forms.Button
    Friend WithEvents btnEditDrink4 As System.Windows.Forms.Button
    Friend WithEvents btnEditDrink3 As System.Windows.Forms.Button
    Friend WithEvents btnEditDrink2 As System.Windows.Forms.Button
    Friend WithEvents btnEditDrink1 As System.Windows.Forms.Button
    Friend WithEvents lblDrink16 As System.Windows.Forms.Label
    Friend WithEvents lblDrink15 As System.Windows.Forms.Label
    Friend WithEvents lblDrink14 As System.Windows.Forms.Label
    Friend WithEvents lblDrink13 As System.Windows.Forms.Label
    Friend WithEvents lblDrink12 As System.Windows.Forms.Label
    Friend WithEvents lblDrink11 As System.Windows.Forms.Label
    Friend WithEvents lblDrink10 As System.Windows.Forms.Label
    Friend WithEvents lblDrink9 As System.Windows.Forms.Label
    Friend WithEvents lblDrink8 As System.Windows.Forms.Label
    Friend WithEvents lblDrink7 As System.Windows.Forms.Label
    Friend WithEvents lblDrink6 As System.Windows.Forms.Label
    Friend WithEvents lblDrink5 As System.Windows.Forms.Label
    Friend WithEvents lblDrink4 As System.Windows.Forms.Label
    Friend WithEvents lblDrink3 As System.Windows.Forms.Label
    Friend WithEvents lblDrink2 As System.Windows.Forms.Label
    Friend WithEvents lblDrink1 As System.Windows.Forms.Label
    Friend WithEvents btnDrink16 As System.Windows.Forms.Button
    Friend WithEvents btnDrink15 As System.Windows.Forms.Button
    Friend WithEvents btnDrink14 As System.Windows.Forms.Button
    Friend WithEvents btnDrink13 As System.Windows.Forms.Button
    Friend WithEvents btnDrink12 As System.Windows.Forms.Button
    Friend WithEvents btnDrink11 As System.Windows.Forms.Button
    Friend WithEvents btnDrink10 As System.Windows.Forms.Button
    Friend WithEvents btnDrink9 As System.Windows.Forms.Button
    Friend WithEvents btnDrink8 As System.Windows.Forms.Button
    Friend WithEvents btnDrink7 As System.Windows.Forms.Button
    Friend WithEvents btnDrink6 As System.Windows.Forms.Button
    Friend WithEvents btnDrink5 As System.Windows.Forms.Button
    Friend WithEvents btnDrink4 As System.Windows.Forms.Button
    Friend WithEvents btnDrink3 As System.Windows.Forms.Button
    Friend WithEvents btnDrink2 As System.Windows.Forms.Button
    Friend WithEvents btnDrink1 As System.Windows.Forms.Button
    Friend WithEvents btnClearSpecial16 As System.Windows.Forms.Button
    Friend WithEvents btnClearSpecial15 As System.Windows.Forms.Button
    Friend WithEvents btnClearSpecial14 As System.Windows.Forms.Button
    Friend WithEvents btnClearSpecial13 As System.Windows.Forms.Button
    Friend WithEvents btnClearSpecial12 As System.Windows.Forms.Button
    Friend WithEvents btnClearSpecial11 As System.Windows.Forms.Button
    Friend WithEvents btnClearSpecial10 As System.Windows.Forms.Button
    Friend WithEvents btnClearSpecial9 As System.Windows.Forms.Button
    Friend WithEvents btnClearSpecial8 As System.Windows.Forms.Button
    Friend WithEvents btnClearSpecial7 As System.Windows.Forms.Button
    Friend WithEvents btnClearSpecial6 As System.Windows.Forms.Button
    Friend WithEvents btnClearSpecial5 As System.Windows.Forms.Button
    Friend WithEvents btnClearSpecial4 As System.Windows.Forms.Button
    Friend WithEvents btnClearSpecial3 As System.Windows.Forms.Button
    Friend WithEvents btnClearSpecial2 As System.Windows.Forms.Button
    Friend WithEvents btnClearSpecial1 As System.Windows.Forms.Button
    Friend WithEvents btnEditSpecial16 As System.Windows.Forms.Button
    Friend WithEvents btnEditSpecial15 As System.Windows.Forms.Button
    Friend WithEvents btnEditSpecial14 As System.Windows.Forms.Button
    Friend WithEvents btnEditSpecial13 As System.Windows.Forms.Button
    Friend WithEvents btnEditSpecial12 As System.Windows.Forms.Button
    Friend WithEvents btnEditSpecial11 As System.Windows.Forms.Button
    Friend WithEvents btnEditSpecial10 As System.Windows.Forms.Button
    Friend WithEvents btnEditSpecial9 As System.Windows.Forms.Button
    Friend WithEvents btnEditSpecial8 As System.Windows.Forms.Button
    Friend WithEvents btnEditSpecial7 As System.Windows.Forms.Button
    Friend WithEvents btnEditSpecial6 As System.Windows.Forms.Button
    Friend WithEvents btnEditSpecial5 As System.Windows.Forms.Button
    Friend WithEvents btnEditSpecial4 As System.Windows.Forms.Button
    Friend WithEvents btnEditSpecial3 As System.Windows.Forms.Button
    Friend WithEvents btnEditSpecial2 As System.Windows.Forms.Button
    Friend WithEvents btnEditSpecial1 As System.Windows.Forms.Button
    Friend WithEvents lblSpecial16 As System.Windows.Forms.Label
    Friend WithEvents lblSpecial15 As System.Windows.Forms.Label
    Friend WithEvents lblSpecial14 As System.Windows.Forms.Label
    Friend WithEvents lblSpecial13 As System.Windows.Forms.Label
    Friend WithEvents lblSpecial12 As System.Windows.Forms.Label
    Friend WithEvents lblSpecial11 As System.Windows.Forms.Label
    Friend WithEvents lblSpecial10 As System.Windows.Forms.Label
    Friend WithEvents lblSpecial9 As System.Windows.Forms.Label
    Friend WithEvents lblSpecial8 As System.Windows.Forms.Label
    Friend WithEvents lblSpecial7 As System.Windows.Forms.Label
    Friend WithEvents lblSpecial6 As System.Windows.Forms.Label
    Friend WithEvents lblSpecial5 As System.Windows.Forms.Label
    Friend WithEvents lblSpecial4 As System.Windows.Forms.Label
    Friend WithEvents lblSpecial3 As System.Windows.Forms.Label
    Friend WithEvents lblSpecial2 As System.Windows.Forms.Label
    Friend WithEvents lblSpecial1 As System.Windows.Forms.Label
    Friend WithEvents btnSpecial16 As System.Windows.Forms.Button
    Friend WithEvents btnSpecial15 As System.Windows.Forms.Button
    Friend WithEvents btnSpecial14 As System.Windows.Forms.Button
    Friend WithEvents btnSpecial13 As System.Windows.Forms.Button
    Friend WithEvents btnSpecial12 As System.Windows.Forms.Button
    Friend WithEvents btnSpecial11 As System.Windows.Forms.Button
    Friend WithEvents btnSpecial10 As System.Windows.Forms.Button
    Friend WithEvents btnSpecial9 As System.Windows.Forms.Button
    Friend WithEvents btnSpecial8 As System.Windows.Forms.Button
    Friend WithEvents btnSpecial7 As System.Windows.Forms.Button
    Friend WithEvents btnSpecial6 As System.Windows.Forms.Button
    Friend WithEvents btnSpecial5 As System.Windows.Forms.Button
    Friend WithEvents btnSpecial4 As System.Windows.Forms.Button
    Friend WithEvents btnSpecial3 As System.Windows.Forms.Button
    Friend WithEvents btnSpecial2 As System.Windows.Forms.Button
    Friend WithEvents btnSpecial1 As System.Windows.Forms.Button
    Friend WithEvents dtgRecord As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents Btnrefresh As System.Windows.Forms.Button
    Friend WithEvents btnExport As System.Windows.Forms.Button
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents txtSearch As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtSearchby As System.Windows.Forms.ComboBox
    Friend WithEvents lblRecords As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
End Class
