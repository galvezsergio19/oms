﻿Imports System.IO
Imports System.Security.Cryptography
Imports System.Text
Public Class frmPayment

#Region "Controlbox"

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
        LstOrder.Items.Clear()
        txtAmount.Clear()
        txtCash.Text = "0.00"
        txtChange.Text = "0.00"
    End Sub

#End Region

    Public Shared Function GetUniqueKey(ByVal maxSize As Integer) As String
        Dim chars As Char() = New Char(61) {}
        chars = "123456789".ToCharArray()
        Dim data As Byte() = New Byte(0) {}
        Dim crypto As New RNGCryptoServiceProvider()
        crypto.GetNonZeroBytes(data)
        data = New Byte(maxSize - 1) {}
        crypto.GetNonZeroBytes(data)
        Dim result As New StringBuilder(maxSize)
        For Each b As Byte In data
            result.Append(chars(b Mod (chars.Length)))
        Next
        Return result.ToString()
    End Function

    Private Sub txtCash_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCash.TextChanged
        txtChange.Text = Format(Val(txtCash.Text) - Val(txtAmount.Text), "###,##0.00")
    End Sub

    Private Sub BtnDone_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnDone.Click
        If Val(txtAmount.Text) > Val(txtCash.Text) Then
            MessageBox.Show("Please Enter cash value higher than the total amount.", "Invalid Cash", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Dim prodname As String = ""
            'If frmMenu.prod.StartsWith(",") = True Then
            '    prodname = frmMenu.prod.Remove(0, 1)
            'End If
            For i = 0 To Me.LstOrder.Items.Count - 1
                prodname = prodname & "," & LstOrder.Items(i).ToString.Replace("& vbcrlf", "")
            Next i
            If prodname.StartsWith(",") = True Then
                prodname = prodname.Remove(0, 1)
            End If
            Dim trpath As String = Application.StartupPath & "\Record\Record.txt"
            File.AppendAllText(trpath, ReceiptNo.Text & " | " & Format(Date.Now, "MM/dd/yyyy") & " | " & prodname.Trim & " | " & txtAmount.Text & " | " & Format(Val(txtCash.Text), "###,###.00") & " | " & txtChange.Text & vbCrLf)
            With frmReceipt
                For i = 0 To Me.LstOrder.Items.Count - 1
                    .LstOrder.Items.Add(LstOrder.Items(i))
                Next i
                .ReceiptNo.Text = Me.ReceiptNo.Text
                .lblTotal.Text = Me.txtAmount.Text
                .lblCash.Text = Me.txtCash.Text
                .lblChange.Text = Me.txtChange.Text
                .ShowDialog()
            End With
            prodname = ""
            btnClose_Click(sender, e)
            frmMenu.BtnClear_Click(sender, e)
            frmMenu.dtgRecord.Rows.Clear()
            frmMenu.Filter_Record()
            End If
    End Sub

    Private Sub frmPayment_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ReceiptNo.Text = "BURGER-" & GetUniqueKey(6)
    End Sub
End Class